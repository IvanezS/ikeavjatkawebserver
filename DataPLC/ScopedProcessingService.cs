﻿using Core.Domain.Management;
using DataAccess.CustomLogger;
using DataAccess.Repositories;
using DataPLC.Data;
using DataPLC.Settings;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DataPLC
{
    public  interface IScopedProcessingService
    {
        Task DoWork(CancellationToken stoppingToken);
    }

    public  class ScopedProcessingService : IScopedProcessingService
    {
        private int executionCount = 0;
        private readonly ILogger _logger;
        public PlcDbReadSettings _plcReadSet { get; private set; }
        public PlcDataExchangeBackgroundSettings _plcBgdSet { get; private set; }
        private readonly IDataPlcExchange plc;
        private readonly IPlcDb _plcDb;
        private int tryCounter = 0;
        private readonly LogRepository _log;
        private readonly ResortedPacketRepository _db_rpack;
        private readonly ItemRepository _db_item;
        private readonly SmenaRepository _db_smena;
        private PlcDto plcDto;
        private PlcDtoSet plcDtoSet;
        private IEnumerable<Alarm> alarms = new List<Alarm>();
        //private IEnumerable<Alarm> alarms_old = new List<Alarm>();
        public IServiceProvider _scopeFactory;

        public ScopedProcessingService( ILogger<ScopedProcessingService> logger,
                                        IOptions<PlcDbReadSettings> plcReadSet,
                                        IOptions<PlcDataExchangeBackgroundSettings> plcBgdSet,
                                        IDataPlcExchange dataPlcExchange,
                                        IPlcDb plcDb,
                                        IServiceProvider scopeFactory)
        {
            _logger = logger;
            _plcReadSet = plcReadSet.Value;
            _plcBgdSet = plcBgdSet.Value;
            plc = dataPlcExchange;
            _plcDb = plcDb;
            _scopeFactory = scopeFactory;
            var scope = _scopeFactory.CreateScope();
            _db_rpack = scope.ServiceProvider.GetRequiredService<ResortedPacketRepository>();
            _log = scope.ServiceProvider.GetRequiredService<LogRepository>();
            _db_item = scope.ServiceProvider.GetRequiredService<ItemRepository>();
            _db_smena = scope.ServiceProvider.GetRequiredService<SmenaRepository>();
            plc.ConnectPlc();
            plcDtoSet = _plcDb.GetPlcSetDBAsync().Result;
            plcDtoSet.SmenaNum = _db_smena.getLastSmenaNum().Result;
            plcDtoSet.RpackNumber = _db_rpack.getLastPacketNum().Result;
            _plcDb.UpdateSettings(plcDtoSet);
        }

        public async Task DoWork(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                executionCount++;

                _logger.LogInformation(
                    "Scoped Processing Service is working. Count: {Count}", executionCount);
                PlcScanAsync();
                await Task.Delay(1, stoppingToken);
            }
        }

        private void GetConnectAlarm(int status)
        {
            if (status == 1)
            {
                //plc.ConnectPlc();
                tryCounter++;
                if (tryCounter > 1)
                {
                    tryCounter = 1;
                    _plcDb.SetAlarmNumber(1000, true);
                }

            }
            else
            {
                if (tryCounter != 0) _plcDb.SetAlarmNumber(1000, false);
                tryCounter = 0;
            }
        }

        public void PlcScanAsync()
        {

            GetConnectAlarm(_plcDb.ReadPlcDb());

            plcDtoSet = _plcDb.GetPlcSetDBAsync().Result;
            plcDto = _plcDb.GetPlcDBAsync().Result;
            alarms = _plcDb.GetPlcAllAlarms().Result;

            foreach (var a in alarms)
            {
                if (a.ON && !a.ON_old)
                {
                    _log.CreateAsync(new LogModel()
                    {
                        Message = a.Description,
                        LogType = "Авария",
                        User = "Система",
                        LogTime = DateTime.Now,
                        ON_OFF_text = "Пришло"
                    });
                }

                if (!a.ON && a.ON_old)
                {
                    _log.CreateAsync(new LogModel()
                    {
                        Message = a.Description,
                        LogType = "Авария",
                        User = "Система",
                        LogTime = DateTime.Now,
                        ON_OFF_text = "Ушло"
                    });
                }
                a.ON_old = a.ON;
            }

            //Incorrect settings in PLC
            var res = (plcDto.ItemId < 0 || plcDto.PartId == 0) ? _plcDb.SetAlarmNumber(1001, true) : _plcDb.SetAlarmNumber(1001, false);

            //Different packs creation
            if ((plcDto.PackResorted || plcDto.DefectPacketResorted || plcDtoSet.GetPackSrez) && plcDto.ItemId >= 0)
            {
                if (plcDto.PartId > 0) CreateResortedPacket();
                if (plcDtoSet.GetPackSrez) _plcDb.GetPackSrez(false);
            }

            //Smena not active
            _plcDb.SetAlarmNumber(1003, plcDtoSet.SmenaActive ? false : true);



        }

        private async Task CreateResortedPacket()
        {
            var item = await _db_item.GetByIdAsync(plcDtoSet.ItemId);

            var rpack = new DResortedPacket()
            {
                PackDate = DateTime.Now,
                SmenaNum = plcDtoSet.SmenaNum,
                SmenaName = plcDtoSet.SmenaName,
                PartId = plcDto.PartId,
                PacketNumber = plcDtoSet.RpackNumber,
                isDefected = plcDto.DefectPacketResorted ? true : false,
            };

            if (plcDto.DefectPacketResorted)
            {
                rpack.TotalPlanks = plcDto.QPlanksNotOk;
                rpack.TotalVolume = plcDto.QPlanksNotOk * (float)(item.Length * item.Thick * item.Width * 0.000000001);
                _plcDb.PackDefectedReaded();
            }

            if (plcDto.PackResorted)
            {
                rpack.TotalPlanks = plcDto.QPlanksOk - plcDtoSet.QplankRemembered;
                rpack.TotalVolume = (float)(plcDto.VPlanksOk * 0.0000001) - plcDtoSet.VplankRemembered;
                _plcDb.PackResortedReaded();

            }

            if (plcDtoSet.GetPackSrez)
            {
                rpack.TotalPlanks = plcDto.NumLayerActual * plcDto.QPlanksInLayer + plcDto.NumPlankInLayerActual - plcDtoSet.QplankRemembered;
                rpack.TotalVolume = (float)((plcDto.VPlanksOk + plcDto.VLayerOk) * 0.0000001) - plcDtoSet.VplankRemembered;
                _plcDb.SaveSrez(rpack.TotalPlanks, rpack.TotalVolume);
            }
            else
            {
                if (!plcDto.DefectPacketResorted) _plcDb.SaveSrez(0, 0);
            }

            try
            {
                await _db_rpack.CreateAsync(rpack);
                var pack = await _db_rpack.GetByIdAsync(rpack.Id);

                await _log.CreateAsync(new LogModel()
                {
                    Message = "Создан пересортированный пакет номер " + pack.PacketNumber + " в партии номер " + pack.Part.PartNumber,
                    LogType = "Тех. процесс",
                    User = "Система",
                    LogTime = DateTime.Now,
                    ON_OFF_text = "Пришло"
                });
                _plcDb.IncreaseRPackNumber();
            }
            catch (Exception e)
            {
                throw e;
            }
        }


    }
}
