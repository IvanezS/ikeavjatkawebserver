﻿using Core.Domain.Management;
using DataAccess.CustomLogger;
using DataAccess.Data;
using DataAccess.Repositories;
using DataPLC.Core;
using DataPLC.Data;
using DataPLC.Settings;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
//using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using S7.Net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace DataPLC
{
    public class PlcDataExchangeService : BackgroundService
    {

        public IServiceProvider _scopeFactory;
        private readonly ILogger _logger;

        
        public  PlcDataExchangeService (IServiceProvider scopeFactory,
                                        ILogger<PlcDataExchangeService> logger)
        {
            _scopeFactory = scopeFactory;
            _logger = logger;
        }

        //protected async override Task ExecuteAsync(CancellationToken stoppingToken)
        //{

        //    int counter = 0;
        //    while (!stoppingToken.IsCancellationRequested)
        //    {
        //        try
        //        {
        //            counter++;
        //            _logger.LogInformation("======================================Cycle № " + counter + ": " + DateTime.Now.ToString("s"));
        //            //plc.ConnectPlc();
        //            await PlcScanAsync(stoppingToken);
        //            await Task.Delay(_plcBgdSet.CheckUpdateTime, stoppingToken);
        //            //plc.DisConnectPlc();
        //        }
        //        catch (Exception ee)
        //        {
        //            _logger.LogInformation("---------------> Exception event:" + ee);
        //        }

        //    }

        //    plc.DisConnectPlc();
        //}


        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation(
                "Consume Scoped Service Hosted Service running.");

            await DoWork(stoppingToken);
        }

        private async Task DoWork(CancellationToken stoppingToken)
        {
            _logger.LogInformation(
                "Consume Scoped Service Hosted Service is working.");

            using (var scope = _scopeFactory.CreateScope())
            {
                var scopedProcessingService =
                    scope.ServiceProvider
                        .GetRequiredService<IScopedProcessingService>();

                await scopedProcessingService.DoWork(stoppingToken);
            }
        }

        public override async Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation(
                "Consume Scoped Service Hosted Service is stopping.");

            await base.StopAsync(stoppingToken);
        }


    }
}

