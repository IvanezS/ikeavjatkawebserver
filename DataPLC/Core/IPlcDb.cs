﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataPLC.Data
{
    public interface IPlcDb
    {
        int ReadPlcDb();
        Task<PlcDto> GetPlcDBAsync();
        Task<PlcDtoSet> GetPlcSetDBAsync();
        Task<IEnumerable<Im>> GetPlcDbImsAsync();
        int StartNewPack(bool smenaActive);
        int FinishPack();
        int SetPlcDB(PlcDtoSet plcDtoSet);
        int PackResortedReaded();
        int FinishDefectedPack();
        int DefectedPackPushed();
        int PackDefectedReaded();
        int PackReadyRidOff();
        int GasketRidedOff();
        void ReadSettingsFromPlc();
        void UpdateSettings(PlcDtoSet plcDtoSet);
        int SendPackParam(short SetNumLayer, short SetNumPlankInLayer);
        int SendPackParamApply();
        Task<IEnumerable<Alarm>> GetPlcActualAlarms();
        Task<List<Alarm>> GetPlcAllAlarms();
        int SetAlarmNumber(int number, bool state);
        void IncreaseRPackNumber();
        void IncreaseDryPackNumber();
        Task<int> ApplySpeedSettings(short speed);
        void GetPackSrez(bool val);
        void SaveSrez(int q, float v);
        int AddGasketsLayer();
        int PackBrakParamChanged(short SetNumPlankInLayerBrak);
        void SmenaNumCorrect(int smenaNum);
        void PackNumCorrect(int packNum);
        int GasketsBrakPut();
    }
}
