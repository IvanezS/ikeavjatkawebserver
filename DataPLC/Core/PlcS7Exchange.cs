﻿
using System;
using System.Collections.Generic;
using System.Text;
using DataPLC.Settings;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using S7.Net;

namespace DataPLC.Core
{
    public class PlcS7Exchange : IDataPlcExchange
    {
        //public readonly IDataPlcExchange _dataPlcExchange;
        private readonly PlcSettings _plcOption;
        //public Plc plc;
        private readonly ILogger _logger;
        private object _locker = new object();
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="plcOption"></param>
        public PlcS7Exchange(IOptions<PlcSettings> plcOption, ILogger<PlcS7Exchange> logger)
        {
            _logger = logger;
            _plcOption = plcOption.Value;
            //plc = new Plc((CpuType)Enum.Parse(typeof(CpuType), _plcOption.PlcType), _plcOption.IPaddress, _plcOption.Rack, _plcOption.Slot);
        }

        /// <summary>
        /// Соединение с ПЛК
        /// </summary>
        /// <returns></returns>
        public bool ConnectPlc()
        {
            
            //if (plc.IsAvailable)
            //{
                //if (!plc.IsConnected)
                //{
                    //try
                    //{
                    //    plc.Open();
                    //    return true;
                    //}
                    //catch (Exception ee)
                    //{
                    //_logger.LogInformation("---------------> Exception event in ConnectPlc:" + ee);
                    //return false;
                    //}
                //}
                //else
               // {
                    return true;
               // }
            //}
            //else
            //{
                //return false;
            //}
        }

        /// <summary>
        /// Разъединение с ПЛК
        /// </summary>
        /// <returns></returns>
        public bool DisConnectPlc()
        {
            //if (plc.IsConnected)
            //{
            //    try
            //    {
            //        plc.Close();
            //        return true;
            //    }
            //    catch(Exception ee)
            //    {
            //        _logger.LogInformation("---------------> Exception event in DisConnectPlc:" + ee);
            //        return false;
            //    }
            //}
            //else
            //{
                return true;
            //}
        }


        /// <summary>
        /// Чтение блока данных
        /// </summary>
        /// <param name="DB_Num">Номер дата блока</param>
        /// <param name="BytesToRead">Кол-во считываемых байтов</param>
        /// <param name="Offset">Смещение в блоке данных</param>
        /// <returns></returns>
        public byte[] Read_DB(int DB_Num, int BytesToRead, int Offset)
        {
            lock (_locker)
            {
                try
                {
                    using (var plc = new Plc((CpuType)Enum.Parse(typeof(CpuType), _plcOption.PlcType), _plcOption.IPaddress, _plcOption.Rack, _plcOption.Slot))
                    {
                        plc.Open();
                        var ReadedBytes = plc.ReadBytes(DataType.DataBlock, DB_Num, Offset, BytesToRead);
                        plc.Close();
                        if (ReadedBytes.Length == BytesToRead) return ReadedBytes;
                        return null;
                    }
                }
                catch (Exception ee)
                {
                    _logger.LogInformation("---------------> Exception event in Read_DB:" + ee);
                    return null;
                }
            }
            
        }

        /// <summary>
        /// Запись блока данных
        /// </summary>
        /// <param name="DB_Num">Номер дата блока</param>
        /// <param name="Offsett">Смещение в блоке данных</param>
        /// <param name="db">Массив записываемых значений</param>
        /// <returns></returns>
        public int Write_DB(int DB_Num, int Offsett, byte[] db)
        {
            lock (_locker)
            {
                try
                {
                    using (var plc = new Plc((CpuType)Enum.Parse(typeof(CpuType), _plcOption.PlcType), _plcOption.IPaddress, _plcOption.Rack, _plcOption.Slot))
                    {
                        plc.Open();
                        plc.WriteBytes(DataType.DataBlock, DB_Num, Offsett, db);
                        plc.Close();
                    }
                    return 0;
                }
                catch (Exception ee)
                {
                    _logger.LogInformation("---------------> Exception event in Write_DB:" + ee);
                    return 1;
                }
            }
        }

        /// <summary>
        /// Запись бита в блок данных
        /// </summary>
        /// <param name="DB_Num">Номер дата блока</param>
        /// <param name="startByteAdr">Смещение в блоке данных</param>
        /// <param name="bitAdr">Номер бита</param>
        /// <param name="value">Значение бита</param>
        /// <returns></returns>
        public int WriteBit(int DB_Num, int startByteAdr, int bitAdr, int value)
        {
            lock (_locker)
            {
                if (value < 0 || value > 1) throw new ArgumentException("Value must be 0 or 1", nameof(value));
                try
                {
                    using (var plc = new Plc((CpuType)Enum.Parse(typeof(CpuType), _plcOption.PlcType), _plcOption.IPaddress, _plcOption.Rack, _plcOption.Slot))
                    {
                        plc.Open();
                        plc.WriteBit(DataType.DataBlock, DB_Num, startByteAdr, bitAdr, value);
                        _logger.LogInformation("---------------> Bit {0}.{1} sended", startByteAdr, bitAdr);
                        plc.Close();
                    }
                    return 0;
                }
                catch (Exception ee)
                {
                    _logger.LogInformation("---------------> Exception event in WriteBit:" + ee);
                    return 1;
                }
            }
        }


    }
}
