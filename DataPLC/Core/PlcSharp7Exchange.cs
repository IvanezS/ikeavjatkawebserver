﻿
using System;
using System.Collections.Generic;
using System.Text;
using DataPLC.Settings;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sharp7;

namespace DataPLC.Core
{
    public class PlcSharp7Exchange : IDataPlcExchange
    {
        //public readonly IDataPlcExchange _dataPlcExchange;
        private readonly PlcSettings _plcOption;
        //public Plc plc;
        private readonly ILogger _logger;
        private object _locker = new object();
        private S7Client client;
        public S7Client Client => client;
        //private S7Client client;
        int rc = 0;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="plcOption"></param>
        public PlcSharp7Exchange(IOptions<PlcSettings> plcOption, ILogger<PlcS7Exchange> logger)
        {
            _logger = logger;
            _plcOption = plcOption.Value;
            client = new S7Client("S7-400");
            client.SetConnectionType(S7Client.CONNTYPE_BASIC);
            //plc = new Plc((CpuType)Enum.Parse(typeof(CpuType), _plcOption.PlcType), _plcOption.IPaddress, _plcOption.Rack, _plcOption.Slot);
        }

        /// <summary>
        /// Соединение с ПЛК
        /// </summary>
        /// <returns></returns>
        public bool ConnectPlc()
        {
            
            //if (plc.IsAvailable)
            //{
                //if (!plc.IsConnected)
                //{
                    //try
                    //{
                    //    plc.Open();
                    //    return true;
                    //}
                    //catch (Exception ee)
                    //{
                    //_logger.LogInformation("---------------> Exception event in ConnectPlc:" + ee);
                    //return false;
                    //}
                //}
                //else
               // {
                    return true;
               // }
            //}
            //else
            //{
                //return false;
            //}
        }

        /// <summary>
        /// Разъединение с ПЛК
        /// </summary>
        /// <returns></returns>
        public bool DisConnectPlc()
        {
            //if (plc.IsConnected)
            //{
            //    try
            //    {
            //        plc.Close();
            //        return true;
            //    }
            //    catch(Exception ee)
            //    {
            //        _logger.LogInformation("---------------> Exception event in DisConnectPlc:" + ee);
            //        return false;
            //    }
            //}
            //else
            //{
                return true;
            //}
        }


        /// <summary>
        /// Чтение блока данных
        /// </summary>
        /// <param name="DB_Num">Номер дата блока</param>
        /// <param name="BytesToRead">Кол-во считываемых байтов</param>
        /// <param name="Offset">Смещение в блоке данных</param>
        /// <returns></returns>
        public byte[] Read_DB(int DB_Num, int BytesToRead, int Offset)
        {
            lock (_locker)
            {
                var DB = new byte[BytesToRead];
                try
                {

                        rc = client.ConnectTo(_plcOption.IPaddress, _plcOption.Rack, _plcOption.Slot);
                        rc = client.Connect();
                        int result = client.DBRead(DB_Num, Offset, BytesToRead, DB);
                     if (result !=0)_logger.LogInformation("Проблема с чтением дата блока. Результат операции: " + result + ": " + DateTime.Now.ToString("s"));
                    rc = client.Disconnect();
                        if (DB.Length == BytesToRead) return DB;
                        return null;
                }
                catch (Exception ee)
                {
                    _logger.LogInformation("---------------> Exception event in Read_DB:" + ee);
                    return null;
                }
            }
            
        }

        /// <summary>
        /// Запись блока данных
        /// </summary>
        /// <param name="DB_Num">Номер дата блока</param>
        /// <param name="Offsett">Смещение в блоке данных</param>
        /// <param name="db">Массив записываемых значений</param>
        /// <returns></returns>
        public int Write_DB(int DB_Num, int Offsett, byte[] db)
        {
            lock (_locker)
            {
                try
                {
                    rc = client.ConnectTo(_plcOption.IPaddress, _plcOption.Rack, _plcOption.Slot);
                    rc = client.Connect();
                    int result = client.DBWrite(DB_Num, Offsett, db.Length, db);
                    rc = client.Disconnect();
                    return 0;
                }
                catch (Exception ee)
                {
                    _logger.LogInformation("---------------> Exception event in Write_DB:" + ee);
                    return 1;
                }
            }
        }

        /// <summary>
        /// Запись бита в блок данных
        /// </summary>
        /// <param name="DB_Num">Номер дата блока</param>
        /// <param name="startByteAdr">Смещение в блоке данных</param>
        /// <param name="bitAdr">Номер бита</param>
        /// <param name="value">Значение бита</param>
        /// <returns></returns>
        [Obsolete]
        public int WriteBit(int DB_Num, int startByteAdr, int bitAdr, int value)
        {
            lock (_locker)
            {
                int Size = Convert.ToInt32(1);
                byte[] buffer = new byte[1];
                Sharp7.S7.SetBitAt(ref buffer, 0, bitAdr, value == 1 ? true : false);
                if (value < 0 || value > 1) throw new ArgumentException("Value must be 0 or 1", nameof(value));
                try
                {
                    //using (var plc = new Plc((CpuType)Enum.Parse(typeof(CpuType), _plcOption.PlcType), _plcOption.IPaddress, _plcOption.Rack, _plcOption.Slot))
                    //{
                    //    plc.Open();
                    //    plc.WriteBit(DataType.DataBlock, DB_Num, startByteAdr, bitAdr, value);
                    //    _logger.LogInformation("---------------> Bit {0}.{1} sended", startByteAdr, bitAdr);
                    //    plc.Close();
                    //}
                    rc = client.ConnectTo(_plcOption.IPaddress, _plcOption.Rack, _plcOption.Slot);
                    rc = client.Connect();
                    rc = client.WriteArea(S7Consts.S7AreaDB, DB_Num, startByteAdr * 8 + bitAdr, Size, S7Consts.S7WLBit, buffer);
                    rc = client.Disconnect();
                    return 0;
                }
                catch (Exception ee)
                {
                    _logger.LogInformation("---------------> Exception event in WriteBit:" + ee);
                    return 1;
                }
            }
        }


    }
}
