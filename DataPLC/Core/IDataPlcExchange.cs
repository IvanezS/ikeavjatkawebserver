﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataPLC
{
    public interface IDataPlcExchange
    {
        public bool ConnectPlc();
        public bool DisConnectPlc();
        public byte[] Read_DB(int DB_Num, int BytesToRead, int Offset);
        public int Write_DB(int DB_Num, int Offsett, byte[] db);
        public int WriteBit(int DB_Num, int startByteAdr, int bitAdr, int value);
    }
}
