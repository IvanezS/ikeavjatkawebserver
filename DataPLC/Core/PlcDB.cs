﻿using Core.Abstractions.Repositories;
using Core.Domain.Management;
using DataPLC.Data;
using DataPLC.Settings;
using Microsoft.Extensions.Options;
using S7.Net;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Repositories;
using Microsoft.Extensions.Logging;
using System.Timers;
using Microsoft.Extensions.DependencyInjection;
using DataAccess.CustomLogger;

namespace DataPLC.Core
{
    public class PlcDB : IPlcDb, IDisposable
    {
        private readonly IDataPlcExchange plcE;
        public PlcDbReadSettings _plcReadSet { get; private set; }
        public ImSettings _imSet { get; private set; }
        public PlcAlarmSettings _almSet { get; private set; }
        private PlcDto plcDto;
        private PlcDtoSet plcDtoSet;
        private List<Im> im = new List<Im>();
        private List<Alarm> alarms { get; set; } = new List<Alarm>();
        private readonly ILogger _logger;
        private readonly Timer _timer;
        private DateTime _lastScanTime;
        private TimeSpan _scanTime;
        private int tryCounter = 0;
        private readonly LogRepository _log;
        private readonly ResortedPacketRepository _db_rpack;
        private readonly ItemRepository _db_item;
        private readonly SmenaRepository _db_smena;
        private readonly GasketRepository _db_gaskets;
        private readonly PartRepository _db_parts;

        public IServiceProvider _scopeFactory;

        public PlcDB(IDataPlcExchange dataPlcExchange, 
                        IOptions<PlcDbReadSettings> plcReadSet, 
                        IOptions<ImSettings> imSet, 
                        IOptions<PlcAlarmSettings> almSet,
                        ILogger<PlcDB> logger,
                        IServiceProvider scopeFactory)
        {
            _imSet = imSet.Value;
            _almSet = almSet.Value;
            _plcReadSet = plcReadSet.Value;
            plcE = dataPlcExchange;
            _logger = logger;
            _scopeFactory = scopeFactory;
            var scope = _scopeFactory.CreateScope();
            _db_rpack = scope.ServiceProvider.GetRequiredService<ResortedPacketRepository>();
            _log = scope.ServiceProvider.GetRequiredService<LogRepository>();
            _db_item = scope.ServiceProvider.GetRequiredService<ItemRepository>();
            _db_smena = scope.ServiceProvider.GetRequiredService<SmenaRepository>();
            _db_gaskets = scope.ServiceProvider.GetRequiredService<GasketRepository>();
            _db_parts = scope.ServiceProvider.GetRequiredService<PartRepository>();

            
            _timer = new Timer
            {
                Enabled = true,
                Interval = 1000
            };
            _timer.Elapsed += OnTimerElapsed;


            plcDtoSet = new PlcDtoSet()
            {
                
                StartNewPack = false, //Начать пересортировку нового пакета
                PackResortedReaded = false, //Пересортированный пакет прочитан
                FinishPackResorting = false, //Принудительно закончить пересортировку пакета
                Enable2Sxema = false,//Собрать пакет 2мя схемами прокладок
                EnableRolling = false,//Раскатать доски пакета на заданную длину
                SettingsWasApplyied = false, //Были применены новые настройки
                CheckPlankL = false,//Проверять длину доски
                FinishDefectPack =false, //Принудительно закончить сборку ПФМ брака
                DefectPacketPushed = false, //Пакет с ПФМ брака забрали
                PackDefectReaded =false, //Пересортированный пакет прочитан
                PackParamChanged = false, //Изменить параметры пакета
                PackReadyRidOff = false, //Пакет можно выкатывать
                QLayersInPack = 0, //Кол-во слоёв в пакете, шт
                VPlanksInPack = 0,  //Объём досок в пакете, м3*1000
                QPlanksInLayer = 0, // Кол-во досок в слое досок пакета, шт
                QPlanksInLayerBrak = 0, //Кол-во досок в слое досок пакета брака, шт
                QPLankLayersBrak = 0, // Кол-во слоёв в пакете брака, шт
                Sxema = 0, // Кол-во слоёв прокладок в пакете, шт
                SxemaNum = 0, // Номер схемы укладок прокладок
                PlankW = 0, // Ширина доски, мм
                PlankT = 0,  //Толщина доски, мм
                Sxema2 = 0,// Cхема 2 укладки прокладок
                Sxema2Qlayers = 0,// Кол-во слоёв схемы 2 укладки прокладок
                SxemaId = 0,// Ключ БД к схеме укладки прокладок
                Sxema2Id = 0,// Ключ БД к 2й схеме укладки прокладок
                PackL =0, // Длина пересобираемого пакета
                PartId =0, // Ключ к пересобираемой партии (на случай перезапуска сервака)
                PlankL = 0, // Проверяемая длина доски, мм
                ItemId = 0, // Ключ к ItemId партии
                SmenaNum = 0, // Номер смены
                SmenaName = "", // Имя смены
                SetNumLayer = 0, //Корректируемое кол-во уложенных слоёв досок, шт
                SetNumPlankInLayer = 0, //Корректируемое кол-во досок в слое, шт
                GeneralSpeed = 50, //Скорость линии
                Counter = 0,
                GasketsBrakPut = false, // Слой прокладок на ПФМ брака положен
                GasketsLayerPeriod =0  //Кол-во слоёв досок между прокладками на ПФМ брака
            };

            plcDto = new PlcDto()
            {
                StartNewPack = false, //Начать пересортировку нового пакета
                PackResortedReaded = false, //Пересортированный пакет прочитан
                FinishPackResorting = false, //Принудительно закончить пересортировку пакета
                Enable2Sxema = false,//Собрать пакет 2мя схемами прокладок
                EnableRolling = false,//Раскатать доски пакета на заданную длину
                SettingsWasApplyied = false, //Были применены новые настройки
                CheckPlankL = false,//Проверять длину доски
                FinishDefectPack = false, //Принудительно закончить сборку ПФМ брака
                DefectPacketPushed = false, //Пакет с ПФМ брака забрали
                PackDefectReaded = false, //Пересортированный пакет прочитан
                PackParamChanged = false, //Изменить параметры пакета
                PackReadyRidOff = false, //Пакет можно выкатывать
                QLayersInPack = 0, //Кол-во слоёв в пакете, шт
                VPlanksInPack = 0,  //Объём досок в пакете, м3*1000
                QPlanksInLayer = 0, // Кол-во досок в слое досок пакета, шт
                QPlanksInLayerBrak = 0, //Кол-во досок в слое досок пакета брака, шт
                QPLankLayersBrak = 0, // Кол-во слоёв в пакете брака, шт
                Sxema = 0, // Кол-во слоёв прокладок в пакете, шт
                SxemaNum = 0, // Номер схемы укладок прокладок
                PlankW = 0, // Ширина доски, мм
                PlankT = 0,  //Толщина доски, мм
                Sxema2 = 0,// Cхема 2 укладки прокладок
                Sxema2Qlayers = 0,// Кол-во слоёв схемы 2 укладки прокладок
                PackL = 0, // Длина пересобираемого пакета
                PartId = 0, // Ключ к пересобираемой партии (на случай перезапуска сервака)
                PlankL = 0, // Проверяемая длина доски, мм
                ItemId = 0, // Ключ к ItemId партии
                SetNumLayer = 0, //Корректируемое кол-во уложенных слоёв досок, шт
                SetNumPlankInLayer = 0, //Корректируемое кол-во досок в слое, шт
                GeneralSpeed = 50, //Скорость линии
                GasketsBrakPut = false, // Слой прокладок на ПФМ брака положен
                GasketsLayerPeriod = 0,  //Кол-во слоёв досок между прокладками на ПФМ брака

                LineInWork = false, // Линия в работе
                PackResorting = false, // Пакет пересортировывается
                PackResorted = false, //  Пакет пересортировался
                RdyStartNewPack = false, // Есть готовность к пересборке нового пакета
                RdyStart = false, //  Есть готовность к запуску линии
                DefectPacketResorted = false, //  Пакет брака пересортировался
                NumLayerActual = 0, // Текущее кол-во уложенных слоёв досок, шт
                NumPlankInLayerActual = 0, //Текущее кол-во уложенных слоёв прокладок, шт
                QPlanksOk = 0, // Текущее кол-во уложенных досок, шт
                QPlanksNotOk = 0, // Текущее кол-во уложенных херовых досок, шт
                VPlanksOk = 0, //Текущий объём уложенных досок, м3*1000
                QPlankBrak = 0, //Кол-во досок в пакете брака, шт
                VLayerOk = 0 // Объём досок на вилах, м3*1000
            };
            
            foreach (var imField in _imSet.im)
            {
                im.Add(new Im()
                {
                    Name = imField.Name,
                    Symbol = imField.Symbol,
                    TypeIM = imField.TypeIM,
                    BtnDU = false, //Включить дистанционный режим
                    BtnAU = false, //Отключить дистанционный режим
                    BtnFwd = false, //Включить вперёд / выдвинуть
                    BtnBwd = false, //Включить назад / задвинуть
                    BtnCmdStop = false, //Остановить
                    SetSpeedDU = 0, //Задание на частотник в ДУ
                    PrmBtnDU_ON = false, //Разрешение на включение дистанционного режима
                    PrmBtnDU_OF = false, //Разрешение на отключение дистанционного режима
                    PRMbtn_Back_ON = false, //Разрешение кнопки включения заднего хода
                    PRMbtn_Back_OFF = false, //Разрешение кнопки отключения заднего хода
                    PrmBtnCmdStop = false, //Разрешение на остановку
                    Ready = false, //Готовность к работе
                    ON = false, //В работе
                    OFF = false, //Не в работе
                    Fault = false, //Обобщенная неисправность
                    Alarm = false, //Авария
                    AU = false, //Автоматическое управление
                    MU = false, //Местное управление
                    DU = false, //Дистанционный режим включен
                    CMD_ON = false, //Запустить ON
                    CMD_OFF = false, //Запустить OFF
                    BACK = false, //Задний ход
                    SetSpeed = 0 //    Задание на частотник
                }) ;
            }
                        
            for (int i = 0; i < _almSet.Alarms.Count(); i++)
            {
                alarms.Add(new Alarm {
                    ON = false,
                    TimeON = DateTime.Now,
                    AlmType = _almSet.Alarms[i].AlmType,
                    Description = _almSet.Alarms[i].Description,
                    Number = _almSet.Alarms[i].Number
                });

            }

            if (ReadPlcDb()==0) ReadSettingsFromPlc();
            plcDtoSet.SmenaNum = _db_smena.getLastSmenaNum().Result;
            plcDtoSet.RpackNumber = _db_rpack.getLastPacketNum().Result;
            UpdateSettings(plcDtoSet);

        }

        public int ReadPlcDb()
        {
            try
            {
                var ReadedBytes = plcE.Read_DB(_plcReadSet.DbNum, _plcReadSet.BytesToRead + _almSet.AlarmsSize, _plcReadSet.StartAddr);
                if (ReadedBytes != null)
                {
                    plcDto.StartNewPack = ReadedBytes[0].SelectBit(0); //Начать пересортировку нового пакета
                    plcDto.PackResortedReaded = ReadedBytes[0].SelectBit(1); //Пересортированный пакет прочитан
                    plcDto.FinishPackResorting = ReadedBytes[0].SelectBit(2); //Принудительно закончить пересортировку пакета
                    plcDto.Enable2Sxema = ReadedBytes[0].SelectBit(3); //Собрать пакет 2мя схемами прокладок
                    plcDto.EnableRolling = ReadedBytes[0].SelectBit(4); //Раскатать доски пакета на заданную длину
                    plcDto.SettingsWasApplyied = ReadedBytes[0].SelectBit(5); //Были применены новые настройки
                    plcDto.CheckPlankL = ReadedBytes[0].SelectBit(6); //Проверять длину доски
                    plcDto.FinishDefectPack = ReadedBytes[0].SelectBit(7);//Принудительно закончить сборку ПФМ брака
                    plcDto.DefectPacketPushed = ReadedBytes[1].SelectBit(0);//Пакет с ПФМ брака забрали
                    plcDto.PackDefectReaded = ReadedBytes[1].SelectBit(1); //Пересортированный пакет прочитан
                    plcDto.PackParamChanged = ReadedBytes[1].SelectBit(2); //Изменить параметры пакета
                    plcDto.PackReadyRidOff = ReadedBytes[1].SelectBit(3); //Пакет можно выкатывать
                    plcDto.QLayersInPack = S7.Net.Types.Int.FromByteArray(ReadedBytes.Skip(2).Take(2).ToArray()); //Кол-во слоёв в пакете, шт
                    plcDto.VPlanksInPack = (float)((S7.Net.Types.Int.FromByteArray(ReadedBytes.Skip(4).Take(2).ToArray())) * 0.001); //Объём досок в пакете, м3*1000
                    plcDto.QPlanksInLayer = S7.Net.Types.Int.FromByteArray(ReadedBytes.Skip(6).Take(2).ToArray()); // Кол-во досок в слое досок пакета, шт
                    plcDto.QPlanksInLayerBrak = S7.Net.Types.Int.FromByteArray(ReadedBytes.Skip(8).Take(2).ToArray()); //Кол-во досок в слое досок пакета брака, шт
                    plcDto.QPLankLayersBrak = S7.Net.Types.Int.FromByteArray(ReadedBytes.Skip(10).Take(2).ToArray()); //Кол-во слоёв в пакете брака, шт
                    plcDto.Sxema = S7.Net.Types.DInt.FromByteArray(ReadedBytes.Skip(12).Take(4).ToArray()); // Схема укладки прокладок
                    plcDto.SxemaNum = S7.Net.Types.Int.FromByteArray(ReadedBytes.Skip(16).Take(2).ToArray()); // Номер схемы укладок прокладок
                    plcDto.PlankW = S7.Net.Types.Int.FromByteArray(ReadedBytes.Skip(18).Take(2).ToArray()); // Ширина доски, мм
                    plcDto.PlankT = S7.Net.Types.Int.FromByteArray(ReadedBytes.Skip(20).Take(2).ToArray());  //Толщина доски, мм
                    plcDto.Sxema2 = S7.Net.Types.DInt.FromByteArray(ReadedBytes.Skip(22).Take(4).ToArray()); // Cхема 2 укладки прокладок
                    plcDto.Sxema2Qlayers = S7.Net.Types.Int.FromByteArray(ReadedBytes.Skip(26).Take(2).ToArray()); // Кол-во слоёв схемы 2 укладки прокладок
                    plcDto.PackL = S7.Net.Types.Int.FromByteArray(ReadedBytes.Skip(28).Take(2).ToArray()); // Длина пересобираемого пакета
                    plcDto.PartId = S7.Net.Types.DInt.FromByteArray(ReadedBytes.Skip(30).Take(4).ToArray()); // Ключ к пересобираемому пакету (на случай перезапуска сервака)
                    plcDto.PlankL = S7.Net.Types.Int.FromByteArray(ReadedBytes.Skip(34).Take(2).ToArray()); // Длина доски, мм
                    plcDto.ItemId = S7.Net.Types.DInt.FromByteArray(ReadedBytes.Skip(36).Take(4).ToArray()); // Ключ к ItemId партии
                    plcDto.SetNumLayer = S7.Net.Types.Int.FromByteArray(ReadedBytes.Skip(40).Take(2).ToArray()); //Корректируемое кол-во уложенных слоёв досок, шт
                    plcDto.SetNumPlankInLayer = S7.Net.Types.Int.FromByteArray(ReadedBytes.Skip(42).Take(2).ToArray()); //Корректируемое кол-во досок в слое, шт
                    plcDto.GeneralSpeed = S7.Net.Types.Int.FromByteArray(ReadedBytes.Skip(44).Take(2).ToArray()); //Скорость линии
                    plcDto.TotalPlanksInPack = S7.Net.Types.Int.FromByteArray(ReadedBytes.Skip(46).Take(2).ToArray()); //Кол-во досок в пакете
                    plcDto.CheckL = S7.Net.Types.Int.FromByteArray(ReadedBytes.Skip(50).Take(2).ToArray()); //Проверяемая длина доски, мм
                    
                    plcDto.GasketsBrakPut = ReadedBytes[52].SelectBit(0); //Слой прокладок на ПФМ брака положен
                    plcDto.GasketsLayerPeriod = S7.Net.Types.Int.FromByteArray(ReadedBytes.Skip(54).Take(2).ToArray()); //Кол-во слоёв досок между прокладками на ПФМ брака


                    plcDto.LineInWork = ReadedBytes[_plcReadSet.StatusOffset].SelectBit(0); // Линия в работе
                    plcDto.PackResorting = ReadedBytes[_plcReadSet.StatusOffset].SelectBit(1); // Пакет пересортировывается
                    plcDto.PackResorted = ReadedBytes[_plcReadSet.StatusOffset].SelectBit(2); //  Пакет пересортировался
                    plcDto.RdyStartNewPack = ReadedBytes[_plcReadSet.StatusOffset].SelectBit(4); // Есть готовность к пересборке нового пакета
                    plcDto.RdyStart = ReadedBytes[_plcReadSet.StatusOffset].SelectBit(5); //  Есть готовность к запуску линии
                    plcDto.DefectPacketResorted = ReadedBytes[_plcReadSet.StatusOffset].SelectBit(6);//Пакет брака пересортировался
                    plcDto.NumLayerActual = S7.Net.Types.Int.FromByteArray(ReadedBytes.Skip(_plcReadSet.StatusOffset + 2).Take(2).ToArray()); // Текущее кол-во уложенных слоёв досок, шт
                    plcDto.NumPlankInLayerActual = S7.Net.Types.Int.FromByteArray(ReadedBytes.Skip(_plcReadSet.StatusOffset + 4).Take(2).ToArray()); //Текущее кол-во уложенных слоёв прокладок, шт
                    plcDto.QPlanksOk = S7.Net.Types.Int.FromByteArray(ReadedBytes.Skip(_plcReadSet.StatusOffset + 6).Take(2).ToArray()); // Текущее кол-во уложенных досок, шт
                    plcDto.QPlanksNotOk = S7.Net.Types.Int.FromByteArray(ReadedBytes.Skip(_plcReadSet.StatusOffset + 8).Take(2).ToArray()); // Текущее кол-во уложенных херовых досок, шт
                    plcDto.VPlanksOk = S7.Net.Types.DInt.FromByteArray(ReadedBytes.Skip(_plcReadSet.StatusOffset + 10).Take(4).ToArray()); //Текущий объём уложенных досок, м3*1000
                    plcDto.QPlankBrak = S7.Net.Types.Int.FromByteArray(ReadedBytes.Skip(_plcReadSet.StatusOffset + 14).Take(2).ToArray()); //Кол-во досок в пакете брака, шт
                    plcDto.VLayerOk = S7.Net.Types.DInt.FromByteArray(ReadedBytes.Skip(_plcReadSet.StatusOffset + 16).Take(4).ToArray()); //Объём досок на вилах, м3*1000

                    for (int i = 0; i < _imSet.im.Count; i++)
                    {

                        im[i].BtnDU = ReadedBytes[_plcReadSet.ImOffset + _plcReadSet.ServIMbytes * i + 0].SelectBit(0); ; //Включить дистанционный режим
                        im[i].BtnAU = ReadedBytes[_plcReadSet.ImOffset + _plcReadSet.ServIMbytes * i + 0].SelectBit(1); //Отключить дистанционный режим
                        im[i].BtnFwd = ReadedBytes[_plcReadSet.ImOffset + _plcReadSet.ServIMbytes * i + 0].SelectBit(2); //Включить вперёд / выдвинуть
                        im[i].BtnBwd = ReadedBytes[_plcReadSet.ImOffset + _plcReadSet.ServIMbytes * i + 0].SelectBit(3); //Включить назад / задвинуть
                        im[i].BtnCmdStop = ReadedBytes[_plcReadSet.ImOffset + _plcReadSet.ServIMbytes * i + 0].SelectBit(4); //Остановить
                        im[i].SetSpeedDU = S7.Net.Types.Double.FromByteArray(ReadedBytes.Skip(_plcReadSet.ImOffset + _plcReadSet.ServIMbytes * i + 2).Take(4).ToArray()); //Задание на частотник в ДУ
                        im[i].PrmBtnDU_ON = ReadedBytes[_plcReadSet.ImOffset + _plcReadSet.ServIMbytes * i + 6].SelectBit(0); //Разрешение на включение дистанционного режима
                        im[i].PrmBtnDU_OF = ReadedBytes[_plcReadSet.ImOffset + _plcReadSet.ServIMbytes * i + 6].SelectBit(1); //Разрешение на отключение дистанционного режима
                        im[i].PRMbtn_Back_ON = ReadedBytes[_plcReadSet.ImOffset + _plcReadSet.ServIMbytes * i + 6].SelectBit(2); //Разрешение кнопки включения заднего хода
                        im[i].PRMbtn_Back_OFF = ReadedBytes[_plcReadSet.ImOffset + _plcReadSet.ServIMbytes * i + 6].SelectBit(3); //Разрешение кнопки отключения заднего хода
                        im[i].PrmBtnCmdStop = ReadedBytes[_plcReadSet.ImOffset + _plcReadSet.ServIMbytes * i + 6].SelectBit(4); //Разрешение на остановку
                        im[i].Ready = ReadedBytes[_plcReadSet.ImOffset + _plcReadSet.ServIMbytes * i + 6].SelectBit(5); //Готовность к работе
                        im[i].ON = ReadedBytes[_plcReadSet.ImOffset + _plcReadSet.ServIMbytes * i + 6].SelectBit(6); //В работе
                        im[i].OFF = ReadedBytes[_plcReadSet.ImOffset + _plcReadSet.ServIMbytes * i + 6].SelectBit(7); //Не в работе
                        im[i].Fault = ReadedBytes[_plcReadSet.ImOffset + _plcReadSet.ServIMbytes * i + 7].SelectBit(0); //Обобщенная неисправность
                        im[i].Alarm = ReadedBytes[_plcReadSet.ImOffset + _plcReadSet.ServIMbytes * i + 7].SelectBit(1); //Авария
                        im[i].AU = ReadedBytes[_plcReadSet.ImOffset + _plcReadSet.ServIMbytes * i + 7].SelectBit(2); //Автоматическое управление
                        im[i].MU = ReadedBytes[_plcReadSet.ImOffset + _plcReadSet.ServIMbytes * i + 7].SelectBit(3); //Местное управление
                        im[i].DU = ReadedBytes[_plcReadSet.ImOffset + _plcReadSet.ServIMbytes * i + 7].SelectBit(4); //Дистанционный режим включен
                        im[i].CMD_ON = ReadedBytes[_plcReadSet.ImOffset + _plcReadSet.ServIMbytes * i + 7].SelectBit(5); //Запустить ON
                        im[i].CMD_OFF = ReadedBytes[_plcReadSet.ImOffset + _plcReadSet.ServIMbytes * i + 7].SelectBit(6); //Запустить OFF
                        im[i].BACK = ReadedBytes[_plcReadSet.ImOffset + _plcReadSet.ServIMbytes * i + 7].SelectBit(7); //Задний ход
                        im[i].SetSpeed = S7.Net.Types.Double.FromByteArray(ReadedBytes.Skip(_plcReadSet.ImOffset + _plcReadSet.ServIMbytes * i + 8).Take(4).ToArray()); //    Задание на частотник

                    }

                    foreach (var a in alarms.Where(x=>x.AlmType == "PLC").ToList())
                    {
                        if (!a.ON) a.TimeON = DateTime.Now;
                        a.ON = ReadedBytes[_almSet.AlarmsOffset + a.Number / 8].SelectBit(a.Number % 8);
                    }

                    return 0;
                }
                _logger.LogInformation("Null readed");
                return 1;
            }
            catch (Exception ee)
            {
                _logger.LogInformation("---------------> Exception event:" + ee);
                return 1;
            }
        }

        public void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                _timer.Stop();
                _scanTime = DateTime.Now - _lastScanTime;

                plcDtoSet.Counter++;
                _logger.LogInformation("======================================Cycle № " + plcDtoSet.Counter + ": " + DateTime.Now.ToString("s"));

                GetConnectAlarm(ReadPlcDb());
                PlcScanAsync();

            }
            catch (Exception ee)
            {
                _logger.LogInformation("Вышло исключение: " + ee.ToString() + " " + DateTime.Now.ToString("s"));
            }
            finally
            {
                _timer.Start();
            }
            _lastScanTime = DateTime.Now;
        }

        private void GetConnectAlarm(int status)
        {
            if (status == 1)
            {
                //plc.ConnectPlc();
                tryCounter++;
                if (tryCounter > 1)
                {
                    tryCounter = 1;
                    SetAlarmNumber(1000, true);
                }

            }
            else
            {
                if (tryCounter != 0) SetAlarmNumber(1000, false);
                tryCounter = 0;
            }
        }

        public void PlcScanAsync()
        {

            foreach (var a in alarms)
            {
                if (a.ON && !a.ON_old)
                {
                    _log.CreateAsync(new LogModel()
                    {
                        Message = a.Description,
                        LogType = "Авария",
                        User = "Система",
                        LogTime = DateTime.Now,
                        ON_OFF_text = "Пришло"
                    }).Wait();
                }

                if (!a.ON && a.ON_old)
                {
                    _log.CreateAsync(new LogModel()
                    {
                        Message = a.Description,
                        LogType = "Авария",
                        User = "Система",
                        LogTime = DateTime.Now,
                        ON_OFF_text = "Ушло"
                    }).Wait();
                }
                a.ON_old = a.ON;
            }

            //Incorrect settings in PLC
            var res = (plcDto.ItemId < 0 
                    || plcDto.PartId == 0
                    || _db_parts.GetByIdAsync(plcDto.PartId).Result == null && plcDto.PartId > 0
                    || _db_item.GetByIdAsync(plcDto.ItemId).Result == null
                    || _db_gaskets.GetBySxema(plcDto.Sxema).Result == null
                    || _db_gaskets.GetBySxema(plcDto.Sxema2).Result == null && plcDto.Enable2Sxema
                    ) ? SetAlarmNumber(1001, true) : SetAlarmNumber(1001, false);

            //Different packs creation
            if ((plcDto.PackResorted || plcDto.DefectPacketResorted || plcDtoSet.GetPackSrez) && plcDto.ItemId >= 0)
            {
                CreateResortedPacket();
                if (plcDtoSet.GetPackSrez) GetPackSrez(false);
            }

            //Smena not active
            SetAlarmNumber(1003, plcDtoSet.SmenaActive ? false : true);



        }
        private Task CreateResortedPacket()
        {
            var item = _db_item.GetByIdAsync(plcDtoSet.ItemId).Result;

            var rpack = new DResortedPacket()
            {
                PackDate = DateTime.Now,
                SmenaNum = plcDtoSet.SmenaNum,
                SmenaName = plcDtoSet.SmenaName,
                PartId = plcDto.PartId,
                PacketNumber = plcDtoSet.RpackNumber,
                isDefected = plcDto.DefectPacketResorted ? true : false,
            };

            if (plcDto.DefectPacketResorted)
            {
                rpack.TotalPlanks = plcDto.QPlanksNotOk;
                rpack.TotalVolume = rpack.TotalPlanks * (float)(item.Length * item.Thick * item.Width * 0.000000001);
                PackDefectedReaded();
            }

            if (plcDto.PackResorted)
            {
                rpack.TotalPlanks = plcDto.QPlanksOk - plcDtoSet.QplankRemembered;
                rpack.TotalVolume = (item.MixedLength == "Yes") ? (float)(plcDto.VPlanksOk * 0.0000001) - plcDtoSet.VplankRemembered : rpack.TotalPlanks * (float)(item.Length * item.Thick * item.Width * 0.000000001);
                PackResortedReaded();
                SaveSrez(0, 0);
            }

            if (plcDtoSet.GetPackSrez)
            {
                rpack.TotalPlanks = plcDto.NumLayerActual * plcDto.QPlanksInLayer + plcDto.NumPlankInLayerActual - plcDtoSet.QplankRemembered;
                rpack.TotalVolume = (item.MixedLength == "Yes") ? (float)((plcDto.VPlanksOk + plcDto.VLayerOk) * 0.0000001) - plcDtoSet.VplankRemembered : rpack.TotalPlanks * (float)(item.Length * item.Thick * item.Width * 0.000000001);
                SaveSrez(rpack.TotalPlanks + plcDtoSet.QplankRemembered, plcDtoSet.VplankRemembered + rpack.TotalVolume);
            }


            try
            {
                if (plcDto.PartId > 0)
                {
                    _db_rpack.CreateAsync(rpack).Wait();
                    var pack = _db_rpack.GetByIdAsync(rpack.Id).Result;

                    _log.CreateAsync(new LogModel()
                    {
                        Message = "Создан пересортированный пакет номер " + pack.PacketNumber + " в партии номер " + pack.Part.PartNumber,
                        LogType = "Тех. процесс",
                        User = "Система",
                        LogTime = DateTime.Now,
                        ON_OFF_text = "Пришло"
                    }).Wait();
                    IncreaseRPackNumber();
                }
                else
                {
                    _log.CreateAsync(new LogModel()
                    {
                        Message = "Создан сухой пакет без партии",
                        LogType = "Тех. процесс",
                        User = "Система",
                        LogTime = DateTime.Now,
                        ON_OFF_text = "Пришло"
                    }).Wait();
                }
                return Task.CompletedTask;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public int StartNewPack(bool smenaActive)
        {
            return plcE.WriteBit(_plcReadSet.DbNum, 0, 0, smenaActive ? 1 : 0);
        }
        public int FinishPack()
        {
            return plcE.WriteBit(_plcReadSet.DbNum, 0, 2, 1);
        }

        public int PackResortedReaded()
        {
            return plcE.WriteBit(_plcReadSet.DbNum, 0, 1, 1);
        }

        public int PackDefectedReaded()
        {
            return plcE.WriteBit(_plcReadSet.DbNum, 1, 1, 1);
        }

        public int FinishDefectedPack()
        {
            return plcE.WriteBit(_plcReadSet.DbNum, 0, 7, 1);
        }
        public int DefectedPackPushed()
        {
            return plcE.WriteBit(_plcReadSet.DbNum, 1, 0, 1);
        }

        public int PackReadyRidOff()
        {
            return plcE.WriteBit(_plcReadSet.DbNum, 1, 3, 1);
        }

        public int GasketRidedOff()
        {
            return plcE.WriteBit(_plcReadSet.DbNum, 1, 4, 1);
        }
        
        public int AddGasketsLayer()
        {
            return plcE.WriteBit(_plcReadSet.DbNum, 1, 6, 1);
        }
                
        public int GasketsBrakPut()
        {
            return plcE.WriteBit(_plcReadSet.DbNum, 52, 0, 1);
        }

                
        public int PackBrakParamChanged(short SetNumPlankInLayerBrak)
        {
            
            byte[] WriteBytes = new byte[2];
            BitConverter.GetBytes(SetNumPlankInLayerBrak).Reverse().ToArray().CopyTo(WriteBytes, 0);
            plcE.Write_DB(_plcReadSet.DbNum, 48, WriteBytes);
            return plcE.WriteBit(_plcReadSet.DbNum, 1, 7, 1);
        }





        public async Task<PlcDto> GetPlcDBAsync()
        {
            return await Task.FromResult(plcDto);
        }

        public async Task<PlcDtoSet> GetPlcSetDBAsync()
        {
            return await Task.FromResult(plcDtoSet);
        }

        public async Task<IEnumerable<Im>> GetPlcDbImsAsync()
        {
            return await Task.FromResult(im.ToList());
        }

        public int SetPlcDB(PlcDtoSet plcDtoSetFromBrowser)
        {

            plcDtoSet.StartNewPack = plcDtoSetFromBrowser.StartNewPack; //Начать пересортировку нового пакета
            plcDtoSet.PackResortedReaded = plcDtoSetFromBrowser.PackResortedReaded; //Пересортированный пакет прочитан
            plcDtoSet.FinishPackResorting = plcDtoSetFromBrowser.FinishPackResorting; //Принудительно закончить пересортировку пакета
            plcDtoSet.Enable2Sxema = plcDtoSetFromBrowser.Enable2Sxema;//Собрать пакет 2мя схемами прокладок
            //plcDtoSet.SettingsWasApplyied = true; //Были применены новые настройки
            plcDtoSet.CheckPlankL = plcDtoSetFromBrowser.CheckPlankL; //Проверять длину доски
            plcDtoSet.FinishDefectPack = plcDtoSetFromBrowser.FinishDefectPack;//Принудительно закончить сборку ПФМ брака
            plcDtoSet.DefectPacketPushed = plcDtoSetFromBrowser.DefectPacketPushed;//Пакет с ПФМ брака забрали
            plcDtoSet.QLayersInPack = plcDtoSetFromBrowser.QLayersInPack; //Кол-во слоёв в пакете, шт
            plcDtoSet.VPlanksInPack = plcDtoSetFromBrowser.VPlanksInPack;  //Объём досок в пакете, м3*1000
            plcDtoSet.QPlanksInLayer = plcDtoSetFromBrowser.QPlanksInLayer; // Кол-во досок в слое досок пакета, шт
            plcDtoSet.QPlanksInLayerBrak = plcDtoSetFromBrowser.QPlanksInLayerBrak; //Кол-во досок в слое досок пакета брака, шт
            plcDtoSet.QPLankLayersBrak = plcDtoSetFromBrowser.QPLankLayersBrak; //Кол-во слоёв в пакете брака, шт
            plcDtoSet.Sxema = plcDtoSetFromBrowser.Sxema; // Кол-во слоёв прокладок в пакете, шт
            plcDtoSet.SxemaNum = plcDtoSetFromBrowser.SxemaNum; // Номер схемы укладок прокладок
            plcDtoSet.PlankW = plcDtoSetFromBrowser.PlankW; // Ширина доски, мм
            plcDtoSet.PlankT = plcDtoSetFromBrowser.PlankT;  //Толщина доски, мм
            plcDtoSet.Sxema2 = plcDtoSetFromBrowser.Sxema2;// Cхема 2 укладки прокладок
            plcDtoSet.Sxema2Qlayers = plcDtoSetFromBrowser.Sxema2Qlayers;// Кол-во слоёв схемы 2 укладки прокладок
            plcDtoSet.SxemaId = plcDtoSetFromBrowser.SxemaId;// Ключ БД к схеме укладки прокладок
            plcDtoSet.Sxema2Id = plcDtoSetFromBrowser.Sxema2Id;// Ключ БД к 2й схеме укладки прокладок
            plcDtoSet.EnableRolling = plcDtoSetFromBrowser.EnableRolling;//Раскатать доски пакета на заданную длину
            plcDtoSet.PackL = plcDtoSetFromBrowser.PackL; // Длина пересобираемого пакета
            plcDtoSet.PartId = plcDtoSetFromBrowser.PartId; // Ключ к пересобираемой партии (на случай перезапуска сервака)
            plcDtoSet.PlankL = plcDtoSetFromBrowser.PlankL; // Длина доски, мм
            plcDtoSet.ItemId = plcDtoSetFromBrowser.ItemId; // Ключ к ItemId партии
            plcDtoSet.GeneralSpeed = plcDtoSetFromBrowser.GeneralSpeed; //Скорость линии
            //plcDtoSet.SetNumLayer = plcDtoSetFromBrowser.SetNumLayer; //Корректируемое кол-во уложенных слоёв досок, шт
            //plcDtoSet.SetNumPlankInLayer = plcDtoSetFromBrowser.SetNumPlankInLayer; //Корректируемое кол-во досок в слое, шт
            plcDtoSet.CheckL = plcDtoSetFromBrowser.CheckL; // Проверяемая длина доски, мм
            plcDtoSet.PackToPack = plcDtoSetFromBrowser.PackToPack; 
            plcDtoSet.TotalPlanksInPack = plcDtoSetFromBrowser.TotalPlanksInPack;
            plcDtoSet.GasketsBrakPut = plcDtoSetFromBrowser.GasketsBrakPut; //Слой прокладок на ПФМ брака положен
            plcDtoSet.GasketsLayerPeriod = plcDtoSetFromBrowser.GasketsLayerPeriod; //Кол-во слоёв досок между прокладками на ПФМ брака

            byte[] WriteBytes = new byte[_plcReadSet.StatusOffset];
            BitArray bitArray = new BitArray(16);

            bitArray[0] = plcDtoSetFromBrowser.StartNewPack;
            bitArray[1] = plcDtoSetFromBrowser.PackResortedReaded;
            bitArray[2] = plcDtoSetFromBrowser.FinishPackResorting;
            bitArray[3] = plcDtoSetFromBrowser.Enable2Sxema;
            bitArray[4] = plcDtoSetFromBrowser.EnableRolling;
            bitArray[5] = true;// plcDtoSetFromBrowser.SettingsWasApplyied;
            bitArray[6] = plcDtoSetFromBrowser.CheckPlankL;
            bitArray[7] = plcDtoSetFromBrowser.FinishDefectPack;
            bitArray[8] = plcDtoSetFromBrowser.DefectPacketPushed;
            bitArray[9] = plcDtoSetFromBrowser.PackDefectReaded;
            bitArray[10] = plcDtoSetFromBrowser.PackParamChanged;
            bitArray[11] = plcDtoSetFromBrowser.PackReadyRidOff;
            bitArray[12] = plcDtoSetFromBrowser.GasketsRidedOff;
            bitArray[13] = plcDtoSetFromBrowser.PackToPack;

            bitArray.CopyTo(WriteBytes,  0);

            BitConverter.GetBytes(plcDtoSetFromBrowser.QLayersInPack).Reverse().ToArray().CopyTo(WriteBytes, 2);
            BitConverter.GetBytes((short)(plcDtoSetFromBrowser.VPlanksInPack*1000)).Reverse().ToArray().CopyTo(WriteBytes, 4);
            BitConverter.GetBytes(plcDtoSetFromBrowser.QPlanksInLayer).Reverse().ToArray().CopyTo(WriteBytes, 6);
            BitConverter.GetBytes(plcDtoSetFromBrowser.QPlanksInLayerBrak).Reverse().ToArray().CopyTo(WriteBytes, 8);
            BitConverter.GetBytes(plcDtoSetFromBrowser.QPLankLayersBrak).Reverse().ToArray().CopyTo(WriteBytes, 10);
            BitConverter.GetBytes(plcDtoSetFromBrowser.Sxema).Reverse().ToArray().CopyTo(WriteBytes, 12);
            BitConverter.GetBytes(plcDtoSetFromBrowser.SxemaNum).Reverse().ToArray().CopyTo(WriteBytes, 16);
            BitConverter.GetBytes(plcDtoSetFromBrowser.PlankW).Reverse().ToArray().CopyTo(WriteBytes, 18);
            BitConverter.GetBytes(plcDtoSetFromBrowser.PlankT).Reverse().ToArray().CopyTo(WriteBytes, 20);
            BitConverter.GetBytes(plcDtoSetFromBrowser.Sxema2).Reverse().ToArray().CopyTo(WriteBytes, 22);
            BitConverter.GetBytes(plcDtoSetFromBrowser.Sxema2Qlayers).Reverse().ToArray().CopyTo(WriteBytes, 26);
            BitConverter.GetBytes(plcDtoSetFromBrowser.PackL).Reverse().ToArray().CopyTo(WriteBytes, 28);
            BitConverter.GetBytes(plcDtoSetFromBrowser.PartId).Reverse().ToArray().CopyTo(WriteBytes, 30);
            BitConverter.GetBytes(plcDtoSetFromBrowser.PlankL).Reverse().ToArray().CopyTo(WriteBytes, 34);
            BitConverter.GetBytes(plcDtoSetFromBrowser.ItemId).Reverse().ToArray().CopyTo(WriteBytes, 36);
            //BitConverter.GetBytes(plcDtoSetFromBrowser.SetNumLayer).Reverse().ToArray().CopyTo(WriteBytes, 40);
            //BitConverter.GetBytes(plcDtoSetFromBrowser.SetNumPlankInLayer).Reverse().ToArray().CopyTo(WriteBytes, 42);
            BitConverter.GetBytes(plcDtoSetFromBrowser.GeneralSpeed).Reverse().ToArray().CopyTo(WriteBytes, 44);
            BitConverter.GetBytes(plcDtoSetFromBrowser.TotalPlanksInPack).Reverse().ToArray().CopyTo(WriteBytes, 46);
            BitConverter.GetBytes(plcDtoSetFromBrowser.CheckL).Reverse().ToArray().CopyTo(WriteBytes, 50);


            //BitArray bitArray2 = new BitArray(16);
            //bitArray2[0] = plcDtoSetFromBrowser.GasketsBrakPut;
            //bitArray2.CopyTo(WriteBytes, 52);

            BitConverter.GetBytes(plcDtoSetFromBrowser.GasketsLayerPeriod).Reverse().ToArray().CopyTo(WriteBytes, 54);

            return plcE.Write_DB(_plcReadSet.DbNum, _plcReadSet.StartAddr, WriteBytes);
        }

        public int SendPackParam(short SetNumLayer, short SetNumPlankInLayer)
        {
            byte[] WriteBytes = new byte[4];
            BitConverter.GetBytes(SetNumLayer).Reverse().ToArray().CopyTo(WriteBytes, 0);
            BitConverter.GetBytes(SetNumPlankInLayer).Reverse().ToArray().CopyTo(WriteBytes, 2);
            return plcE.Write_DB(_plcReadSet.DbNum, 40, WriteBytes);
        }
        public int SendPackParamApply()
        {
            return plcE.WriteBit(_plcReadSet.DbNum, 1, 2, 1);
        }


        public async Task<int> ApplySpeedSettings(short speed)
        {
            plcDtoSet.GeneralSpeed = speed; //Скорость линии
            byte[] wrBytes = new byte[2];
            BitConverter.GetBytes(speed).Reverse().ToArray().CopyTo(wrBytes, 0);
            return plcE.Write_DB(_plcReadSet.DbNum, 44, wrBytes);
        }

        public void IncreaseRPackNumber()
        {
            plcDtoSet.RpackNumber++;
        }

        public void IncreaseDryPackNumber()
        {
            plcDtoSet.DryPackNumber++;
        }

        public void ReadSettingsFromPlc()
        {
            plcDtoSet.StartNewPack = plcDto.StartNewPack; //Начать пересортировку нового пакета
            plcDtoSet.PackResortedReaded = plcDto.PackResortedReaded; //Пересортированный пакет прочитан
            plcDtoSet.FinishPackResorting = plcDto.FinishPackResorting; //Принудительно закончить пересортировку пакета
            plcDtoSet.Enable2Sxema = plcDto.Enable2Sxema;//Собрать пакет 2мя схемами прокладок
            plcDtoSet.EnableRolling = plcDto.EnableRolling;//Раскатать доски пакета на заданную длину
            plcDtoSet.SettingsWasApplyied = plcDto.SettingsWasApplyied; //Были применены новые настройки
            plcDtoSet.CheckPlankL = plcDto.CheckPlankL; //Проверять длину доски
            plcDtoSet.FinishDefectPack = plcDto.FinishDefectPack;//Принудительно закончить сборку ПФМ брака
            plcDtoSet.DefectPacketPushed = plcDto.DefectPacketPushed;//Пакет с ПФМ брака забрали
            plcDtoSet.PackDefectReaded = plcDto.PackDefectReaded;//Пересортированный пакет прочитан
            plcDtoSet.PackParamChanged = plcDto.PackParamChanged;//Изменить параметры пакета
            plcDtoSet.PackToPack = plcDto.PackToPack; //Пакет в пакет
            plcDtoSet.QLayersInPack = plcDto.QLayersInPack; //Кол-во слоёв в пакете, шт
            plcDtoSet.VPlanksInPack = plcDto.VPlanksInPack;  //Объём досок в пакете, м3*1000
            plcDtoSet.QPlanksInLayer = plcDto.QPlanksInLayer; // Кол-во досок в слое досок пакета, шт
            plcDtoSet.QPlanksInLayerBrak = plcDto.QPlanksInLayerBrak; //Кол-во досок в слое досок пакета брака, шт
            plcDtoSet.QPLankLayersBrak = plcDto.QPLankLayersBrak; //Кол-во слоёв в пакете брака, шт
            plcDtoSet.Sxema = plcDto.Sxema; // Кол-во слоёв прокладок в пакете, шт
            plcDtoSet.SxemaNum = plcDto.SxemaNum; // Номер схемы укладок прокладок
            plcDtoSet.PlankW = plcDto.PlankW; // Ширина доски, мм
            plcDtoSet.PlankT = plcDto.PlankT;  //Толщина доски, мм
            plcDtoSet.Sxema2 = plcDto.Sxema2;// Cхема 2 укладки прокладок
            plcDtoSet.Sxema2Qlayers = plcDto.Sxema2Qlayers;// Кол-во слоёв схемы 2 укладки прокладок
            plcDtoSet.PackL = plcDto.PackL; // Длина пересобираемого пакета
            plcDtoSet.PartId = plcDto.PartId; // Ключ к пересобираемой партии (на случай перезапуска сервака)
            plcDtoSet.PlankL = plcDto.PlankL; // Проверяемая длина доски, мм
            plcDtoSet.ItemId = plcDto.ItemId; // Ключ к ItemId партии
            plcDtoSet.SetNumLayer = plcDto.SetNumLayer; //Корректируемое кол-во уложенных слоёв досок, шт
            plcDtoSet.SetNumPlankInLayer = plcDto.SetNumPlankInLayer; //Корректируемое кол-во досок в слое, шт
            plcDtoSet.GeneralSpeed = plcDto.GeneralSpeed; //Корректируемое кол-во досок в слое, шт
            plcDtoSet.TotalPlanksInPack = plcDto.TotalPlanksInPack; //Кол-во досок в пакете
            plcDtoSet.CheckL = plcDto.CheckL; // Проверяемая длина доски, мм

            var gask = _db_gaskets.GetBySxema(plcDtoSet.Sxema).Result;
            if (gask != null) plcDtoSet.SxemaId = gask.Id;
            gask = _db_gaskets.GetBySxema(plcDtoSet.Sxema2).Result;
            if (gask != null) plcDtoSet.Sxema2Id = gask.Id;


            //plcDtoSet.GasketsBrakPut = plcDto.GasketsBrakPut; //Слой прокладок на ПФМ брака положен
            plcDtoSet.GasketsLayerPeriod = plcDto.GasketsLayerPeriod; //Кол-во слоёв досок между прокладками на ПФМ брака

        }

        public void UpdateSettings(PlcDtoSet dtoSet)
        {
            plcDtoSet.StartNewPack = dtoSet.StartNewPack; //Начать пересортировку нового пакета
            plcDtoSet.PackResortedReaded = dtoSet.PackResortedReaded; //Пересортированный пакет прочитан
            plcDtoSet.FinishPackResorting = dtoSet.FinishPackResorting; //Принудительно закончить пересортировку пакета
            plcDtoSet.Enable2Sxema = dtoSet.Enable2Sxema;//Собрать пакет 2мя схемами прокладок
            plcDtoSet.EnableRolling = dtoSet.EnableRolling;//Раскатать доски пакета на заданную длину
            plcDtoSet.SettingsWasApplyied = dtoSet.SettingsWasApplyied; //Были применены новые настройки
            plcDtoSet.CheckPlankL = dtoSet.CheckPlankL; //Проверять длину доски
            plcDtoSet.FinishDefectPack = dtoSet.FinishDefectPack;//Принудительно закончить сборку ПФМ брака
            plcDtoSet.DefectPacketPushed = dtoSet.DefectPacketPushed;//Пакет с ПФМ брака забрали
            plcDtoSet.PackDefectReaded = dtoSet.PackDefectReaded;//Пересортированный пакет прочитан
            plcDtoSet.PackParamChanged = dtoSet.PackParamChanged;//Изменить параметры пакета
            plcDtoSet.PackToPack = dtoSet.PackToPack; //Пакет в пакет
            plcDtoSet.QLayersInPack = dtoSet.QLayersInPack; //Кол-во слоёв в пакете, шт
            plcDtoSet.VPlanksInPack = dtoSet.VPlanksInPack;  //Объём досок в пакете, м3*1000
            plcDtoSet.QPlanksInLayer = dtoSet.QPlanksInLayer; // Кол-во досок в слое досок пакета, шт
            plcDtoSet.QPlanksInLayerBrak = dtoSet.QPlanksInLayerBrak; //Кол-во досок в слое досок пакета брака, шт
            plcDtoSet.QPLankLayersBrak = dtoSet.QPLankLayersBrak; //Кол-во слоёв в пакете брака, шт
            plcDtoSet.Sxema = dtoSet.Sxema; // Кол-во слоёв прокладок в пакете, шт
            plcDtoSet.SxemaNum = dtoSet.SxemaNum; // Номер схемы укладок прокладок
            plcDtoSet.PlankW = dtoSet.PlankW; // Ширина доски, мм
            plcDtoSet.PlankT = dtoSet.PlankT;  //Толщина доски, мм
            plcDtoSet.Sxema2 = dtoSet.Sxema2;// Cхема 2 укладки прокладок
            plcDtoSet.Sxema2Qlayers = dtoSet.Sxema2Qlayers;// Кол-во слоёв схемы 2 укладки прокладок
            plcDtoSet.PackL = dtoSet.PackL; // Длина пересобираемого пакета
            plcDtoSet.PartId = dtoSet.PartId; // Ключ к пересобираемой партии (на случай перезапуска сервака)
            plcDtoSet.PlankL = dtoSet.PlankL; // Проверяемая длина доски, мм
            plcDtoSet.ItemId = dtoSet.ItemId; // Ключ к ItemId партии
            plcDtoSet.SmenaNum = dtoSet.SmenaNum; // Номер смены
            plcDtoSet.SmenaName = dtoSet.SmenaName; // Имя смены
            plcDtoSet.SetNumLayer = dtoSet.SetNumLayer; //Корректируемое кол-во уложенных слоёв досок, шт
            plcDtoSet.SetNumPlankInLayer = dtoSet.SetNumPlankInLayer; //Корректируемое кол-во досок в слое, шт
            plcDtoSet.GeneralSpeed = dtoSet.GeneralSpeed; //Скорость линии
            plcDtoSet.TotalPlanksInPack = dtoSet.TotalPlanksInPack; //Кол-во досок в пакете
            plcDtoSet.RpackNumber = dtoSet.RpackNumber;
            plcDtoSet.CheckL = dtoSet.CheckL; // Проверяемая длина доски, мм
            //plcDtoSet.GasketsBrakPut = dtoSet.GasketsBrakPut; //Слой прокладок на ПФМ брака положен
            plcDtoSet.GasketsLayerPeriod = dtoSet.GasketsLayerPeriod; //Кол-во слоёв досок между прокладками на ПФМ брака
        }

        public async Task<IEnumerable<Alarm>> GetPlcActualAlarms()
        {
            return await Task.FromResult(alarms.Where(x => x.ON == true).OrderByDescending(date => date.TimeON));
        }

        public async Task<List<Alarm>> GetPlcAllAlarms()
        {
            return await Task.FromResult(alarms.ToList());
        }

        public int SetAlarmNumber(int number, bool state)
        {
            try
            {
                if(!alarms.FirstOrDefault(x => x.Number == number).ON) alarms.FirstOrDefault(x => x.Number == number).TimeON = DateTime.Now;
                alarms.FirstOrDefault(x => x.Number == number).ON = state;
                
                return 0;
            }
            catch
            {
                return 1;
            }
        }

        public void GetPackSrez(bool val)
        {
            plcDtoSet.GetPackSrez = val;
        }

        public void SmenaNumCorrect(int smenaNum)
        {
            plcDtoSet.SmenaNum = smenaNum;
        }

        public void PackNumCorrect(int packNum)
        {
            plcDtoSet.RpackNumber = packNum;
        }

        public void SaveSrez(int q, float v)
        {
            plcDtoSet.QplankRemembered = q;
            plcDtoSet.VplankRemembered = v;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }

    }
}