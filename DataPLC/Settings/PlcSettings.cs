﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataPLC.Settings
{
    public class PlcSettings
    {
        public string PlcType { get; set; }
        public string IPaddress { get; set; }
        public short Rack { get; set; }
        public short Slot { get; set; }
    }
}
