﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataPLC.Settings
{
    public class PlcDbReadSettings
    {
        public int DbNum { get; set; }
        public int StartAddr { get; set; }
        public int BytesToRead { get; set; }
        public int ImOffset { get; set; }
        public int ServIMbytes { get; set; }
        public int StatusOffset { get; set; }
    }
}
