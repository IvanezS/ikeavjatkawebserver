﻿using DataPLC.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataPLC.Settings
{
    public class PlcAlarmSettings
    {
        public int AlarmsOffset { get; set; }
        public int AlarmsSize { get; set; }
        public List<AlarmUnit> Alarms { get; set; }
    }
}
