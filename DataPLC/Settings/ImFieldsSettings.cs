﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataPLC.Settings
{
    public class ImFieldsSettings
    {
        public string Symbol { get; set; }
        public string Name { get; set; }
        public string TypeIM { get; set; }

    }
}
