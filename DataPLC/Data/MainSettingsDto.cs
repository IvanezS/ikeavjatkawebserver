﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataPLC.Data
{
    public class MainSettingsDto
    {
        public string SmenaName { get; set; } // Имя смены
        public string OperatorSurname { get; set; } // Фамилия оператора
    }
}
