﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataPLC.Data
{
    public class PlcDto
    {
        public bool StartNewPack { get; set;  } //Начать пересортировку нового пакета
		public bool PackResortedReaded { get; set;  } //Пересортированный пакет прочитан
		public bool FinishPackResorting { get; set;  } //Принудительно закончить пересортировку пакета
		public bool Enable2Sxema { get; set; } //Собрать пакет 2мя схемами прокладок
		public bool EnableRolling { get; set; } //Раскатать доски пакета на заданную длину
		public bool SettingsWasApplyied { get; set; } //Были применены новые настройки
		public bool CheckPlankL { get; set; } //Проверять длину доски
		public bool FinishDefectPack { get; set; } //Принудительно закончить сборку ПФМ брака
		public bool DefectPacketPushed { get; set; } //Пакет с ПФМ брака забрали
		public bool PackDefectReaded { get; set; } //Пересортированный пакет прочитан
		public bool PackParamChanged { get; set; } //Изменить параметры пакета
		public bool PackReadyRidOff { get; set; } //Пакет можно выкатывать
		public bool GasketsRidedOff { get; set; } //Прокладки забрали с ПФМ
		public bool PackToPack { get; set; } //Пакет в пакет


		public short QLayersInPack { get; set; } //Кол-во слоёв в пакете, шт
		public float VPlanksInPack { get; set; } //Объём досок в пакете, м3
		public short QPlanksInLayer { get; set; } // Кол-во досок в слое досок пакета, шт
		public short QPlanksInLayerBrak { get; set; } //Кол-во досок в слое досок пакета брака, шт
		public short QPLankLayersBrak { get; set; } // Кол-во слоёв в пакете брака, шт
		public int Sxema { get; set; } // Cхема укладки прокладок
		public short SxemaNum { get; set; } // Номер схемы укладок прокладок
		public short PlankW { get; set; } // Ширина доски, мм
		public short PlankT { get; set; } //Толщина доски, мм
		public int Sxema2 { get; set; } // Cхема 2 укладки прокладок
		public short Sxema2Qlayers { get; set; } // Кол-во слоёв схемы 2 укладки прокладок
		public short PackL { get; set; } // Длина пересобираемого пакета
		public int PartId { get; set; } // Ключ к пересобираемой партии (на случай перезапуска сервака)
		public short PlankL { get; set; } // Проверяемая длина доски, мм
		public int ItemId { get; set; } // Ключ к ItemId партии
		public short SetNumLayer { get; set; } //Корректируемое кол-во уложенных слоёв досок, шт
		public short SetNumPlankInLayer { get; set; } //Корректируемое кол-во досок в слое, шт
		public short GeneralSpeed { get; set; } //Скорость линии
		public short TotalPlanksInPack { get; set; } //Кол-во досок в пакете
		public short SetNumPlankInLayerBrak { get; set; } //Корректируемое кол-во досок в слое ПФМ брака, шт
		public short CheckL { get; set; } //Проверяемая длина доски, мм
		public bool GasketsBrakPut { get; set; } // Слой прокладок на ПФМ брака положен
		public short GasketsLayerPeriod { get; set; } //Кол-во слоёв досок между прокладками на ПФМ брака



		public bool LineInWork { get; set; } // Линия в работе
		public bool PackResorting { get; set; } // Пакет пересортировывается
		public bool PackResorted { get; set; } //  Пакет пересортировался
		public bool RdyStartNewPack { get; set; } // Есть готовность к пересборке нового пакета
		public bool RdyStart { get; set; } //  Есть готовность к запуску линии
		public bool DefectPacketResorted { get; set; } //  Пакет брака пересортировался

		public short NumLayerActual { get; set; } // Текущее кол-во уложенных слоёв досок, шт
		public short NumPlankInLayerActual { get; set; } //Текущее кол-во досок в слое, шт
		public short QPlanksOk { get; set; } // Текущее кол-во уложенных досок, шт
		public short QPlanksNotOk { get; set; } // Текущее кол-во уложенных херовых досок, шт
		public int VPlanksOk { get; set; } //Текущий объём уложенных досок, м3*1000
		public short QPlankBrak { get; set; } //Кол-во досок в пакете брака, шт
		public int VLayerOk { get; set; } //Объём досок на вилах, м3*1000




	}
}
