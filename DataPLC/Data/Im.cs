﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataPLC.Data
{
	public class Im
	{
		public string Name { get; set; } //Наименование исполнительного механизма
		public string Symbol { get; set; } //Наименование исполнительного механизма
		public string TypeIM { get; set; } //Наименование исполнительного механизма
		public bool BtnDU { get; set; } //Включить дистанционный режим
		public bool BtnAU { get; set; } //Отключить дистанционный режим
		public bool BtnFwd { get; set; } //Включить вперёд / выдвинуть
		public bool BtnBwd { get; set; } //Включить назад / задвинуть
		public bool BtnCmdStop { get; set; } //Остановить
		public double SetSpeedDU { get; set; } // Задание на частотник в ДУ
		public bool PrmBtnDU_ON { get; set; } //Разрешение на включение дистанционного режима
		public bool PrmBtnDU_OF { get; set; } // Разрешение на отключение дистанционного режима
		public bool PRMbtn_Back_ON { get; set; } //Разрешение кнопки включения заднего хода
		public bool PRMbtn_Back_OFF { get; set; } // Разрешение кнопки отключения заднего хода
		public bool PrmBtnCmdStop { get; set; } //  Разрешение на остановку
		public bool Ready { get; set; } //Готовность к работе
		public bool ON { get; set; } //В работе (выдвинуть)
		public bool OFF { get; set; } //Не в работе (задвинуть)
		public bool Fault { get; set; } //Обобщенная неисправность
		public bool Alarm { get; set; } // Авария
		public bool AU { get; set; } //Автоматическое управление
		public bool MU { get; set; } // Местное управление
		public bool DU { get; set; } // Дистанционный режим включен
		public bool CMD_ON { get; set; } //Запустить ON в АУ
		public bool CMD_OFF { get; set; } //Запустить OFF в АУ
		public bool BACK { get; set; } //Задний ход
		public double SetSpeed { get; set; } //Задание на частотник

	}
}
