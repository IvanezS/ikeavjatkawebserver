﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataPLC.Data
{
	public class PlcDtoSet
	{
		public bool StartNewPack { get; set; } //Смена активна
		public bool PackResortedReaded { get; set; } //Пересортированный пакет прочитан
		public bool FinishPackResorting { get; set; } //Принудительно закончить пересортировку пакета
		public bool Enable2Sxema { get; set; } //Собрать пакет 2мя схемами прокладок
		public bool EnableRolling { get; set; } //Раскатать доски пакета на заданную длину
		public bool SettingsWasApplyied { get; set; } //Были применены новые настройки
		public bool CheckPlankL { get; set; } //Проверять длину доски
		public bool FinishDefectPack { get; set; } //Принудительно закончить сборку ПФМ брака
		public bool DefectPacketPushed { get; set; } //Пакет с ПФМ брака забрали
		public bool PackDefectReaded { get; set; } //Пересортированный пакет прочитан
		public bool PackParamChanged { get; set; } //Изменить параметры пакета
		public bool PackReadyRidOff { get; set; } //Пакет можно выкатывать
		public bool GasketsRidedOff { get; set; } //Прокладки забрали с ПФМ
		public bool PackToPack { get; set; } //Пакет в пакет
		public bool AddGasketsLayer { get; set; } //Собрать дополнительный слой прокладок
		public bool PackBrakParamChanged { get; set; } //Откорректировать номер доски в слое ПФМ брака



		public short QLayersInPack { get; set; } //Кол-во слоёв в пакете, шт
		public float VPlanksInPack { get; set; } //Объём досок в пакете, м3*1000
		public short QPlanksInLayer { get; set; } // Кол-во досок в слое досок пакета, шт
		public short QPlanksInLayerBrak { get; set; } //Кол-во досок в слое досок пакета брака, шт
		public short QPLankLayersBrak { get; set; } // Кол-во слоёв в пакете брака, шт
		public int Sxema { get; set; } // Cхема укладки прокладок
		public short SxemaNum { get; set; } // Номер схемы укладок прокладок
		public short PlankW { get; set; } // Ширина доски, мм
		public short PlankT { get; set; } //Толщина доски, мм
		public int Sxema2 { get; set; } // Cхема 2 укладки прокладок
		public short Sxema2Qlayers { get; set; } // Кол-во слоёв схемы 2 укладки прокладок
		public short PackL { get; set; } // Длина пересобираемого пакета
		public int PartId { get; set; } // Ключ к пересобираемой партии (на случай перезапуска сервака)
		public short PlankL { get; set; } // длина доски, мм
		public int ItemId { get; set; } // Ключ к ItemId партии
		public short SetNumLayer { get; set; } //Корректируемое кол-во уложенных слоёв досок, шт
		public short SetNumPlankInLayer { get; set; } //Корректируемое кол-во досок в слое, шт
		public short GeneralSpeed { get; set; } //Скорость линии
		public short TotalPlanksInPack { get; set; } //Кол-во досок в пакете
		public short SetNumPlankInLayerBrak { get; set; } //Корректируемое кол-во досок в слое ПФМ брака, шт
		public short CheckL { get; set; } //Проверяемая длина доски, мм
		public bool GasketsBrakPut { get; set; } // Слой прокладок на ПФМ брака положен
		public short GasketsLayerPeriod { get; set; } //Кол-во слоёв досок между прокладками на ПФМ брака

		//То что ниже - не передаю в ПЛК
		public int Counter { get; set; } // Cчётчик циклов
		public int SxemaId { get; set; } // Ключ БД к схеме укладки прокладок
		public int Sxema2Id { get; set; } // Ключ БД к 2й схеме укладки прокладок
		public int RpackNumber { get; set; } // Номер текущего пересортированного пакета партии
		public int DryPackNumber { get; set; } // Номер текущего пересортированного  сухого пакета
		public int SmenaNum { get; set; } // Номер смены
		public int QplankRemembered { get; set; } // Сохранённое значение кол-ва досок в пакете
		public float VplankRemembered { get; set; } // Сохранённое значение объёма досок в пакете
		public string SmenaName { get; set; } // Имя смены
		public string OperatorSurname { get; set; } // Фамилия оператора
		public bool GetPackSrez { get; set; } //Бит мгновенного среза
		public bool SmenaActive { get; set; } // Смена активна

	}
}
