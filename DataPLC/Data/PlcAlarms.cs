﻿using System;
using System.Collections.Generic;

namespace DataPLC.Data
{


    public class Alarm : AlarmUnit
    {
        public bool ON { get; set; }
        public bool ON_old { get; set; }
        public DateTime TimeON { get; set; }

    }

    public class AlarmUnit
    {
        public string Description { get; set; }
        public int Number { get; set; }
        public string AlmType { get; set; }
    }

}
