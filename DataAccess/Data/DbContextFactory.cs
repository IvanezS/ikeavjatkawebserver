﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Data
{
    public class DbContextFactory
    {
        public IConfiguration Configuration { get; }

        public DbContextFactory(IConfiguration configuration) => Configuration = configuration;

        public UserDBcontext Create()
        {
            var options = new DbContextOptionsBuilder<UserDBcontext>()
                .UseSqlServer(Configuration.GetConnectionString("DevConnection"))
                .Options;

            return new UserDBcontext(options);
        }
    }
}
