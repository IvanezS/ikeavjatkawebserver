﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Domain.Administration;
using Core.Domain.Management;

namespace DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Employee> Employees => new List<Employee>()
        {
            new Employee()
            {
                Id = 1,
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                RoleId = 1
 

            },
            new Employee()
            {
                Id = 2,
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев", 
                RoleId = 2

            },
        };

        public static IEnumerable<Role> Roles => new List<Role>()
        {
            new Role()
            {
                Id = 1,
                Name = "Admin",
                Description = "Администратор",
            },
            new Role()
            {
                Id = 2,
                Name = "Operator",
                Description = "Оператор"
            }
        };

        public static IEnumerable<DProvider> DProviders => new List<DProvider>()
        {
            new DProvider()
            {
                Id = 1,
                Name = "Luzales LLC;",
                ITEM ="73850",
                VATno = "RU1112003481"
            },

            new DProvider()
            {
                Id =2,
                Name = "IKEA Industry Tikhvin LLC",
                ITEM ="90048",
                VATno = "RU4715011103"
            },
        };

        public static IEnumerable<DPart> DParts => new List<DPart>()
        {
            new DPart()
            {
                Id = 1,
                State = 0,
                Comments = "Партия привезена из далече",
                PartNumber = "20.11.1",

            }
        };

        public static IEnumerable<DPacket> DPackets => new List<DPacket>()
        {
            new DPacket()
            {
                Id = 1,


                PartId = 1,
                State = 0,

                TotalPlanks = 150,

                TotalVolume = 3

            }
        };

        public static IEnumerable<DItem> DItems => new List<DItem>()
        {
            new DItem()
            {
                Id =  1,
                Breed = "Pine",
                ItemName = "Item1",
                Quality = "A",
                Side = "center",
                Length = 4000,
                MixedLength = "No",
                Moisture = "Dry",
                Thick = 20,
                Width = 100
            }
        };


        public static IEnumerable<DGaskets> DGaskets => new List<DGaskets>()
        {
            new DGaskets()
            {
                Id =  1,
                name = "1.8",
                sxema = 100
            }
        };



        public static IEnumerable<DResortedPacket> DResortedPackets => new List<DResortedPacket>()
        {
            new DResortedPacket()
            {
                Id =  1,
                isDefected = false,

                PartId = 1,
                TotalPlanks = 150,

            }
        };

    }
}