﻿using Microsoft.EntityFrameworkCore;
using Core.Domain.Administration;
using Core.Domain.Management;
using System;
using System.Collections.Generic;
using System.Text;
using DataAccess.CustomLogger;

namespace DataAccess.Data
{
    public class UserDBcontext : DbContext
    {
        //public DbSet<Employee> Employees { get; set; }
        //public DbSet<Role> Roles { get; set; }
        public DbSet<DProvider> Providers { get; set; }
        public DbSet<DPart> Parts { get; set; }
        public DbSet<DPacket> Packets { get; set; }
        public DbSet<DItem> Items { get; set; }
        public DbSet<DResortedPacket> ResortedPackets { get; set; }
        public DbSet<DMainPart> MainParts { get; set; }
        public DbSet<LogModel> Logs { get; set; }
        public DbSet<DSmena> Smenas { get; set; }
        public DbSet<DGaskets> Gaskets { get; set; }


        public UserDBcontext(DbContextOptions<UserDBcontext> dbContextOptions) : base(dbContextOptions)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Employee>().ToTable("Employees").HasKey(x => x.Id);
            //modelBuilder.Entity<Employee>().Property(x => x.Email).HasMaxLength(50);
            //modelBuilder.Entity<Employee>().Property(x => x.FirstName).HasMaxLength(50);
            //modelBuilder.Entity<Employee>().Property(x => x.LastName).HasMaxLength(50);
            //modelBuilder.Entity<Employee>().Ignore(x => x.FullName);
            //modelBuilder.Entity<Employee>().HasOne(x => x.Role).WithMany().HasForeignKey(x => x.RoleId);

            //modelBuilder.Entity<Role>().ToTable("Roles").HasKey(x => x.Id);
            //modelBuilder.Entity<Role>().Property(x => x.Name).HasMaxLength(50);

            modelBuilder.Entity<DProvider>().ToTable("Providers").HasKey(x => x.Id);

            modelBuilder.Entity<DMainPart>().ToTable("MainParties").HasKey(x => x.Id);
            modelBuilder.Entity<DMainPart>().HasOne(x => x.Provider).WithMany().HasForeignKey(x => x.ProviderId);

            modelBuilder.Entity<DPart>().ToTable("Parties").HasKey(x => x.Id);
            modelBuilder.Entity<DPart>().HasOne(x => x.Item).WithMany().HasForeignKey(x => x.ItemId);
            modelBuilder.Entity<DPart>().HasOne(x => x.MainPart).WithMany(q=>q.ItemParts).HasForeignKey(x => x.MainPartId);

            modelBuilder.Entity<DPacket>().ToTable("Packets").HasKey(x => x.Id);
            modelBuilder.Entity<DPacket>().HasOne(x => x.Part).WithMany(q=>q.Packets).HasForeignKey(x => x.PartId);

            modelBuilder.Entity<DResortedPacket>().ToTable("ResortedPackets").HasKey(x => x.Id);
            modelBuilder.Entity<DResortedPacket>().HasOne(x => x.Part).WithMany(q => q.ResortedPackets).HasForeignKey(x => x.PartId);

            modelBuilder.Entity<DItem>().ToTable("Items").HasKey(x => x.Id);

            modelBuilder.Entity<DGaskets>().ToTable("Gaskets").HasKey(x => x.Id);

            modelBuilder.Entity<LogModel>().ToTable("Logs").HasKey(x => x.Id);

            modelBuilder.Entity<DSmena>().ToTable("Smenas").HasKey(x => x.Id);

            //modelBuilder.Entity<DItem>().HasData(FakeDataFactory.DItems);
            //modelBuilder.Entity<DPacket>().HasData(FakeDataFactory.DPackets);
            //modelBuilder.Entity<DPart>().HasData(FakeDataFactory.DParts);
            //modelBuilder.Entity<DProvider>().HasData(FakeDataFactory.DProviders);
            //modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);
            //modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);
            //modelBuilder.Entity<DGaskets>().HasData(FakeDataFactory.DGaskets);
            //modelBuilder.Entity<DResortedPacket>().HasData(FakeDataFactory.DResortedPackets);
        }
    }
}
