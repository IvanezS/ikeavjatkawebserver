﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Data
{
    public class DbContextInit : IUserDbContextInit
    {
        private readonly UserDBcontext _db;

        public DbContextInit(UserDBcontext db)
        {
            _db = db;
        }

        public void UserDbContextInit()
        {
            _db.Database.EnsureDeleted();
            _db.Database.EnsureCreated();


            _db.AddRange(FakeDataFactory.Employees);
            _db.AddRange(FakeDataFactory.Roles);
            _db.AddRange(FakeDataFactory.DItems);
            _db.AddRange(FakeDataFactory.DPackets);
            _db.AddRange(FakeDataFactory.DParts);
            _db.AddRange(FakeDataFactory.DProviders);

            _db.SaveChanges();
        }


    }
}
