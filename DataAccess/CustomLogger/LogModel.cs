﻿using Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.CustomLogger
{
    public class LogModel : BaseEntity
    {
        public string Message { get; set; }
        public string LogType { get; set; }
        public string User { get; set; }
        public string ON_OFF_text { get; set; }
        public DateTime LogTime { get; set; }
    }
}
