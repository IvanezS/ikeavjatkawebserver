﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Core.Abstractions.Repositories;
using Core.Domain;
using DataAccess.Data;
using Core.Domain.Management;
using DataAccess.CustomLogger;

namespace DataAccess.Repositories
{
    public class LogRepository : IRepository<LogModel>
    {
        //private readonly UserDBcontext _db;

        private readonly DbContextFactory _dbContextFactory;

        public LogRepository(DbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task CreateAsync(LogModel item)
        {
            using (var _db = _dbContextFactory.Create())
            {
                await _db.Logs.AddAsync(item);
                try
                {
                    await _db.SaveChangesAsync();
                }
                catch (Exception ee)
                {
                    throw ee;
                }
            }
        }

        public async Task DeleteAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.Logs.AnyAsync(x => x.Id == id);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {id} не существует");
                _db.Logs.Remove(_db.Logs.Find(id));
                await _db.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<LogModel>> GetAllAsync()
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Logs.ToListAsync();
            }
        }

        public async Task<LogModel> GetByIdAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Logs.FirstOrDefaultAsync(x => x.Id == id);
            }
        }

        public async Task<List<LogModel>> GetListByListId(List<int> ListId)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Logs.Where(x => ListId.Contains(x.Id)).ToListAsync();
            }
        }

        public Task ImportCSV(List<LogModel> ListItem)
        {
            using (var _db = _dbContextFactory.Create())
            {
                throw new NotImplementedException();
            }
        }

        public async Task UpdateAsync(LogModel item)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.Logs.AnyAsync(x => x.Id == item.Id);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {item.Id} не существует");
                _db.Entry(item).State = EntityState.Modified;
                await _db.SaveChangesAsync();
            }
        }




    }
}