﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Core.Abstractions.Repositories;
using Core.Domain;
using DataAccess.Data;
using Core.Domain.Management;

namespace DataAccess.Repositories
{
    public class ResortedPacketRepository : IRepository<DResortedPacket>

    {
        //private readonly UserDBcontext _db;
        private readonly DbContextFactory _dbContextFactory;

        public ResortedPacketRepository(DbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task CreateAsync(DResortedPacket item)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.ResortedPackets.AnyAsync(x => x.Id == item.Id);
                if (xxx) throw new ArgumentException($"Объект с ID = {item.Id} уже существует");
                await _db.ResortedPackets.AddAsync(item);
                try
                {
                    await _db.SaveChangesAsync();
                }
                catch (Exception ee)
                {
                    throw ee;
                }
            }
        }

        public async Task DeleteAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.ResortedPackets.AnyAsync(x => x.Id == id);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {id} не существует");
                _db.ResortedPackets.Remove(_db.ResortedPackets.Find(id));
                await _db.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<DResortedPacket>> GetAllAsync()
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.ResortedPackets.Include(t => t.Part).ToListAsync();
            }
        }

        public async Task<DResortedPacket> GetByIdAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.ResortedPackets.Include(t => t.Part).ThenInclude(tt => tt.Item).Include(t => t.Part).ThenInclude(tt => tt.MainPart).FirstOrDefaultAsync(x => x.Id == id);
            }
        }

        public async Task<List<DResortedPacket>> GetListByListId(List<int> ListId)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.ResortedPackets.Where(x => ListId.Contains(x.Id)).ToListAsync();
            }
        }

        public async Task<DResortedPacket> GetListByPacketNumber(string PN)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.ResortedPackets.Include(t => t.Part).FirstOrDefaultAsync(x => x.PacketNumber.Equals(PN));
            }
        }

        public async Task UpdateAsync(DResortedPacket item)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.ResortedPackets.AnyAsync(x => x.Id == item.Id);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {item.Id} не существует");
                _db.Entry(item).State = EntityState.Modified;
                await _db.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<DResortedPacket>> GetByPartId(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.ResortedPackets.Where(x => x.PartId == id).ToListAsync();
            }

        }

        [Obsolete]
        public async Task ImportCSV(List<DResortedPacket> ListItem)
        {
            using (var _db = _dbContextFactory.Create())
            {
                _db.ResortedPackets.RemoveRange(_db.ResortedPackets);
                await _db.SaveChangesAsync();
                _db.Database.ExecuteSqlCommand("DBCC CHECKIDENT('ResortedPackets', RESEED, 0)");


                foreach (var item in ListItem)
                {
                    var xxx = await _db.ResortedPackets.AnyAsync(x => x.Id == item.Id);
                    if (xxx) throw new ArgumentException($"Объект с ID = {item.Id} уже существует");
                    var it = new DResortedPacket
                    {
                        isDefected = item.isDefected,
                        PacketNumber = item.PacketNumber,
                        PartId = item.PartId,
                        Part = item.Part,
                        TotalPlanks = item.TotalPlanks,
                        TotalVolume = item.TotalVolume,
                        PackDate = item.PackDate,
                        SmenaName = item.SmenaName,
                        SmenaNum = item.SmenaNum,
                        State = item.State

                    };
                    await _db.ResortedPackets.AddAsync(it);
                }
                try
                {
                    await _db.SaveChangesAsync();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

        }

        public async Task<int> getLastPacketNum()
        {
            using (var _db = _dbContextFactory.Create())
            {
                var pn = 0;
                try
                {
                    var count = _db.ResortedPackets.Count();
                    if (count > 0)
                    {
                        var pnka = await _db.ResortedPackets.OrderByDescending(x => x.Id).FirstAsync();
                        return pnka.PacketNumber;
                    }
                }
                catch (Exception ee) { }

                return pn;
            }
        }

    }
}