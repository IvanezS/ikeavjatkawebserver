﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Core.Abstractions.Repositories;
using Core.Domain;
using DataAccess.Data;
using Core.Domain.Management;

namespace DataAccess.Repositories
{
    public class ProviderRepository : IRepository<DProvider>

    {
        //private readonly UserDBcontext _db;
        private readonly DbContextFactory _dbContextFactory;

        public ProviderRepository(DbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task CreateAsync(DProvider item)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.Providers.AnyAsync(x => x.Id == item.Id);
                if (xxx) throw new ArgumentException($"Объект с ID = {item.Id} уже существует");
                await _db.Providers.AddAsync(item);
                await _db.SaveChangesAsync();
            }
        }

        public async Task DeleteAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.Providers.AnyAsync(x => x.Id == id);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {id} не существует");
                _db.Providers.Remove(_db.Providers.Find(id));
                await _db.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<DProvider>> GetAllAsync()
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Providers.ToListAsync();
            }
        }

        public async Task<DProvider> GetByIdAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Providers.FirstOrDefaultAsync(x => x.Id == id);
            }
        }

        public async  Task<List<DProvider>> GetListByListId(List<int> ListId)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Providers.Where(x => ListId.Contains(x.Id)).ToListAsync();
            }
        }

        [Obsolete]
        public async Task ImportCSV(List<DProvider> ListItem)
        {
            using (var _db = _dbContextFactory.Create())
            {
                _db.Providers.RemoveRange(_db.Providers);
                await _db.SaveChangesAsync();
                _db.Database.ExecuteSqlCommand("DBCC CHECKIDENT('Providers', RESEED, 0)");


                foreach (var item in ListItem)
                {
                    var xxx = await _db.Providers.AnyAsync(x => x.Id == item.Id);
                    if (xxx) throw new ArgumentException($"Объект с ID = {item.Id} уже существует");
                    var it = new DProvider
                    {
                        ITEM = item.ITEM,
                        Name = item.Name,
                        VATno = item.VATno
                    };
                    await _db.Providers.AddAsync(it);
                }
                try
                {
                    await _db.SaveChangesAsync();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

        }

        public async Task UpdateAsync(DProvider item)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.Providers.AnyAsync(x => x.Id == item.Id);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {item.Id} не существует");
                _db.Entry(item).State = EntityState.Modified;
                await _db.SaveChangesAsync();
            }
        }
    }
}