﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Core.Abstractions.Repositories;
using Core.Domain;
using DataAccess.Data;
using Core.Domain.Management;

namespace DataAccess.Repositories
{
    public class PacketRepository : IRepository<DPacket>

    {
        //private readonly UserDBcontext _db;
        private readonly PartRepository _part;
        private readonly DbContextFactory _dbContextFactory;

        public PacketRepository(DbContextFactory dbContextFactory, PartRepository part)
        {
            _dbContextFactory = dbContextFactory;
            _part = part;
        }

        public async Task CreateAsync(DPacket item)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.Packets.AnyAsync(x => x.Id == item.Id);
                if (xxx) throw new ArgumentException($"Объект с ID = {item.Id} уже существует");
                await _db.Packets.AddAsync(item);
                await _db.SaveChangesAsync();
                await _part.CheckPart(item.PartId);
            }
        }

        public async Task DeleteAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.Packets.AnyAsync(x => x.Id == id);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {id} не существует");
                var pack = _db.Packets.Find(id);
                _db.Packets.Remove(pack);
                await _db.SaveChangesAsync();
                await _part.CheckPart(pack.PartId);
            }
        }

        public async Task<IEnumerable<DPacket>> GetAllAsync()
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Packets.Include(t => t.Part).ToListAsync();
            }
        }

        public async Task<DPacket> GetByIdAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Packets.Include(t => t.Part).ThenInclude(t => t.MainPart).Include(z => z.Part).ThenInclude(xx => xx.Item).FirstOrDefaultAsync(x => x.Id == id);
            }
        }

        public async Task<List<DPacket>> GetListByListId(List<int> ListId)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Packets.Where(x => ListId.Contains(x.Id)).ToListAsync();
            }
        }

        public async Task UpdateAsync(DPacket item)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.Packets.AnyAsync(x => x.Id == item.Id);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {item.Id} не существует");
                _db.Entry(item).State = EntityState.Modified;
                await _db.SaveChangesAsync();
                await _part.CheckPart(item.PartId);
            }
        }

        public async Task<IEnumerable<DPacket>> GetByPartId(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Packets.Where(x => x.PartId == id).ToListAsync();
            }

        }

        public Task ImportCSV(List<DPacket> ListItem)
        {
            using (var _db = _dbContextFactory.Create())
            {
                throw new NotImplementedException();
            }
        }

        public async Task<DPacket> ScanPack(int id)
        {
            var pack = await GetByIdAsync(id);
            if (pack != null)
            {
                await PackComesOnResorting(pack);
            }
            return pack;
        }


        public async Task ScanPackById(int id)
        {
            var pack = await GetByIdAsync(id);
            if (pack != null)
            {
                await PackComesOnResorting(pack);

            }
        }


        private async Task PackComesOnResorting(DPacket packet)
        {
            packet.State = 1;
            await UpdateAsync(packet);
            
            await _part.CheckPart(packet.PartId);

            
        }


    }
}