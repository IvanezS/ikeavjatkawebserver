﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Core.Abstractions.Repositories;
using Core.Domain;
using DataAccess.Data;
using Core.Domain.Management;

namespace DataAccess.Repositories
{
    public class MainPartRepository : IRepository<DMainPart>

    {
        //private readonly UserDBcontext _db;
        private readonly DbContextFactory _dbContextFactory;


        public MainPartRepository(DbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task CreateAsync(DMainPart item)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.MainParts.AnyAsync(x => x.Id == item.Id);
                if (xxx) throw new ArgumentException($"Объект с ID = {item.Id} уже существует");
                await _db.MainParts.AddAsync(item);
                await _db.SaveChangesAsync();
            }
        }

        public async Task DeleteAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.MainParts.AnyAsync(x => x.Id == id);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {id} не существует");
                _db.MainParts.Remove(_db.MainParts.Find(id));
                await _db.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<DMainPart>> GetAllAsync()
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.MainParts
                .Include(t => t.Provider)
                .Include(x => x.ItemParts).ThenInclude(e => e.MainPart)
                .Include(x => x.ItemParts).ThenInclude(e => e.Item)
                .Include(x => x.ItemParts).ThenInclude(e => e.Packets)
                .Include(x => x.ItemParts).ThenInclude(w => w.ResortedPackets)
                .ToListAsync();
            }
        }

        public async Task<DMainPart> GetByIdAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.MainParts
                .Include(x => x.Provider)
                .Include(x => x.ItemParts).ThenInclude(e => e.MainPart)
                .Include(x => x.ItemParts).ThenInclude(e => e.Item)
                .Include(x => x.ItemParts).ThenInclude(e => e.Packets)
                .Include(x => x.ItemParts).ThenInclude(w => w.ResortedPackets)
                .FirstOrDefaultAsync(x => x.Id == id);
            }
        }

        public async  Task<List<DMainPart>> GetListByListId(List<int> ListId)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.MainParts.Where(x => ListId.Contains(x.Id)).ToListAsync();
            }
        }

        public Task ImportCSV(List<DMainPart> ListItem)
        {
            using (var _db = _dbContextFactory.Create())
            {
                throw new NotImplementedException();
            }
        }

        public async Task UpdateAsync(DMainPart item)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.MainParts.AnyAsync(x => x.Id == item.Id);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {item.Id} не существует");
                _db.Entry(item).State = EntityState.Modified;
                await _db.SaveChangesAsync();
            }
        }



    }
}