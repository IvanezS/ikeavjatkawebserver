﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Core.Abstractions.Repositories;
using Core.Domain;
using DataAccess.Data;
using Core.Domain.Management;
using DataAccess.CustomLogger;

namespace DataAccess.Repositories
{
    public class GasketRepository : IRepository<DGaskets>

    {
        //private readonly UserDBcontext _db;
        private readonly DbContextFactory _dbContextFactory;

        public GasketRepository(DbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task CreateAsync(DGaskets item)
        {
            using (var _db = _dbContextFactory.Create())
            {
                await _db.Gaskets.AddAsync(item);
                try
                {
                    await _db.SaveChangesAsync();
                }
                catch (Exception ee)
                {
                    throw ee;
                }
            }
        }

        public async Task DeleteAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.Gaskets.AnyAsync(x => x.Id == id);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {id} не существует");
                _db.Gaskets.Remove(_db.Gaskets.Find(id));
                await _db.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<DGaskets>> GetAllAsync()
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Gaskets.ToListAsync();
            }
        }

        public async Task<DGaskets> GetByIdAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Gaskets.FirstOrDefaultAsync(x => x.Id == id);
            }
        }

        public async Task<DGaskets> GetBySxema(int sxema)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Gaskets.FirstOrDefaultAsync(x => x.sxema == sxema);
            }
        }

        public async Task<List<DGaskets>> GetListByListId(List<int> ListId)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Gaskets.Where(x => ListId.Contains(x.Id)).ToListAsync();
            }
        }

        [Obsolete]
        public async Task ImportCSV(List<DGaskets> ListItem)
        {
            using (var _db = _dbContextFactory.Create())
            {
                _db.Gaskets.RemoveRange(_db.Gaskets);
                await _db.SaveChangesAsync();
                _db.Database.ExecuteSqlCommand("DBCC CHECKIDENT('Gaskets', RESEED, 0)");


                foreach (var item in ListItem)
                {
                    var xxx = await _db.Gaskets.AnyAsync(x => x.Id == item.Id);
                    if (xxx) throw new ArgumentException($"Объект с ID = {item.Id} уже существует");
                    var it = new DGaskets
                    {
                        name = item.name,
                        sxema = item.sxema
                    };
                    await _db.Gaskets.AddAsync(it);
                }
                try
                {
                    await _db.SaveChangesAsync();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

        }

        public async Task UpdateAsync(DGaskets item)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.Gaskets.AnyAsync(x => x.Id == item.Id);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {item.Id} не существует");
                _db.Entry(item).State = EntityState.Modified;
                await _db.SaveChangesAsync();
            }
        }




    }
}