﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Core.Abstractions.Repositories;
using Core.Domain;
using DataAccess.Data;

namespace DataAccess.Repositories
{
    public class EfRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        //private readonly UserDBcontext _db;
        private readonly DbContextFactory _dbContextFactory;

        public EfRepository(DbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task CreateAsync(T item)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.Set<T>().AnyAsync(x => x.Id == item.Id);
                if (xxx) throw new ArgumentException($"Объект с ID = {item.Id} уже существует");
                await _db.Set<T>().AddAsync(item);
                await _db.SaveChangesAsync();
            }
        }

        public async Task DeleteAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.Set<T>().AnyAsync(x => x.Id == id);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {id} не существует");
                _db.Set<T>().Remove(_db.Set<T>().Find(id));
                await _db.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Set<T>().ToListAsync();
            }
        }

        public async Task<T> GetByIdAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
                return xxx;
            }
        }

        public async Task<List<T>> GetListByListId(List<int> ListId)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Set<T>().Where(x => ListId.Contains(x.Id)).ToListAsync();
            }
        }

        public Task ImportCSV(List<T> ListItem)
        {
            using (var _db = _dbContextFactory.Create())
            {
                throw new NotImplementedException();
            }
        }

        public async Task UpdateAsync(T item)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.Set<T>().AnyAsync(x => x.Id == item.Id);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {item.Id} не существует");
                _db.Entry(item).State = EntityState.Modified;
                await _db.SaveChangesAsync();
            }
        }


    }
}