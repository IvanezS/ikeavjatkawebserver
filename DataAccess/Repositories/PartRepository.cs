﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Core.Abstractions.Repositories;
using Core.Domain;
using DataAccess.Data;
using Core.Domain.Management;

namespace DataAccess.Repositories
{
    public class PartRepository : IRepository<DPart>

    {
        //private readonly UserDBcontext _db;
        private readonly DbContextFactory _dbContextFactory;

        public PartRepository(DbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task CreateAsync(DPart item)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.Parts.AnyAsync(x => x.Id == item.Id);
                if (xxx) throw new ArgumentException($"Объект с ID = {item.Id} уже существует");
                await _db.Parts.AddAsync(item);
                await _db.SaveChangesAsync();
            }
        }

        public async Task DeleteAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.Parts.AnyAsync(x => x.Id == id);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {id} не существует");
                _db.Parts.Remove(_db.Parts.Find(id));
                await _db.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<DPart>> GetAllAsync()
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Parts.Include(x => x.Item).Include(t => t.Packets).Include(t => t.ResortedPackets).Include(w => w.MainPart).ToListAsync();
            }
        }

        public async Task<DPart> GetByIdAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Parts.Include(x => x.Item).Include(t => t.Packets).Include(t => t.ResortedPackets).Include(w => w.MainPart).FirstOrDefaultAsync(x => x.Id == id);
            }
        }

        public async  Task<List<DPart>> GetListByListId(List<int> ListId)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Parts.Where(x => ListId.Contains(x.Id)).ToListAsync();
            }
        }

        public Task ImportCSV(List<DPart> ListItem)
        {
            using (var _db = _dbContextFactory.Create())
            {
                throw new NotImplementedException();
            }
        }

        public async Task UpdateAsync(DPart item)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.Parts.AnyAsync(x => x.Id == item.Id);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {item.Id} не существует");
                _db.Entry(item).State = EntityState.Modified;
                await _db.SaveChangesAsync();
            }
        }

        public async Task CheckPart(int id)
        {
            var part = await GetByIdAsync(id);
            if (part != null)
            {
                var SelectedPacks = part.Packets.Where(x => x.State == 1);
                if (SelectedPacks != null) {
                    if (SelectedPacks.Count() > 0 ) part.State = 1;
                    if (SelectedPacks.Count() == part.Packets.Count()) part.State = 2;

                    await UpdateAsync(part);
                }
            }


        }


    }
}