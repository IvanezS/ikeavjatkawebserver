﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Core.Abstractions.Repositories;
using Core.Domain;
using DataAccess.Data;
using Core.Domain.Management;

namespace DataAccess.Repositories
{
    public class ItemRepository : IRepository<DItem>

    {
        //private readonly UserDBcontext _db;
        //private readonly PlcDataExchangeService _plc;

        private readonly DbContextFactory _dbContextFactory;

        public ItemRepository(DbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task CreateAsync(DItem item)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.Items.AnyAsync(x => x.Id == item.Id);
                if (xxx) throw new ArgumentException($"Объект с ID = {item.Id} уже существует");
                await _db.Items.AddAsync(item);
                await _db.SaveChangesAsync();
            }
        }

        public async Task DeleteAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.Items.AnyAsync(x => x.Id == id);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {id} не существует");
                _db.Items.Remove(_db.Items.Find(id));
                await _db.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<DItem>> GetAllAsync()
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Items.ToListAsync();
            }
        }

        public async Task<DItem> GetByIdAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Items.FirstOrDefaultAsync(x => x.Id == id);
            }
        }

        public async Task<List<DItem>> GetListByListId(List<int> ListId)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Items.Where(x => ListId.Contains(x.Id)).ToListAsync();
            }
        }

        public async Task UpdateAsync(DItem item)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.Items.AnyAsync(x => x.Id == item.Id);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {item.Id} не существует");
                _db.Entry(item).State = EntityState.Modified;
                await _db.SaveChangesAsync();
            }
        }

        public async Task<DItem> GetItemByParams(DItem item)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Items.Where(x => x.Breed == item.Breed
                                            && x.Quality == item.Quality
                                            && x.Side == item.Side
                                            && x.Thick == item.Thick
                                            && x.Moisture == item.Moisture
                                            && x.Length == item.Length
                                            && x.MixedLength == item.MixedLength
                                            && x.Width == item.Width).FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<DItem>> GetItemsByTandW(int T, int W)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Items.Where(x => x.Thick == T && x.Width == W).ToListAsync();
            }
        }

        [Obsolete]
        public async Task ImportCSV(List<DItem> ListItem)
        {
            using (var _db = _dbContextFactory.Create())
            {

                _db.Items.RemoveRange(_db.Items);
                await _db.SaveChangesAsync();
                _db.Database.ExecuteSqlCommand("DBCC CHECKIDENT('Items', RESEED, 0)");


                foreach (var item in ListItem)
                {
                    var xxx = await _db.Items.AnyAsync(x => x.Id == item.Id);
                    if (xxx) throw new ArgumentException($"Объект с ID = {item.Id} уже существует");
                    var it = new DItem
                    {
                        Breed = item.Breed,
                        ItemName = item.ItemName,
                        Quality = item.Quality,
                        Side = item.Side,
                        Thick = item.Thick,
                        Width = item.Width,
                        Length = item.Length,
                        MixedLength = item.MixedLength,
                        Moisture = item.Moisture
                    };
                    await _db.Items.AddAsync(it);
                }
                try
                {
                    await _db.SaveChangesAsync();
                }
                catch (Exception e)
                {
                    throw e;
                }

            }

        }
    }
}