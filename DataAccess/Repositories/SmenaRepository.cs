﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Core.Abstractions.Repositories;
using Core.Domain;
using DataAccess.Data;
using Core.Domain.Management;
using DataAccess.CustomLogger;

namespace DataAccess.Repositories
{
    public class SmenaRepository : IRepository<DSmena>

    {
        //private readonly UserDBcontext _db;
        private readonly DbContextFactory _dbContextFactory;

        public SmenaRepository(DbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task CreateAsync(DSmena item)
        {
            using (var _db = _dbContextFactory.Create())
            {
                await _db.Smenas.AddAsync(item);
                try
                {
                    await _db.SaveChangesAsync();
                }
                catch (Exception ee)
                {
                    throw ee;
                }
            }
        }

        public async Task DeleteAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.Smenas.AnyAsync(x => x.Id == id);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {id} не существует");
                _db.Smenas.Remove(_db.Smenas.Find(id));
                await _db.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<DSmena>> GetAllAsync()
        {
            using (var _db = _dbContextFactory.Create())
            {
                var smenas = await _db.Smenas.ToListAsync();
                return smenas;
            }
        }

        public async Task<DSmena> GetByIdAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Smenas.FirstOrDefaultAsync(x => x.Id == id);
            }
        }

        public async Task<DSmena> GetByIdNumber(int number)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Smenas.OrderByDescending(s => s.Id).FirstOrDefaultAsync(x => x.SmenaNum == number);
            }
        }

        public async Task<List<DSmena>> GetListByListId(List<int> ListId)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Smenas.Where(x => ListId.Contains(x.Id)).ToListAsync();
            }
        }

        public Task ImportCSV(List<DSmena> ListItem)
        {
            using (var _db = _dbContextFactory.Create())
            {
                throw new NotImplementedException();
            }
        }

        public async Task UpdateAsync(DSmena item)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.Smenas.AnyAsync(x => x.Id == item.Id);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {item.Id} не существует");
                _db.Entry(item).State = EntityState.Modified;
                await _db.SaveChangesAsync();
            }
        }

        public async Task<int> getLastSmenaNum()
        {
            using (var _db = _dbContextFactory.Create())
            {
                var smena = 0;
                try
                {
                    var count = _db.Smenas.Count();
                    if (count > 0)
                    {
                        var smenka = await _db.Smenas.OrderByDescending(x => x.Id).FirstAsync();
                        smena = smenka.SmenaNum;
                    }
                }
                catch (Exception ee)
                {
                    return 0;
                }

                return smena;
            }
        }


    }
}