﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Gaskets",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(nullable: true),
                    sxema = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Gaskets", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Items",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ItemName = table.Column<string>(nullable: true),
                    Thick = table.Column<int>(nullable: false),
                    Width = table.Column<int>(nullable: false),
                    Length = table.Column<int>(nullable: false),
                    MixedLength = table.Column<string>(nullable: true),
                    Quality = table.Column<string>(nullable: true),
                    Breed = table.Column<string>(nullable: true),
                    Side = table.Column<string>(nullable: true),
                    Moisture = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Items", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Logs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Message = table.Column<string>(nullable: true),
                    LogType = table.Column<string>(nullable: true),
                    User = table.Column<string>(nullable: true),
                    ON_OFF_text = table.Column<string>(nullable: true),
                    LogTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Logs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Providers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ITEM = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    VATno = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Providers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Smenas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SmenaName = table.Column<string>(nullable: true),
                    SmenaNum = table.Column<string>(nullable: true),
                    Surname = table.Column<string>(nullable: true),
                    DateTimeStart = table.Column<DateTime>(nullable: false),
                    DateTimeStop = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Smenas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MainParties",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    State = table.Column<int>(nullable: false),
                    MainPartNumber = table.Column<string>(nullable: true),
                    ItemCounter = table.Column<int>(nullable: false),
                    ProviderId = table.Column<int>(nullable: false),
                    Comment = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MainParties", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MainParties_Providers_ProviderId",
                        column: x => x.ProviderId,
                        principalTable: "Providers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Parties",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    State = table.Column<int>(nullable: false),
                    PartNumber = table.Column<string>(nullable: true),
                    PacketNumber = table.Column<int>(nullable: false),
                    ResortedPacketNumber = table.Column<int>(nullable: false),
                    MainPartId = table.Column<int>(nullable: false),
                    ItemId = table.Column<int>(nullable: false),
                    Comments = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Parties", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Parties_Items_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Items",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Parties_MainParties_MainPartId",
                        column: x => x.MainPartId,
                        principalTable: "MainParties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Packets",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    State = table.Column<int>(nullable: false),
                    PacketNumber = table.Column<int>(nullable: false),
                    PartId = table.Column<int>(nullable: false),
                    TotalPlanks = table.Column<int>(nullable: false),
                    TotalVolume = table.Column<float>(nullable: false),
                    Note = table.Column<string>(nullable: true),
                    Status = table.Column<bool>(nullable: false),
                    PackDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Packets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Packets_Parties_PartId",
                        column: x => x.PartId,
                        principalTable: "Parties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ResortedPackets",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    State = table.Column<int>(nullable: false),
                    PacketNumber = table.Column<int>(nullable: false),
                    isDefected = table.Column<bool>(nullable: false),
                    PartId = table.Column<int>(nullable: false),
                    TotalPlanks = table.Column<int>(nullable: false),
                    TotalVolume = table.Column<float>(nullable: false),
                    SmenaNum = table.Column<int>(nullable: false),
                    SmenaName = table.Column<string>(nullable: true),
                    PackDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ResortedPackets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ResortedPackets_Parties_PartId",
                        column: x => x.PartId,
                        principalTable: "Parties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MainParties_ProviderId",
                table: "MainParties",
                column: "ProviderId");

            migrationBuilder.CreateIndex(
                name: "IX_Packets_PartId",
                table: "Packets",
                column: "PartId");

            migrationBuilder.CreateIndex(
                name: "IX_Parties_ItemId",
                table: "Parties",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_Parties_MainPartId",
                table: "Parties",
                column: "MainPartId");

            migrationBuilder.CreateIndex(
                name: "IX_ResortedPackets_PartId",
                table: "ResortedPackets",
                column: "PartId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Gaskets");

            migrationBuilder.DropTable(
                name: "Logs");

            migrationBuilder.DropTable(
                name: "Packets");

            migrationBuilder.DropTable(
                name: "ResortedPackets");

            migrationBuilder.DropTable(
                name: "Smenas");

            migrationBuilder.DropTable(
                name: "Parties");

            migrationBuilder.DropTable(
                name: "Items");

            migrationBuilder.DropTable(
                name: "MainParties");

            migrationBuilder.DropTable(
                name: "Providers");
        }
    }
}
