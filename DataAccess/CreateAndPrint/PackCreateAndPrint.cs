﻿using BarcodeLib;
using DataAccess.Repositories;
using Microsoft.Extensions.Options;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.CreateAndPrint
{
    public class PackCreateAndPrint : ICreatePdfAndPrint
    {

        private readonly PacketRepository _context;
        private readonly ProviderRepository _contextProv;
        private readonly PrinterNames _printerNames;

        public PackCreateAndPrint(PacketRepository context, ProviderRepository contextProv, IOptions<PrinterNames> printerNames)
        {
            _context = context;
            _contextProv = contextProv;
            _printerNames = printerNames.Value;
        }

        public async Task CreatePDF(int id, string operatorSurname)
        {
            var pack = await _context.GetByIdAsync(id);
            if (pack != null)
            {
                var provider = await _contextProv.GetByIdAsync(pack.Part.MainPart.ProviderId);
                // Create a new PDF document
                PdfDocument doc = new PdfDocument();
                doc.Info.Title = "Паспорт пакета";

                double scale = 1.0;

                // Create an empty page
                PdfPage page = doc.AddPage();
                page.Height = 368.50 * scale;//0.35278
                page.Width = 538.58 * scale;
                

                XFont fontH1B = new XFont("Verdana", 20, XFontStyle.Bold);
                XFont fontAddr = new XFont("Verdana", 10);
                XFont fontsm = new XFont("Verdana", 8);
                XFont fontH1 = new XFont("Verdana", 14);
                XFont fontOtherSmall = new XFont("Verdana", 22);

                // Get an XGraphics object for drawing
                XGraphics gfx = XGraphics.FromPdfPage(page);
                XColor color = new XColor();
                XPen xPen = new XPen(color);

                gfx.DrawString("IKEA Industry", fontAddr, XBrushes.Black, 70, 15);
                gfx.DrawString("Vyatka", fontAddr, XBrushes.Black, 70, 25);

                gfx.DrawString("ВХОДНОЙ КОНТРОЛЬ - 1 Контроль", fontAddr, XBrushes.Black, page.Width * 0.5 - 29 * 4 * 0.5, 15);
                gfx.DrawString("INCOMING INSPECTION - 1st CONTROL", fontsm, XBrushes.Black, page.Width * 0.5 - 33 * 3 * 0.5, 25);

                gfx.DrawString("Supplier", fontAddr, XBrushes.Black, 5, 55);
                gfx.DrawString(provider.Name, fontOtherSmall, XBrushes.Black, 470*0.5 - provider.Name.Length*7*0.5 + 50, 60);
                gfx.DrawRectangle(xPen, XBrushes.Transparent, 60, 40, 470, 25);

                gfx.DrawString("ID:", fontAddr, XBrushes.Black, 35, 100);
                gfx.DrawString(pack.Part.PartNumber, fontH1, XBrushes.Black, 65, 102);
                gfx.DrawRectangle(xPen, XBrushes.Transparent, 60, 85, 145, 25);
                gfx.DrawString("PN:", fontAddr, XBrushes.Black, 215, 100);
                gfx.DrawString(pack.PacketNumber.ToString(), fontH1, XBrushes.Black, 245, 102);
                gfx.DrawRectangle(xPen, XBrushes.Transparent, 235, 85, 50, 25);
                gfx.DrawString("Date", fontAddr, XBrushes.Black, 300, 100);
                gfx.DrawString(DateTime.Now.ToShortDateString(), fontH1, XBrushes.Black, 340, 102);
                gfx.DrawRectangle(xPen, XBrushes.Transparent, 330, 85, 100, 25);
                gfx.DrawString("Sort", fontAddr, XBrushes.Black, 446, 100);
                gfx.DrawString(pack.Part.Item.Quality, fontH1, XBrushes.Black, 485, 102);
                gfx.DrawRectangle(xPen, XBrushes.Transparent, 480, 85, 50, 25);

                gfx.DrawString("Material", fontAddr, XBrushes.Black, 5, 145);
                gfx.DrawRectangle(xPen, XBrushes.Transparent, 60, 130, 70, 25);
                gfx.DrawString(pack.Part.Item.Thick.ToString(), fontH1B, XBrushes.Black, 75, 150);
                gfx.DrawRectangle(xPen, XBrushes.Transparent, 135, 130, 70, 25);
                gfx.DrawString(pack.Part.Item.Width.ToString(), fontH1B, XBrushes.Black, 150, 150);
                gfx.DrawRectangle(xPen, XBrushes.Transparent, 210, 130, 70, 25);
                gfx.DrawString(pack.Part.Item.MixedLength == "No" ? (pack.Part.Item.Length * 0.001).ToString("F1") : "MIX", fontH1B, XBrushes.Black, 223, 150);

                gfx.DrawString("m3", fontAddr, XBrushes.Black, 340, 140);
                gfx.DrawRectangle(xPen, XBrushes.Transparent, 360, 130, 80, 25);
                gfx.DrawString(pack.TotalVolume.ToString("F3"), fontH1B, XBrushes.Black, 365, 150);
                gfx.DrawString("Pcs", fontAddr, XBrushes.Black, 450, 140);
                gfx.DrawRectangle(xPen, XBrushes.Transparent, 470, 130, 60, 25);
                gfx.DrawString(pack.TotalPlanks.ToString(), fontH1B, XBrushes.Black, 480, 150);

                gfx.DrawString("Moisture", fontAddr, XBrushes.Black, 5, 190);
                gfx.DrawRectangle(xPen, XBrushes.Transparent, 60, 175, 90, 25);
                gfx.DrawString(pack.Part.Item.Moisture, fontH1, XBrushes.Black, 70, 192);
                gfx.DrawString("Wood", fontAddr, XBrushes.Black, 180, 184);
                gfx.DrawString("species", fontAddr, XBrushes.Black, 170, 196);
                gfx.DrawRectangle(xPen, XBrushes.Transparent, 215, 175, 80, 25);
                gfx.DrawString(pack.Part.Item.Breed, fontH1, XBrushes.Black, 225, 192);
                gfx.DrawString("Status", fontAddr, XBrushes.Black, 365, 190);
                gfx.DrawRectangle(xPen, XBrushes.Transparent, 405, 175, 60, 25);
                gfx.DrawRectangle(xPen, XBrushes.Transparent, 470, 175, 60, 25);
                if (pack.Status)
                    gfx.DrawString("OK", fontOtherSmall, XBrushes.Black, 415, 195);
                else
                    gfx.DrawString("NOK", fontOtherSmall, XBrushes.Black, 478, 195);

                

                gfx.DrawString("Note", fontAddr, XBrushes.Black, 20, 235);
                gfx.DrawRectangle(xPen, XBrushes.Transparent, 60, 220, 470, 25);
                if (pack.Note != null) gfx.DrawString(pack.Note, fontH1, XBrushes.Black, 470 * 0.5 - pack.Note.Length*5 * 0.5  + 50, 237);


                gfx.DrawRectangle(xPen, XBrushes.Transparent, 1, 1, page.Width - 1, page.Height - 1);

                XImage pimage = XImage.FromFile("\\\\" + Environment.MachineName + "\\ExportCSV\\PackPDF\\logo.PNG");
                gfx.DrawImage(pimage, 5, 5, 60, 20);

                //--------------------------------------------------------------------
                //Штрихкод
                string shtr = "PackID" + pack.Id;

                var barcode = new BarcodeLib.Barcode
                {
                    Height = 100,
                    Width = 400
                };
                barcode.Encode(TYPE.CODE128, shtr);
                byte[] bytes = barcode.Encoded_Image_Bytes;
                using (MemoryStream ms = new MemoryStream(bytes))
                {
                    XImage ximage = XImage.FromStream(ms);
                    gfx.DrawImage(ximage, page.Width * 0.5 - barcode.Width * 0.5, 260, barcode.Width, barcode.Height );
                }



                //--------------------------------------------------------------------
                try
                {

                    // Save the document...
                    string filename = "\\\\" + Environment.MachineName + "\\ExportCSV\\PackPDF\\Pack_" + pack.PacketNumber.ToString() + ".PDF";
                    doc.Save(filename);

                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public async Task Print(int id, string operatorSurname)
        {
            var pack = await _context.GetByIdAsync(id);
            if (pack != null)
            {
                var fileName = "\\\\" + Environment.MachineName + "\\ExportCSV\\PackPDF\\Pack_" + pack.PacketNumber.ToString() + ".PDF";

                //if (!File.Exists(fileName)) 
                    await CreatePDF(id, operatorSurname);

                try
                {
                    string processFilename = Microsoft.Win32.Registry.LocalMachine
                         .OpenSubKey("Software")
                         .OpenSubKey("Microsoft")
                         .OpenSubKey("Windows")
                         .OpenSubKey("CurrentVersion")
                         .OpenSubKey("App Paths")
                         .OpenSubKey("AcroRd32.exe")
                         .GetValue(String.Empty).ToString();

                    ProcessStartInfo info = new ProcessStartInfo();
                    info.Verb = "print";
                    info.FileName = processFilename;
                    //info.Arguments = String.Format("/p /h {0}", fileName);
                    info.Arguments = String.Format("/t /p /h {0} \"{1}\"", fileName, _printerNames.InputPrinter);
                    info.CreateNoWindow = true;
                    info.WindowStyle = ProcessWindowStyle.Hidden;
                    //(It won't be hidden anyway... thanks Adobe!)
                    info.UseShellExecute = false;

                    info.RedirectStandardOutput = true;

                    Process p = Process.Start(info);
                    p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    //Console.WriteLine(p.StandardOutput.ReadToEnd());

                    int counter = 0;
                    while (!p.HasExited)
                    {
                        System.Threading.Thread.Sleep(1000);
                        counter += 1;
                        if (counter == 9) break;
                    }

                    p.EnableRaisingEvents = true;


                    if (!p.CloseMainWindow())              // CloseMainWindow never seems to succeed
                        p.Kill(); p.WaitForExit();  // Kill AcroRd32.exe

                    p.Close();  // Close the process and release resources

                    if (!File.Exists(fileName)) File.Delete(fileName);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw e;
                }
            } else
            {

            }
        }
    }
}
