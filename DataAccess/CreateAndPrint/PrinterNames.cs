﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.CreateAndPrint
{
    public class PrinterNames
    {
        public string InputPrinter { get; set; }
        public string OutputPrinter { get; set; }
}
}
