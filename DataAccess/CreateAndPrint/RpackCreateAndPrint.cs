﻿using BarcodeLib;
using DataAccess.Repositories;
using Microsoft.Extensions.Options;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.CreateAndPrint
{
    public class RpackCreateAndPrint : ICreatePdfAndPrint
    {

        private readonly ResortedPacketRepository _context;
        private readonly ProviderRepository _contextProv;
        private readonly PrinterNames _printerNames;

        public RpackCreateAndPrint(ResortedPacketRepository context, ProviderRepository contextProv, IOptions<PrinterNames> printerNames)
        {
            _context = context;
            _contextProv = contextProv;
            _printerNames = printerNames.Value;
        }

        public async Task CreatePDF(int id, string operatorSurname)
        {
            var pack = await _context.GetByIdAsync(id);
            if (pack != null)
            {
                var provider = await _contextProv.GetByIdAsync(pack.Part.MainPart.ProviderId);

                PdfDocument doc = new PdfDocument();
                doc.Info.Title = "Паспорт пакета";

                // Create an empty page
                PdfPage page = doc.AddPage();
                page.Height = 595.275;
                page.Width = 419.527;
                
                XFont fontH1 = new XFont("Arial Narrow", 18, XFontStyle.Bold);
                XFont fontAddr = new XFont("Arial Narrow", 9);
                XFont font11 = new XFont("Arial Narrow", 11);
                XFont fontSize = new XFont("Arial", 110, XFontStyle.Bold);
                XFont fontPackNum = new XFont("Arial", 60, XFontStyle.Bold);
                XFont fontShtrihujok = new XFont("XCode 128", 130);
                XFont fontOK = new XFont("Arial Narrow", 45, XFontStyle.Bold);
                XFont fontOtherBig = new XFont("Arial Narrow", 50, XFontStyle.Bold);
                XFont fontOtherSmall = new XFont("Arial Narrow", 25, XFontStyle.Bold);

                // Get an XGraphics object for drawing
                XGraphics gfx = XGraphics.FromPdfPage(page);
                XColor color = new XColor();
                XPen xPen = new XPen(color);
                gfx.DrawRectangle(xPen, XBrushes.LightGray, 200, 50, (page.Width - 200), 25);
                gfx.DrawRectangle(xPen, XBrushes.LightGray, 1, 50, 99, 75);

                if (pack.isDefected)
                {
                    XPoint[] xp = new XPoint[4] { new XPoint(page.Width * 0.5, 1), new XPoint(page.Width, 1), new XPoint(1, page.Height), new XPoint(1, page.Height * 0.5) };

                    gfx.DrawPolygon(xPen, XBrushes.DarkGray, xp, XFillMode.Alternate);
                }

                gfx.DrawRectangle(xPen, XBrushes.Transparent, 1, 1, page.Width - 1, page.Height - 1);

                
                gfx.DrawRectangle(xPen, XBrushes.Transparent, 1, 1, page.Width, 24);
                gfx.DrawRectangle(xPen, XBrushes.Transparent, 1, 25, page.Width, 25);
                gfx.DrawRectangle(xPen, XBrushes.Transparent, 100, 75, page.Width - 100, 25);
                gfx.DrawRectangle(xPen, XBrushes.Transparent, 100, 100, page.Width - 100, 25);
                gfx.DrawRectangle(xPen, XBrushes.Transparent, 60, 265, 80, 135);
                gfx.DrawRectangle(xPen, XBrushes.Transparent, 210, 265, 110, 135);
                gfx.DrawRectangle(xPen, XBrushes.Transparent, 320, 265, 50, 135);

                gfx.DrawRectangle(xPen, XBrushes.Transparent, 1, 265, page.Width, 15);
                gfx.DrawRectangle(xPen, XBrushes.Transparent, 1, 280, page.Width, 40);
                gfx.DrawRectangle(xPen, XBrushes.Transparent, 1, 320, page.Width, 40);
                gfx.DrawRectangle(xPen, XBrushes.Transparent, 1, 360, page.Width, 40);

                gfx.DrawRectangle(xPen, XBrushes.Transparent, 1, 235, page.Width, 25);
                gfx.DrawRectangle(xPen, XBrushes.Transparent, 60, 235, 100, 25);
                gfx.DrawRectangle(xPen, XBrushes.Transparent, 320, 235, 100, 25);

                gfx.DrawRectangle(xPen, XBrushes.Transparent, 1, 130, page.Width, 100);

                gfx.DrawRectangle(xPen, XBrushes.Transparent, 1, 25, 99, 100);
                gfx.DrawRectangle(xPen, XBrushes.Transparent, 100, 25, 100, 75);
                gfx.DrawRectangle(xPen, XBrushes.Transparent, 150, 100, 150, 25);
                gfx.DrawRectangle(xPen, XBrushes.Transparent, 350, 100, 100, 25);



                gfx.DrawString("IKEA Industry Vyatka LLC - Sorting line", fontH1, XBrushes.Black, 50, 20);
                //Штрихкод
                string shtr = "99" + pack.PacketNumber.ToString();
                var barcode = new BarcodeLib.Barcode
                {
                    Height = 60,
                    Width = 300
                };
                barcode.Encode(TYPE.CODE128, shtr);
                byte[] bytes = barcode.Encoded_Image_Bytes;

                using (MemoryStream ms = new MemoryStream(bytes))
                {
                    XImage ximage = XImage.FromStream(ms);
                    gfx.DrawImage(ximage, page.Width * 0.5 - barcode.Width * 0.5, 520, barcode.Width, barcode.Height);
                }

                if (pack.isDefected)
                {
                    gfx.DrawString("NOK", fontOK, XBrushes.Black, 10, 100);
                }
                else
                {
                    gfx.DrawString("OK", fontOK, XBrushes.Black, 20, 100);
                }

                gfx.DrawString("Quality", fontAddr, XBrushes.Black, 40, 45);
                gfx.DrawString("ID", fontAddr, XBrushes.Black, 110, 45);
                gfx.DrawString(pack.Part.PartNumber, font11, XBrushes.Black, 210, 45);
                gfx.DrawString("Supplier", fontAddr, XBrushes.Black, 110, 70);
                gfx.DrawString(provider.Name, fontH1, XBrushes.Black, 210, 70);
                gfx.DrawString("Operator", fontAddr, XBrushes.Black, 110, 95);
                gfx.DrawString(operatorSurname, font11, XBrushes.Black, 210, 95);
                gfx.DrawString("Date", fontAddr, XBrushes.Black, 110, 120);
                gfx.DrawString(DateTime.Now.ToShortDateString(), font11, XBrushes.Black, 250, 120);
                gfx.DrawString("Time", fontAddr, XBrushes.Black, 320, 120);
                gfx.DrawString(DateTime.Now.ToLongTimeString(), font11, XBrushes.Black, 370, 120);

                //Сечение
                string sizes = pack.Part.Item.Thick.ToString() + "x" + pack.Part.Item.Width.ToString();
                gfx.DrawString(sizes, fontSize, XBrushes.Black, page.Width * 0.5 - sizes.Length * 65 * 0.5, 220);
                
                //Порода и влажность
                gfx.DrawString("Wood species", fontAddr, XBrushes.Black, 10, 255);
                gfx.DrawString(pack.Part.Item.Breed, fontH1, XBrushes.Black, 70, 255);
                gfx.DrawString("Moisture", fontAddr, XBrushes.Black, 250, 255);
                gfx.DrawString(pack.Part.Item.Moisture, fontH1, XBrushes.Black, 330, 255);


                ////Таблица с объёмами
                gfx.DrawString("Lengh, м", fontAddr, XBrushes.Black, 15, 275);

                gfx.DrawString("Grade", fontAddr, XBrushes.Black, 90, 275);
                gfx.DrawString("Pcs", fontAddr, XBrushes.Black, 170, 275);
                gfx.DrawString("м3", fontAddr, XBrushes.Black, 260, 275);
                gfx.DrawString("OK", fontAddr, XBrushes.Black, 340, 275);
                gfx.DrawString("NOK", fontAddr, XBrushes.Black, 390, 275);

                gfx.DrawString(pack.Part.Item.Length == 0 ? "MIX" : (pack.Part.Item.Length * 0.001).ToString("F1"), fontOtherSmall, XBrushes.Black, 10, 310);
                gfx.DrawString(pack.Part.Item.Quality, fontOtherSmall, XBrushes.Black, 70, 310);
                gfx.DrawString(pack.TotalPlanks.ToString(), fontOtherSmall, XBrushes.Black, 160, 310);
                gfx.DrawString((pack.TotalVolume).ToString("F3"), fontOtherSmall, XBrushes.Black, 240, 310);
                if (pack.isDefected)
                    gfx.DrawString("NOK", fontOtherSmall, XBrushes.Black, 372, 310);
                else
                    gfx.DrawString("OK", fontOtherSmall, XBrushes.Black, 330, 310);


                string it = "ITEM: " + pack.Part.Item.ItemName;
                gfx.DrawString(it, fontOtherSmall, XBrushes.Black, page.Width * 0.5 - it.Length * 11 * 0.5, 440);

                gfx.DrawString(pack.PacketNumber.ToString(), fontPackNum, XBrushes.Black, page.Width * 0.5 - (pack.PacketNumber.ToString()).Length * 25 * 0.5, 510);




                //--------------------------------------------------------------------
                try
                {

                    // Save the document...
                    string filename = "\\\\" + Environment.MachineName + "\\ExportCSV\\RpackPDF\\Rpack_" + pack.PacketNumber.ToString() + ".PDF";
                    doc.Save(filename);

                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public async Task Print(int id, string operatorSurname)
        {
            var pack = await _context.GetByIdAsync(id);
            if (pack != null)
            {
                var fileName = "\\\\" + Environment.MachineName + "\\ExportCSV\\RpackPDF\\Rpack_" + pack.PacketNumber.ToString() + ".PDF";

                //if (!File.Exists(fileName)) 
                    await CreatePDF(id, operatorSurname);

                try
                {
                    string processFilename = Microsoft.Win32.Registry.LocalMachine
                         .OpenSubKey("Software")
                         .OpenSubKey("Microsoft")
                         .OpenSubKey("Windows")
                         .OpenSubKey("CurrentVersion")
                         .OpenSubKey("App Paths")
                         .OpenSubKey("AcroRd32.exe")
                         .GetValue(String.Empty).ToString();

                    ProcessStartInfo info = new ProcessStartInfo();
                    info.Verb = "print";
                    info.FileName = processFilename;
                    info.Arguments = String.Format("/t /p /h {0} \"{1}\"", fileName, _printerNames.OutputPrinter);
                    info.CreateNoWindow = true;
                    info.WindowStyle = ProcessWindowStyle.Hidden;
                    //(It won't be hidden anyway... thanks Adobe!)
                    info.UseShellExecute = false;

                    info.RedirectStandardOutput = true;

                    Process p = Process.Start(info);
                    p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    //Console.WriteLine(p.StandardOutput.ReadToEnd());

                    int counter = 0;
                    while (!p.HasExited)
                    {
                        System.Threading.Thread.Sleep(1000);
                        counter += 1;
                        if (counter == 4) break;
                    }

                    p.EnableRaisingEvents = true;

                    p.CloseMainWindow();
                    p.Kill();

                    if (!File.Exists(fileName)) File.Delete(fileName);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw e;
                }
            } else
            {

            }
        }
    }
}
