﻿using System.Threading.Tasks;

namespace DataAccess.CreateAndPrint
{
    public interface ICreatePdfAndPrint
    {
        Task CreatePDF(int id, string operatorSurname);
        Task Print(int id, string operatorSurname);
    }
}
