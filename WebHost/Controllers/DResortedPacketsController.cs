﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Core.Domain.Management;
using DataAccess.CreateAndPrint;
using DataAccess.CustomLogger;
using DataAccess.Repositories;
using DataPLC.Data;
using IdentityModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebHost.Models;

namespace WebHost.Controllers
{
    /// <summary>
    /// Пакеты
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DResortedPacketsController : ControllerBase
    {
        private readonly ResortedPacketRepository _context;
        private readonly IPlcDb _plcDb;
        private readonly RpackCreateAndPrint _cNp;
        private readonly LogRepository _log;
        /// <summary>
        /// Пересортированные пакеты
        /// </summary>
        public DResortedPacketsController(ResortedPacketRepository context, IPlcDb plcDb, RpackCreateAndPrint cNp, LogRepository log)
        {
            _context = context;
            _plcDb = plcDb;
            _cNp = cNp;
            _log = log;
        }

        /// <summary>
        /// Получить список всех пересортированных пакетов
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DResortedPacketShortResponse>>> Getpackets()
        {
            var packs = await _context.GetAllAsync();
            var packsResp = packs.Select(x => new DResortedPacketShortResponse
            {
                Id = x.Id,
                PacketNumber = x.PacketNumber,
                PartId = x.PartId,
                isDefected = x.isDefected,
                TotalPlanks = x.TotalPlanks,
                SmenaNum = x.SmenaNum,
                SmenaName = x.SmenaName,
                PackDate = x.PackDate.ToString("s"),
                TotalVolume = x.TotalVolume
            });
            return Ok(packsResp);
        }

        /// <summary>
        /// Получить список пересортированных пакетов по id партии
        /// </summary>
        /// <returns></returns>
        /// <param name="id">Id партии</param>
        [HttpGet("Part/{id}")]
        public async Task<ActionResult<IEnumerable<DResortedPacketShortResponse>>> GetDPacketByPartId(int id)
        {
            var packs = await _context.GetByPartId(id);
            var packsResp = packs.Select(x => new DResortedPacketShortResponse
            {
                Id = x.Id,
                PacketNumber = x.PacketNumber,
                PartId = x.PartId,
                isDefected = x.isDefected,
                TotalPlanks = x.TotalPlanks,
                SmenaNum = x.SmenaNum,
                SmenaName = x.SmenaName,
                PackDate = x.PackDate.ToString("s"),
                TotalVolume = x.TotalVolume
            });
            return Ok(packsResp.OrderByDescending(x => x.PackDate).Take(1000));
        }
        /// <summary>
        /// Получить данные пересортированного пакета по id
        /// </summary>
        /// <returns></returns>
        /// <param name="id">Id пакета</param>
        [HttpGet("{id}")]
        public async Task<ActionResult<DResortedPacket>> GetDPacket(int id)
        {
            var packet = await _context.GetByIdAsync(id);

            if (packet == null)
            {
                return NotFound();
            }

            var packetResponse = new DResortedPacketResponse
            {
                Id = packet.Id,
                PacketNumber = packet.PacketNumber,
                Part = new DPartShortResponse
                {
                    PartNumber = packet.Part.PartNumber,
                    Comments = packet.Part.Comments,
                    Id = packet.Part.Id,
                    State = packet.Part.State,
                    ItemId = packet.Part.ItemId,
                    MainPartId = packet.Part.MainPartId
                },
                PartId = packet.PartId,
                isDefected = packet.isDefected,
                TotalPlanks = packet.TotalPlanks,
                SmenaNum = packet.SmenaNum,
                SmenaName = packet.SmenaName,
                PackDate = packet.PackDate.ToString("s"),
                TotalVolume = packet.TotalVolume
            };

            return Ok(packetResponse);
        }

        /// <summary>
        /// Обновить данные пересортированного пакета по id
        /// </summary>
        /// <returns></returns>
        /// <param name="id">Id пакета</param>
        /// <param name="cep">Параметры пакета</param>
        [HttpPut("{id}")]
        [Authorize(Roles = "Admin, Oper")]
        public async Task<ActionResult<DResortedPacket>> PutDPacket(int id, CreateOrEditResortedPacket cep)
        {


            var plcDtoSet = await _plcDb.GetPlcSetDBAsync();

            try
            {
                var packet = await _context.GetByIdAsync(id);
                packet.PacketNumber = cep.PacketNumber;
                packet.isDefected = cep.isDefected;
                packet.TotalPlanks = cep.TotalPlanks;
                packet.SmenaNum = plcDtoSet.SmenaNum;
                packet.SmenaName = plcDtoSet.SmenaName;
                //packet.PackDate = cep.PackDate;
                packet.TotalVolume = cep.TotalVolume;
                await _context.UpdateAsync(packet);
                await _log.CreateAsync(new LogModel()
                {
                    Message = "Обновлён пересортированный пакет номер " + packet.PacketNumber + " в партии номер " + packet.Part.PartNumber,
                    LogType = "Действие оператора",
                    User = User.FindFirstValue(JwtClaimTypes.Name),
                    LogTime = DateTime.Now,
                    ON_OFF_text = "Пришло"
                });
                return Ok();
            }
            catch
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Создать данные нового пересортированного пакета
        /// </summary>
        /// <returns></returns>
        /// <param name="cep">Параметры пакета</param>
        [HttpPost]
        [Authorize(Roles = "Admin, Oper")]
        public async Task<ActionResult<DResortedPacket>> PostDPacket(CreateOrEditResortedPacket cep)
        {

            var plcDtoSet = await _plcDb.GetPlcSetDBAsync();
            plcDtoSet.RpackNumber++;
            _plcDb.UpdateSettings(plcDtoSet);

            var dPacket = new DResortedPacket()
            {
                PartId = cep.PartId,
                PacketNumber = plcDtoSet.RpackNumber,
                isDefected = cep.isDefected,
                TotalPlanks = cep.TotalPlanks,
                SmenaNum = plcDtoSet.SmenaNum,
                SmenaName = plcDtoSet.SmenaName,
                PackDate = DateTime.Now,
                TotalVolume = cep.TotalVolume,
                State = 0
            };
            
            try
            {
                await _context.CreateAsync(dPacket);
                var pack = await _context.GetByIdAsync(dPacket.Id);
                await _log.CreateAsync(new LogModel()
                {
                    Message = "Создан пересортированный пакет номер " + pack.PacketNumber + " в партии номер " + pack.Part.PartNumber,
                    LogType = "Действие оператора",
                    User = User.FindFirstValue(JwtClaimTypes.Name),
                    LogTime = DateTime.Now,
                    ON_OFF_text = "Пришло"
                });
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Удалить пересортированный пакет
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin, Oper")]
        public async Task<ActionResult<DResortedPacket>> DeleteDPacket(int id)
        {
            try
            {
                var packet = await _context.GetByIdAsync(id);
                await _context.DeleteAsync(id);
                await _log.CreateAsync(new LogModel()
                {
                    Message = "Удалён пересортированный пакет номер " + packet.PacketNumber + " из партии номер " + packet.Part.PartNumber,
                    LogType = "Действие оператора",
                    User = User.FindFirstValue(JwtClaimTypes.Name),
                    LogTime = DateTime.Now,
                    ON_OFF_text = "Пришло"
                });
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpPost("CreatePDF")]
        public async Task<IActionResult> CreatePDF(int Id)
        {
            var plcDtoSet = await _plcDb.GetPlcSetDBAsync();
            
            await _cNp.CreatePDF(Id, (plcDtoSet.OperatorSurname == null) ? "" : plcDtoSet.OperatorSurname);
            return Ok(0);
        }


        [HttpPost("PrintRpackPDF")]
        [Authorize(Roles = "Admin, Oper")]
        public async Task<IActionResult> PrintRpackPDF(int Id)
        {
            var plcDtoSet = await _plcDb.GetPlcSetDBAsync();
            var packet = await _context.GetByIdAsync(Id);
            if (packet == null) return NotFound();
            await _cNp.Print(Id, plcDtoSet.OperatorSurname);
            await _log.CreateAsync(new LogModel()
            {
                Message = "Распечатана этикетка для пересортированного пакета номер " + packet.PacketNumber + " из партии номер " + packet.Part.PartNumber,
                LogType = "Действие оператора",
                User = User.FindFirstValue(JwtClaimTypes.Name),
                LogTime = DateTime.Now,
                ON_OFF_text = "Пришло"
            });
            return Ok(0);
        }




    }
}
