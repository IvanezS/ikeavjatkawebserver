﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Abstractions.Repositories;
using Core.Domain.Management;
using DataAccess.CustomLogger;
using IdentityModel;
using Microsoft.AspNetCore.Mvc;
using WebHost.Models;
using System.Security.Claims;
using DataAccess.Repositories;
using Microsoft.AspNetCore.Authorization;
using DataPLC.Data;
using System.IO;
using System.Text;
using CsvHelper;
using System.Globalization;

namespace WebHost.Controllers
{   
    /// <summary>
    /// Схемы укладки прокладок
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin, Oper")]
    public class DGasketsController : ControllerBase
    {
        private readonly GasketRepository _context;
        private readonly IPlcDb _contextPlc;
        private readonly LogRepository _log;

        /// <summary>
        /// Схемы укладки прокладок
        /// </summary>
        public DGasketsController(GasketRepository context, LogRepository log, IPlcDb contextPlc)
        {
            _contextPlc = contextPlc;
            _context = context;
            _log = log;
        }

        /// <summary>
        /// Получить список всех схем укладки прокладок
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DGasketResponse>>> GetAll()
        {
            var sxem = await _context.GetAllAsync();
            var sxemResp = sxem.Select(x => new DGasketResponse
            {
                Id = x.Id,
                Name = x.name,
                Sxema=x.sxema
                
            });
            return Ok(sxemResp);
        }

        /// <summary>
        /// Получить схему укладки прокладок по id
        /// </summary>
        /// <returns></returns>
        /// <param name="id">Id пакета</param>
        [HttpGet("{id}")]
        public async Task<ActionResult<DGaskets>> GetById(int id)
        {
            var sxem = await _context.GetByIdAsync(id);

            if (sxem == null)
            {
                return NotFound();
            }

            var gasketketResponse = new DGasketResponse
            {
                Id = sxem.Id,
                Name=sxem.name,
                Sxema=sxem.sxema
            };

            return Ok(gasketketResponse);
        }

        /// <summary>
        /// Обновить схему укладки прокладок по id
        /// </summary>
        /// <returns></returns>
        /// <param name="id">Id пакета</param>
        /// <param name="cep">Параметры пакета</param>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUpdate(int id, DGasketResponse cep)
        {

            try
            {
                var settings = await _contextPlc.GetPlcSetDBAsync();
                var gaskets = await _context.GetByIdAsync(id);

                gaskets.name = cep.Name;
                gaskets.sxema = cep.Sxema;
                
                if (id == settings.SxemaId || id == settings.Sxema2Id)
                {
                    if (id == settings.SxemaId) settings.Sxema = cep.Sxema;
                    if (id == settings.Sxema2Id) settings.Sxema2 = cep.Sxema;
                    _contextPlc.UpdateSettings(settings);
                }

                await _context.UpdateAsync(gaskets);
                await _log.CreateAsync(new LogModel()
                {
                    Message = "Обновлена схема укладки прокладок " + gaskets.name,
                    LogType = "Действие оператора",
                    User = User.FindFirstValue(JwtClaimTypes.Name),
                    LogTime = DateTime.Now,
                    ON_OFF_text = "Пришло"
                });
                return Ok();
            }
            catch
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Создать новую схему укладки прокладок
        /// </summary>
        /// <returns></returns>
        /// <param name="cep">Параметры пакета</param>
        [HttpPost]
        public async Task<ActionResult<DPacket>> PostNew(DGaskets cep)
        {
            var gaskets = new DGaskets()
            {
                name = cep.name,
                sxema = cep.sxema
            };

            try
            {
                await _context.CreateAsync(gaskets);
                await _log.CreateAsync(new LogModel()
                {
                    Message = "Создана схема укладки прокладок " + gaskets.name,
                    LogType = "Действие оператора",
                    User = User.FindFirstValue(JwtClaimTypes.Name),
                    LogTime = DateTime.Now,
                    ON_OFF_text = "Пришло"
                });
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Удалить данные пакета по id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<DGaskets>> Delete(int id)
        {
            try
            {
                var gaskets = await _context.GetByIdAsync(id);
                await _context.DeleteAsync(id);
                await _log.CreateAsync(new LogModel()
                {
                    Message = "Удалена схема укладки прокладок " + gaskets.name,
                    LogType = "Действие оператора",
                    User = User.FindFirstValue(JwtClaimTypes.Name),
                    LogTime = DateTime.Now,
                    ON_OFF_text = "Пришло"
                });
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }


        /// <summary>
        /// Импортировать CSV c ITEM-кодами
        /// </summary>
        /// <returns></returns>
        [HttpPost("ImportCSV")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> ImportCSV()
        {
            using (var reader = new StreamReader("\\\\" + Environment.MachineName + "\\ExportCSV\\GASKETS\\GASKETS.csv"))
            {
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    var xxx = csv.GetRecords<DGaskets>().ToList();

                    try
                    {
                        await _context.ImportCSV(xxx);
                        await _log.CreateAsync(new LogModel()
                        {
                            Message = "Импортирован список с таблицей прокладок ",
                            LogType = "Действие оператора",
                            User = User.FindFirstValue(JwtClaimTypes.Name),
                            LogTime = DateTime.Now,
                            ON_OFF_text = "Пришло"
                        });
                        return Ok();
                    }
                    catch (Exception e)
                    {
                        return Ok(e);

                    }
                }
            }
        }

        /// <summary>
        /// Экспортировать CSV c таблицей прокладок
        /// </summary>
        /// <returns></returns>
        [HttpPost("ExportCSV")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> ExportCSV()
        {
            var items = await _context.GetAllAsync();
            string filename = "\\\\" + Environment.MachineName + "\\ExportCSV\\GASKETS\\GASKETS.csv";


            using (FileStream stream = System.IO.File.Create(filename))
            using (var writer = new StreamWriter(stream, Encoding.UTF8))

            {
                using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                {
                    //csv.Configuration.Delimiter = ";";
                    csv.WriteRecords(items);

                }
                writer.Close();

            }
            await _log.CreateAsync(new LogModel()
            {
                Message = "Экспортирован список с таблицей прокладок ",
                LogType = "Действие оператора",
                User = User.FindFirstValue(JwtClaimTypes.Name),
                LogTime = DateTime.Now,
                ON_OFF_text = "Пришло"
            });
            return Ok();
        }

    }
}
