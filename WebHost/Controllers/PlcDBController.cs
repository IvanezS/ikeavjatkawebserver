﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DataAccess.CustomLogger;
using DataAccess.Repositories;
using DataPLC;
using DataPLC.Data;
using IdentityModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using WebHost.Models;

namespace WebHost.Controllers
{
    /// <summary>
    /// Общение с ПЛК
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class PlcDBController : ControllerBase
    {
        private readonly IPlcDb _context;
        private readonly LogRepository _log;

        /// <summary>
        /// Данные с контроллера
        /// </summary>
        public PlcDBController(IPlcDb context, LogRepository log)
        {
            _context = context;
            _log = log;
            
        }

        /// <summary>
        /// Получить данные plc по ПФМ
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<PlcDto>> GetPlcDb()
        {
            var plcData = await _context.GetPlcDBAsync();

            return Ok(plcData);

        }

        /// <summary>
        /// Получить задание на пересортировку
        /// </summary>
        [HttpGet("getSettings")]
        public async Task<ActionResult<PlcDtoSet>> GetPlcSetDb()
        {
            var plcData = await _context.GetPlcSetDBAsync();

            return Ok(plcData);

        }

        /// <summary>
        /// Получить данные plc по ИМ
        /// </summary>
        [HttpGet("ims")]
        public async Task<ActionResult<IEnumerable<Im>>> GetPlcImDb()
        {
            var plcData = await _context.GetPlcDbImsAsync();

            return Ok(plcData);
        }



        /// <summary>
        /// Снять мгновенный срез сортируемого пакета
        /// </summary>
        [HttpPost("GetPackSrez")]
        public async Task<ActionResult> GetPackSrez()
        {
            await _log.CreateAsync(new LogModel()
            {
                Message = "Нажата кнопка \"Снять мгновенный срез сортируемого пакета\"",
                LogType = "Действие оператора",
                User = User.FindFirstValue(JwtClaimTypes.Name),
                LogTime = DateTime.Now,
                ON_OFF_text = "Пришло"
            });
            _context.GetPackSrez(true);
            return Ok();
        }

        /// <summary>
        /// Выкатить пакет с ПФМ
        /// </summary>
        [HttpPost("PackReadyToRidOff")]
        public async Task <ActionResult<int>> PackReadyToRidOff()
        {
            await _log.CreateAsync(new LogModel()
            {
                Message = "Нажата кнопка \"Выкатить пакет с ПФМ\"",
                LogType = "Действие оператора",
                User = User.FindFirstValue(JwtClaimTypes.Name),
                LogTime = DateTime.Now,
                ON_OFF_text = "Пришло"
            });
            return Ok(_context.PackReadyRidOff());
        }


        /// <summary>
        /// Прокладки забрали c ПФМ
        /// </summary>
        [HttpPost("GasketsRidedOff")]
        public async Task<ActionResult<int>> GasketsRidedOff()
        {
            await _log.CreateAsync(new LogModel()
            {
                Message = "Нажата кнопка \"Прокладки забрали c ПФМ\"",
                LogType = "Действие оператора",
                User = User.FindFirstValue(JwtClaimTypes.Name),
                LogTime = DateTime.Now,
                ON_OFF_text = "Пришло"
            });
            return Ok(_context.GasketRidedOff());
        }
        
        /// <summary>
        /// Прокладки положены на ПФМ брака
        /// </summary>
        [HttpPost("GasketsBrakPut")]
        public async Task<ActionResult<int>> GasketsBrakPut()
        {
            await _log.CreateAsync(new LogModel()
            {
                Message = "Нажата кнопка \"Прокладки положены на ПФМ брака\"",
                LogType = "Действие оператора",
                User = User.FindFirstValue(JwtClaimTypes.Name),
                LogTime = DateTime.Now,
                ON_OFF_text = "Пришло"
            });
            return Ok(_context.GasketsBrakPut());
        }


        /// <summary>
        /// Закончить пересортировку пакета
        /// </summary>
        [HttpPost("finishPack")]
        [Authorize(Roles = "Admin, Oper")]
        public async Task<ActionResult<int>> FinishPack()
        {
            await _log.CreateAsync(new LogModel()
            {
                Message = "Нажата кнопка \"Завершить пересортировку пакета\"",
                LogType = "Действие оператора",
                User = User.FindFirstValue(JwtClaimTypes.Name),
                LogTime = DateTime.Now,
                ON_OFF_text = "Пришло"
            });
            return Ok(_context.FinishPack());
        }


        /// <summary>
        /// Закончить пересортировку пакета брака
        /// </summary>
        [HttpPost("finishDefectPack")]
        [Authorize(Roles = "Admin, Oper")]
        public async Task<ActionResult<int>> FinishDefectPack()
        {
            await _log.CreateAsync(new LogModel()
            {
                Message = "Нажата кнопка \"Завершить пересортировку пакета брака\"",
                LogType = "Действие оператора",
                User = User.FindFirstValue(JwtClaimTypes.Name),
                LogTime = DateTime.Now,
                ON_OFF_text = "Пришло"
            });
            return Ok(_context.FinishDefectedPack());
        }

        /// <summary>
        /// Убран пакет брака
        /// </summary>
        [HttpPost("DefectPackPushed")]
        [Authorize(Roles = "Admin, Oper")]
        public async Task<ActionResult<int>> DefectPackPushed()
        {
            await _log.CreateAsync(new LogModel()
            {
                Message = "Нажата кнопка \"Убран пакет брака\"",
                LogType = "Действие оператора",
                User = User.FindFirstValue(JwtClaimTypes.Name),
                LogTime = DateTime.Now,
                ON_OFF_text = "Пришло"
            });
            return Ok(_context.DefectedPackPushed());
        }

        /// <summary>
        /// Собрать дополнительный слой прокладок
        /// </summary>
        [HttpPost("AddGasketsLayer")]
        [Authorize(Roles = "Admin, Oper")]
        public async Task<ActionResult<int>> AddGasketsLayer()
        {
            await _log.CreateAsync(new LogModel()
            {
                Message = "Нажата кнопка \"Собрать дополнительный слой прокладок\"",
                LogType = "Действие оператора",
                User = User.FindFirstValue(JwtClaimTypes.Name),
                LogTime = DateTime.Now,
                ON_OFF_text = "Пришло"
            });
            return Ok(_context.AddGasketsLayer());
        }

        /// <summary>
        /// Задать исходные данные
        /// </summary>
        [HttpPost("setPlcDB")]
        [Authorize(Roles = "Admin, Oper")]
        public async Task<ActionResult<int>> SetPlcDB(PlcDtoSet plcDtoSet)
        {

                await _log.CreateAsync(new LogModel()
                {
                    Message = "Нажата кнопка \"Загрузить настройки в сервер и ПЛК\"",
                    LogType = "Действие оператора",
                    User = User.FindFirstValue(JwtClaimTypes.Name),
                    LogTime = DateTime.Now,
                    ON_OFF_text = "Пришло"
                });
            try
            {
               if (_context.SetPlcDB(plcDtoSet) == 0) return Ok();
               return BadRequest();
            }
            catch
            {
                return BadRequest();
            }
            
        }



        /// <summary>
        /// Считать настройкии ПЛК
        /// </summary>
        [HttpPost("ReadSettingsFromPlc")]
        [Authorize(Roles = "Admin, Oper")]
        public async Task<ActionResult<int>> ReadSettingsFromPlc() { 
            _context.ReadSettingsFromPlc();
            await _log.CreateAsync(new LogModel()
            {
                Message = "Нажата кнопка \"Считать основные настройки из ПЛК\"",
                LogType = "Действие оператора",
                User = User.FindFirstValue(JwtClaimTypes.Name),
                LogTime = DateTime.Now,
                ON_OFF_text = "Пришло"
            });
            return Ok(); 
        }
        /// <summary>
        /// Откорректировать настройки собираемого пакета
        /// </summary>
        [HttpPost("ChangePackParam")]
        [Authorize(Roles = "Admin, Oper")]
        public async Task<ActionResult> ChangePackParam(short SetNumLayer, short SetNumPlankInLayer)
        {
            _context.SendPackParam(SetNumLayer, SetNumPlankInLayer);
            _context.SendPackParamApply();
            await _log.CreateAsync(new LogModel()
            {
                Message = "Нажата кнопка \"Откорректировать настройки собираемого пакета\"",
                LogType = "Действие оператора",
                User = User.FindFirstValue(JwtClaimTypes.Name),
                LogTime = DateTime.Now,
                ON_OFF_text = "Пришло"
            });
            return Ok();

        }

        /// <summary>
        /// Откорректировать настройки собираемого пакета ПФМ брака
        /// </summary>
        [HttpPost("ChangePackParamBrak")]
        [Authorize(Roles = "Admin, Oper")]
        public async Task<ActionResult> ChangePackParam(short SetNumPlankInLayerBrak)
        {
            _context.PackBrakParamChanged(SetNumPlankInLayerBrak);
            await _log.CreateAsync(new LogModel()
            {
                Message = "Нажата кнопка \"Откорректировать настройки собираемого пакета ПФМ брака\"",
                LogType = "Действие оператора",
                User = User.FindFirstValue(JwtClaimTypes.Name),
                LogTime = DateTime.Now,
                ON_OFF_text = "Пришло"
            });
            return Ok();

        }

        /// <summary>
        /// Задать скорость
        /// </summary>
        [HttpPost("ApplySpeed")]
        [Authorize(Roles = "Admin, Oper")]
        public async Task<ActionResult> ApplySpeed(short speed)
        {
            if (speed >= 10 && speed <= 100) await _context.ApplySpeedSettings(speed);
            await _log.CreateAsync(new LogModel()
            {
                Message = "Задана скорость линии " + speed + "%",
                LogType = "Действие оператора",
                User = User.FindFirstValue(JwtClaimTypes.Name),
                LogTime = DateTime.Now,
                ON_OFF_text = "Пришло"
            });
            return Ok();

        }

        /// <summary>
        /// Получить данные plc по ПФМ
        /// </summary>
        [HttpGet("GetActualAlarms")]
        public async Task<ActionResult<AlarmResponse>> GetActualAlarms()
        {
            var plcAlarms = await _context.GetPlcActualAlarms();

            var response = plcAlarms.Select(pa => new AlarmResponse()
            {
                TimeON = pa.TimeON.ToString("u"),
                Message = pa.Description,
                On = pa.ON
            });

            return Ok(response);

        }

        /// <summary>
        /// Откорректировать номер смены
        /// </summary>
        [HttpPost("SmenaNumCorrect")]
        public async Task<ActionResult<int>> SmenaNumCorrect(int smenaNum)
        {
            _context.SmenaNumCorrect(smenaNum);
            await _log.CreateAsync(new LogModel()
            {
                Message = "Задана смена №" + smenaNum,
                LogType = "Действие оператора",
                User = User.FindFirstValue(JwtClaimTypes.Name),
                LogTime = DateTime.Now,
                ON_OFF_text = "Пришло"
            });
            return Ok();
        }

        /// <summary>
        /// Откорректировать номер пакета
        /// </summary>
        [HttpPost("PackNumCorrect")]
        public async Task<ActionResult<int>> PackNumCorrect(int PackNum)
        {
            _context.PackNumCorrect(PackNum);
            await _log.CreateAsync(new LogModel()
            {
                Message = "Задана текущий номера пакета №" + PackNum,
                LogType = "Действие оператора",
                User = User.FindFirstValue(JwtClaimTypes.Name),
                LogTime = DateTime.Now,
                ON_OFF_text = "Пришло"
            });
            return Ok();
        }


    }


}
