﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Domain.Management;
using DataAccess.CreateAndPrint;
using DataAccess.Repositories;
using IdentityModel;
using Microsoft.AspNetCore.Mvc;
using WebHost.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using DataAccess.CustomLogger;
using System.Text.RegularExpressions;

namespace WebHost.Controllers
{   
    /// <summary>
    /// Пакеты
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DPacketsController : ControllerBase
    {
        private readonly PacketRepository _context;
        private readonly ResortedPacketRepository _contextR;
        private readonly PackCreateAndPrint _cNp;
        private readonly LogRepository _log;
        private readonly PartRepository _contextP;

        /// <summary>
        /// Пакеты
        /// </summary>
        public DPacketsController(PacketRepository context, ResortedPacketRepository contextR, PackCreateAndPrint cNp, LogRepository log, PartRepository contextP)
        {
            _cNp = cNp;
            _context = context;
            _contextR = contextR;
            _log = log;
            _contextP = contextP;
        }

        /// <summary>
        /// Получить список всех пакетов
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DPacketShortResponse>>> Getpackets()
        {
            var packs = await _context.GetAllAsync();
            var packsResp = packs.Select(x => new DPacketShortResponse
            {
                Id = x.Id,
                PacketNumber = x.PacketNumber,
                PartId = x.PartId,
                State=x.State,
                TotalPlanks=x.TotalPlanks,
                TotalVolume=x.TotalVolume,
                Status = x.Status,
                Note = x.Note,
                PackDate = x.PackDate.ToString("s")
            });
            return Ok(packsResp.OrderByDescending(x => x.PackDate).Take(1000));
        }

        /// <summary>
        /// Получить список пакетов по id партии
        /// </summary>
        /// <returns></returns>
        /// <param name="id">Id партии</param>
        [HttpGet("Part/{id}")]
        public async Task<ActionResult<IEnumerable<DPacketShortResponse>>> GetDPacketByPartId(int id)
        {
            var packs = await _context.GetByPartId(id);
            var packsResp = packs.Select(x => new DPacketShortResponse
            {
                Id = x.Id,
                PacketNumber = x.PacketNumber,
                PartId = x.PartId,
                State = x.State,
                TotalPlanks = x.TotalPlanks,
                TotalVolume = x.TotalVolume,
                Status = x.Status,
                Note = x.Note,
                PackDate = x.PackDate.ToString("s")
            });
            return Ok(packsResp);
        }
        /// <summary>
        /// Получить данные пакета по id
        /// </summary>
        /// <returns></returns>
        /// <param name="id">Id пакета</param>
        [HttpGet("{id}")]
        public async Task<ActionResult<DPacket>> GetDPacket(int id)
        {
            var packet = await _context.GetByIdAsync(id);

            if (packet == null)
            {
                return NotFound();
            }

            var packetResponse = new DPacketResponse
            {
                Id = packet.Id,
                PacketNumber = packet.PacketNumber,
                
                Part = new DPartShortResponse 
                { 
                    PartNumber = packet.Part.PartNumber,
                    Comments = packet.Part.Comments,
                    Id = packet.Part.Id,
                    State = packet.Part.State,
                    ItemId = packet.Part.ItemId

                },
                PartId = packet.PartId,

                State = packet.State,
                TotalPlanks = packet.TotalPlanks,
                TotalVolume = packet.TotalVolume,
                Status = packet.Status,
                Note = packet.Note,
                PackDate = packet.PackDate.ToString("s")
            };

            return Ok(packetResponse);
        }

        /// <summary>
        /// Обновить данные пакета по id
        /// </summary>
        /// <returns></returns>
        /// <param name="id">Id пакета</param>
        /// <param name="cep">Параметры пакета</param>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDPacket(int id, CreateOrEditPacket cep)
        {

            try
            {
                var packet = await _context.GetByIdAsync(id);
                packet.PacketNumber = cep.PacketNumber;
                packet.TotalPlanks = cep.TotalPlanks;
                packet.TotalVolume = cep.TotalVolume;
                packet.State = cep.State;
                packet.Status = cep.Status;
                packet.Note = cep.Note;
                await _context.UpdateAsync(packet);
                await _log.CreateAsync(new LogModel()
                {
                    Message = "Обновлён исходный пакет номер " + packet.PacketNumber + " в партии номер " + packet.Part.PartNumber,
                    LogType = "Действие оператора",
                    User = User.FindFirstValue(JwtClaimTypes.Name),
                    LogTime = DateTime.Now,
                    ON_OFF_text = "Пришло"
                });
                return Ok();
            }
            catch
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Создать данные нового пакета
        /// </summary>
        /// <returns></returns>
        /// <param name="cep">Параметры пакета</param>
        [HttpPost]
        public async Task<ActionResult<DPacket>> PostDPacket(CreateOrEditPacket cep)
        {

            var part = await _contextP.GetByIdAsync(cep.PartId);
            part.PacketNumber++;
            await _contextP.UpdateAsync(part);

            var dPacket = new DPacket()
            {
                PartId = cep.PartId,
                PacketNumber = part.PacketNumber,
                TotalPlanks = cep.TotalPlanks,
                TotalVolume = cep.TotalVolume,
                Status = cep.Status,
                Note = cep.Note,
                PackDate = DateTime.Now,
                State = 0

            };

            try
            {
                await _context.CreateAsync(dPacket);
                await _log.CreateAsync(new LogModel()
                {
                    Message = "Создан исходный пакет номер " + dPacket.PacketNumber + " в партии номер " + part.PartNumber,// dPacket.Part.PartNumber,
                    LogType = "Действие оператора",
                    User = User.FindFirstValue(JwtClaimTypes.Name),
                    LogTime = DateTime.Now,
                    ON_OFF_text = "Пришло"
                }) ;
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Удалить данные пакета по id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<DPacket>> DeleteDPacket(int id)
        {
            try
            {
                var packet = await _context.GetByIdAsync(id);
                await _context.DeleteAsync(id);
                await _log.CreateAsync(new LogModel()
                {
                    Message = "Удалён исходный пакет номер " + packet.PacketNumber + " из партии номер " + packet.Part.PartNumber,
                    LogType = "Действие оператора",
                    User = User.FindFirstValue(JwtClaimTypes.Name),
                    LogTime = DateTime.Now,
                    ON_OFF_text = "Пришло"
                });
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }


        /// <summary>
        /// Отправить скан со штрихкода пакета
        /// </summary>
        [HttpPost("SendScan")]
        public async Task<ActionResult> SendScan(string scan)
        {
            if (scan == null) return BadRequest("Штрихкод пуст");

            var regex = new Regex("^PackID([0-9]+)?$");
            if (regex.IsMatch(scan))
            {
                var packId = Convert.ToInt32(regex.Match(scan).Groups[1].Value);
                var pack = await _context.ScanPack(packId);
                if (pack == null) return BadRequest("Штрихкод считан успешно, но пакет не найден в базе");
                try
                {
                
                    await _log.CreateAsync(new LogModel()
                    {
                        Message = "Отсканирован исходный пакет номер " + pack.PacketNumber + " из партии номер " + pack.Part.PartNumber,
                        LogType = "Действие оператора",
                        User = User.FindFirstValue(JwtClaimTypes.Name),
                        LogTime = DateTime.Now,
                        ON_OFF_text = "Пришло"
                    });
                    return Ok();
                }
                catch (Exception ee)
                {
                    return BadRequest(ee);
                }
            }
            regex = new Regex("^99([0-9]+)?$");
            if (regex.IsMatch(scan))
            {
                var packNumber = regex.Match(scan).Groups[1].Value;
                var RPack = await _contextR.GetListByPacketNumber(packNumber);
                if (RPack == null) return BadRequest("Штрихкод считан успешно, но пакет не найден в базе");
                try
                {

                    await _log.CreateAsync(new LogModel()
                    {
                        Message = "Отсканирован cухой пакет номер " + RPack.PacketNumber,
                        LogType = "Действие оператора",
                        User = User.FindFirstValue(JwtClaimTypes.Name),
                        LogTime = DateTime.Now,
                        ON_OFF_text = "Пришло"
                    });
                    return Ok();
                }
                catch (Exception ee)
                {
                    return BadRequest(ee);
                }
            }
            
            return BadRequest("Неправильный штрихкод");

        }


        /// <summary>
        /// Пометить пакет отсканированным
        /// </summary>
        [HttpPost("MarkScan")]
        public async Task<ActionResult> MarkScan(int id)
        {

            try
            {
                var packet = await _context.GetByIdAsync(id);
                await _context.ScanPackById(id);
                await _log.CreateAsync(new LogModel()
                {
                    Message = "Помечен отсканированным исходный пакет номер " + packet.PacketNumber + " из партии номер " + packet.Part.PartNumber,
                    LogType = "Действие оператора",
                    User = User.FindFirstValue(JwtClaimTypes.Name),
                    LogTime = DateTime.Now,
                    ON_OFF_text = "Пришло"
                });
                return Ok();
            }
            catch (Exception ee)
            {
                return BadRequest(ee);
            }
        }


        [HttpPost("CreatePDF")]
        public async Task<IActionResult> CreatePDF(int Id)
        {
            await _cNp.CreatePDF(Id, "");
            return Ok();
        }


        [HttpPost("PrintRpackPDF")]
        public async Task<IActionResult> PrintRpackPDF(int Id)
        {
            try
            {
                var packet = await _context.GetByIdAsync(Id);
                await _cNp.Print(Id,"");
                await _log.CreateAsync(new LogModel()
                {
                    Message = "Распечатана этикетка для исходного пакета номер " + packet.PacketNumber + " из партии номер " + packet.Part.PartNumber,
                    LogType = "Действие оператора",
                    User = User.FindFirstValue(JwtClaimTypes.Name),
                    LogTime = DateTime.Now,
                    ON_OFF_text = "Пришло"
                });
                return Ok();
            }
            catch (Exception ee)
            {
                return BadRequest(ee);
            }

        }

    }
}
