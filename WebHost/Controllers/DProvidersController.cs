﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Core.Domain.Management;
using CsvHelper;
using DataAccess.CustomLogger;
using DataAccess.Repositories;
using IdentityModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebHost.Models;

namespace WebHost.Controllers

{    /// <summary>
     /// Поставщики
     /// </summary>
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class DProvidersController : ControllerBase
    {
        private readonly ProviderRepository _context;
        private readonly LogRepository _log;
        
        /// <summary>
        /// Поставщики
        /// </summary>
        public DProvidersController(ProviderRepository context, LogRepository log)

        {
            _context = context;
            _log = log;
        }

        /// <summary>
        /// Получить данные всех поставщиков
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DProviderResponse>>> Getproviders()
        {
            var providers = await _context.GetAllAsync();
            var providersResp = providers.Select(x => new DProviderShortResponse
            {
                Id = x.Id,
                ITEM = x.ITEM,
                VATno = x.VATno,
                Name = x.Name

            });
            return Ok(providersResp);
        }

        /// <summary>
        /// Получить данные поставщика по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<DProviderResponse>> GetDProviders(int id)
        {
            var provider = await _context.GetByIdAsync(id);

            if (provider == null)
            {
                return NotFound();
            }

            var providerResponse = new DProviderResponse
            {
                Id = provider.Id,
                ITEM = provider.ITEM,
                VATno = provider.VATno,
                Name = provider.Name
            };

            return Ok(providerResponse);
        }

        /// <summary>
        /// Обновить данные поставщика по id
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> PutDProviders(int id, CreateOrEditProvider cep)
        {
            

            try
            {
                var dProvider = await _context.GetByIdAsync(id);
                
                dProvider.Name = cep.Name;
                dProvider.ITEM = cep.ITEM;
                dProvider.VATno = cep.VATno;                
                await _context.UpdateAsync(dProvider);
                await _log.CreateAsync(new LogModel() {
                    Message = "Обновлён поставщик " + dProvider.Name, 
                    LogType ="Действие оператора", 
                    User = User.FindFirstValue(JwtClaimTypes.Name),
                    LogTime = DateTime.Now,
                    ON_OFF_text = "Пришло" });
                return Ok();
            }
            catch
            {
                //await _log.ToDB("Не удалось обновить поставщика", "Действие оператора", User.FindFirstValue(JwtClaimTypes.Name), true);
                return NotFound();
            }
        }


        /// <summary>
        /// Создать данные нового поставщика
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<DProviderResponse>> PostDProviders(CreateOrEditProvider cep)
        {

            var dProvider = new DProvider()
            {
                Name = cep.Name,
                ITEM = cep.ITEM,
                VATno = cep.VATno
            };

            try
            {
                await _context.CreateAsync(dProvider);
                await _log.CreateAsync(new LogModel()
                {
                    Message = "Создан поставщик " + dProvider.Name,
                    LogType = "Действие оператора",
                    User = User.FindFirstValue(JwtClaimTypes.Name),
                    LogTime = DateTime.Now,
                    ON_OFF_text = "Пришло"
                });
                return Ok();
            }
            catch
            {
                return NotFound();
            }
        }

        
        /// <summary>
        /// Удалить данные поставщика по id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<DProvider>> DeleteDProviders(int id)
        {
            try
            {
                var dProvider = await _context.GetByIdAsync(id);
                await _context.DeleteAsync(id);
                await _log.CreateAsync(new LogModel()
                {
                    Message = "Удалён поставщик " + dProvider.Name,
                    LogType = "Действие оператора",
                    User = User.FindFirstValue(JwtClaimTypes.Name),
                    LogTime = DateTime.Now,
                    ON_OFF_text = "Пришло"
                });
                return Ok();
            }
            catch
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Импортировать CSV c поставщиками
        /// </summary>
        /// <returns></returns>
        [HttpPost("ImportCSV")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> ImportCSV()
        {
            using (var reader = new StreamReader("\\\\" + Environment.MachineName + "\\ExportCSV\\SUPPLIERS\\SUPPLIERS.csv"))
            {
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    var xxx = csv.GetRecords<DProvider>().ToList();

                    try
                    {
                        await _context.ImportCSV(xxx);
                        await _log.CreateAsync(new LogModel()
                        {
                            Message = "Импортирован список поставщиков ",
                            LogType = "Действие оператора",
                            User = User.FindFirstValue(JwtClaimTypes.Name),
                            LogTime = DateTime.Now,
                            ON_OFF_text = "Пришло"
                        });
                        return Ok();
                    }
                    catch (Exception e)
                    {
                        return Ok(e);

                    }
                }
            }
        }

        /// <summary>
        /// Экспортировать CSV c поставщиками
        /// </summary>
        /// <returns></returns>
        [HttpPost("ExportCSV")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> ExportCSV()
        {
            var items = await _context.GetAllAsync();
            string filename = "\\\\" + Environment.MachineName + "\\ExportCSV\\SUPPLIERS\\SUPPLIERS.csv";


            using (FileStream stream = System.IO.File.Create(filename))
            using (var writer = new StreamWriter(stream, Encoding.UTF8))

            {
                using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                {
                    //csv.Configuration.Delimiter = ";";
                    csv.WriteRecords(items);

                }
                writer.Close();

            }
            await _log.CreateAsync(new LogModel()
            {
                Message = "Экспортирован список поставщиков ",
                LogType = "Действие оператора",
                User = User.FindFirstValue(JwtClaimTypes.Name),
                LogTime = DateTime.Now,
                ON_OFF_text = "Пришло"
            });
            return Ok();
        }
    }
}
