﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Abstractions.Repositories;
using Core.Domain.Management;
using DataAccess.CustomLogger;
using DataAccess.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebHost.Models;

namespace WebHost.Controllers
{   
    /// <summary>
    /// Логи
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class LogsController : ControllerBase
    {
        private readonly IRepository<LogModel> _context;

        /// <summary>
        /// Логи
        /// </summary>
        public LogsController(IRepository<LogModel> context)
        {
            _context = context;
        }

        /// <summary>
        /// Получить список всех логов
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<LogResponse>>> GetAll()
        {
            var Logs = await _context.GetAllAsync();
            var LogsResp = Logs.Select(x => new LogResponse
            {
                LogTime = x.LogTime.ToString("u"),
                LogType = x.LogType,
                Message = x.Message,
                ON_OFF_text = x.ON_OFF_text,
                User = x.User
                
            });
            return Ok(LogsResp.OrderByDescending(date => date.LogTime).Take(1000));
        }



    }
}
