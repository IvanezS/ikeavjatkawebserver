﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Domain.Management;
using CsvHelper;
using DataAccess.CustomLogger;
using DataAccess.Repositories;
using IdentityModel;
using Microsoft.AspNetCore.Mvc;
using WebHost.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace WebHost.Controllers
{
   
    /// <summary>
    /// ITEM-коды
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DItemsController : ControllerBase
    {
        private readonly ItemRepository _context;
        private readonly LogRepository _log;

        /// <summary>
        /// ITEM-коды
        /// </summary>
        public DItemsController(ItemRepository context, LogRepository log)
        {
            _context = context;
            _log = log;
        }

        /// <summary>
        /// Получить данные всех ITEM-кодов
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DItemResponse>>> GetDItems()
        {
            var items = await _context.GetAllAsync();
            var ItemsResponse = items.Select(x => new DItemResponse
            {
                Breed = x.Breed,
                Id = x.Id,
                ItemName = x.ItemName,
                Quality = x.Quality,
                Side = x.Side,
                Thick = x.Thick,
                Width = x.Width,
                Length = x.Length,
                MixedLength = x.MixedLength,
                Moisture = x.Moisture
                
            }).ToList(); 

            return Ok (ItemsResponse);
        }

        /// <summary>
        /// Получить данные ITEM-кода по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<DItemResponse>> GetDItems(int id)
        {
            var item = await _context.GetByIdAsync(id);

            if (item == null)
            {
                return NotFound();
            }

            var ItemResponse = new DItemResponse
            {
                Breed = item.Breed,
                Id = item.Id,
                ItemName = item.ItemName,
                Quality = item.Quality,
                Side = item.Side,
                Thick = item.Thick,
                Width = item.Width,
                Length = item.Length,
                MixedLength = item.MixedLength,
                Moisture = item.Moisture
            };

            return Ok(ItemResponse);
        }

        /// <summary>
        /// Обновить данные ITEM-кода по id
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Authorize(Roles = "Admin, Master")]
        public async Task<IActionResult> PutDItems(DItem dItem)
        {

            try
            {
                await _context.UpdateAsync(dItem);
                await _log.CreateAsync(new LogModel()
                {
                    Message = "Обновлён ITEM-код " + dItem.ItemName,
                    LogType = "Действие оператора",
                    User = User.FindFirstValue(JwtClaimTypes.Name),
                    LogTime = DateTime.Now,
                    ON_OFF_text = "Пришло"
                });
                return Ok();
            }
            catch
            {
                return NotFound();
            }
            
        }

        ///// <summary>
        ///// Получть ITEM-код по его параметрам
        ///// </summary>
        ///// <returns></returns>
        //[HttpPost("GetItemByParams")]
        //public async Task<IActionResult> GetItemByParams(CreateOrEditItem dItem)
        //{
        //    var item = new DItem
        //    {
        //        Breed = dItem.Breed,
        //        ItemName = dItem.ItemName,
        //        Quality = dItem.Quality,
        //        Side = dItem.Side,
        //        Thick = dItem.Thick,
        //        Width = dItem.Width,
        //        Length = dItem.Length,
        //        MixedLength = dItem.MixedLength,
        //        Moisture = dItem.Moisture
        //    };

        //    try
        //    {
        //       var res = await _context.GetItemByParams(item);
        //        return Ok(res);
        //    }
        //    catch
        //    {
        //        return NotFound();
        //    }
        //}

        /// <summary>
        /// Создать данные нового ITEM-кода
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Admin, Master")]
        public async Task<IActionResult> Create(CreateOrEditItem dItem)
        {
            var item = new DItem
            {
                Breed = dItem.Breed,
                ItemName = dItem.ItemName,
                Quality = dItem.Quality,
                Side = dItem.Side,
                Thick = dItem.Thick,
                Width = dItem.Width,
                Length = dItem.Length,
                MixedLength = dItem.MixedLength,
                Moisture = dItem.Moisture
            };

            try
            {
                await _context.CreateAsync(item);
                await _log.CreateAsync(new LogModel()
                {
                    Message = "Создан ITEM-код " + dItem.ItemName,
                    LogType = "Действие оператора",
                    User = User.FindFirstValue(JwtClaimTypes.Name),
                    LogTime = DateTime.Now,
                    ON_OFF_text = "Пришло"
                });
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }


        /// <summary>
        /// Удалить данные ITEM-кода по id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin, Master")]
        public async Task<IActionResult> DeleteDItems(int id)
        {

            try
            {
                var item = await _context.GetByIdAsync(id);
                await _context.DeleteAsync(id);
                await _log.CreateAsync(new LogModel()
                {
                    Message = "Удалён ITEM-код " + item.ItemName,
                    LogType = "Действие оператора",
                    User = User.FindFirstValue(JwtClaimTypes.Name),
                    LogTime = DateTime.Now,
                    ON_OFF_text = "Пришло"
                });
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Получить список ITEM-кодов по толщине и ширине
        /// </summary>
        /// <returns></returns>
        [HttpGet("ItemsTW")]
        public async Task<ActionResult<IEnumerable<DItemResponse>>> GetItemsByTandW(int T, int W)
        {
            var items = await _context.GetItemsByTandW(T, W);
            var ItemsResponse = items.Select(x => new DItemResponse
            {
                Breed = x.Breed,
                Id = x.Id,
                ItemName = x.ItemName,
                Quality = x.Quality,
                Side = x.Side,
                Thick = x.Thick,
                Width = x.Width,
                Length = x.Length,
                MixedLength = x.MixedLength,
                Moisture = x.Moisture

            }).ToList();

            return Ok(ItemsResponse);

        }

        /// <summary>
        /// Импортировать CSV c ITEM-кодами
        /// </summary>
        /// <returns></returns>
        [HttpPost("ImportCSV")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> ImportCSV()
        {
            using (var reader = new StreamReader("\\\\" + Environment.MachineName + "\\ExportCSV\\ITEMS\\ITEMS.csv"))
            {
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    var xxx = csv.GetRecords<DItem>().ToList();

                    try
                    {
                        await _context.ImportCSV(xxx);
                        await _log.CreateAsync(new LogModel()
                        {
                            Message = "Импортирован список ITEM-кодов ",
                            LogType = "Действие оператора",
                            User = User.FindFirstValue(JwtClaimTypes.Name),
                            LogTime = DateTime.Now,
                            ON_OFF_text = "Пришло"
                        });
                        return Ok();
                    }
                    catch (Exception e)
                    {
                        return Ok(e);

                    }
                }
            }
        }

        /// <summary>
        /// Экспортировать CSV c ITEM-кодами
        /// </summary>
        /// <returns></returns>
        [HttpPost("ExportCSV")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> ExportCSV()
        {
            var items = await _context.GetAllAsync();
            string filename = "\\\\" + Environment.MachineName + "\\ExportCSV\\ITEMS\\ITEMS.csv";


            using (FileStream stream = System.IO.File.Create(filename))
            using (var writer = new StreamWriter(stream, Encoding.UTF8))

            {
                using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                {
                    //csv.Configuration.Delimiter = ";";
                    csv.WriteRecords(items);

                }
                writer.Close();

            }
            await _log.CreateAsync(new LogModel()
            {
                Message = "Экспортирован список ITEM-кодов ",
                LogType = "Действие оператора",
                User = User.FindFirstValue(JwtClaimTypes.Name),
                LogTime = DateTime.Now,
                ON_OFF_text = "Пришло"
            });
            return Ok();
        }
    }
}
