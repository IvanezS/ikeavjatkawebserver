﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Abstractions.Repositories;
using Core.Domain.Administration;
using Core.Domain.Management;
using Microsoft.AspNetCore.Mvc;
using WebHost.Models;

namespace WebHost.Controllers
{
   
    /// <summary>
    /// Сотрудники
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IRepository<Employee> _context;
        /// <summary>
        /// Сотрудники
        /// </summary>
        public EmployeeController(IRepository<Employee> context)
        {
            _context = context;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<EmployeeResponse>>> GetEmpl()
        {
            var emples = await _context.GetAllAsync();
            var emplesResponse = emples.Select(x => new EmployeeResponse
            {
                Id = x.Id,
                Email = x.Email,
                FullName = x.FullName,
                Role = new RoleItemResponse 
                { 
                    Id = x.Role.Id,
                    Name = x.Role.Name,
                    Description = x.Role.Description
                }

            }).ToList(); 

            return Ok (emplesResponse);
        }

        /// <summary>
        /// Получить данные сотрудника по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmplId(int id)
        {
            var emple = await _context.GetByIdAsync(id);

            if (emple == null)
            {
                return NotFound();
            }

            var empleResponse = new EmployeeResponse
            {
                Id = emple.Id,
                Email = emple.Email,
                FullName = emple.FullName,
                Role = new RoleItemResponse
                {
                    Id = emple.Role.Id,
                    Name = emple.Role.Name,
                    Description = emple.Role.Description
                }
            };

            return Ok(empleResponse);
        }

        /// <summary>
        /// Обновить данные сотрудника по id
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEmpl(Employee empl)
        {

            try
            {
                await _context.UpdateAsync(empl);
                return Ok();
            }
            catch
            {
                return NotFound();
            }
            
        }

        /// <summary>
        /// Создать данные нового ITEM-кода
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> PostEmple(Employee empl)
        {

            try
            {
                await _context.CreateAsync(empl);
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Удалить данные сотрудника по id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEmpl(int id)
        {

            try
            {
                await _context.DeleteAsync(id);
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }


    }
}
