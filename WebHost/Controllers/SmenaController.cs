﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Core.Abstractions.Repositories;
using Core.Domain.Management;
using DataAccess.CustomLogger;
using DataAccess.Repositories;
using DataPLC.Data;
using IdentityModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebHost.Models;

namespace WebHost.Controllers
{
    /// <summary>
    /// Смены
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class SmenaController : ControllerBase
    {
        private readonly SmenaRepository _context;
        private readonly LogRepository _log;
        private readonly IPlcDb _contextPLC;

        /// <summary>
        /// Смены
        /// </summary>
        public SmenaController(SmenaRepository context, LogRepository log, IPlcDb contextPLC)
        {
            _context = context;
            _log = log;
            _contextPLC = contextPLC;
        }

        /// <summary>
        /// Получить список всех смен
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DSmena>>> GetAll()
        {
            var Smenas = await _context.GetAllAsync();
            return Ok(Smenas.OrderByDescending(x => x.Id).Take(100));
        }

        /// <summary>
        /// Создать смену
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<DPacket>> Create(MainSettingsDto settingsDto)
        {
            var settings = await _contextPLC.GetPlcSetDBAsync();
            if (!settings.SmenaActive)
            {
                settings.SmenaNum++;
                settings.SmenaName = settingsDto.SmenaName;
                settings.SmenaActive = true;
                settings.OperatorSurname = settingsDto.OperatorSurname;
                _contextPLC.UpdateSettings(settings);
                _contextPLC.StartNewPack(settings.SmenaActive);
                var smena = new DSmena()
                {
                    SmenaName = settings.SmenaName,
                    SmenaNum = settings.SmenaNum,
                    Surname = settings.OperatorSurname,
                    DateTimeStart = DateTime.Now,
                    DateTimeStop = DateTime.Now
                };

                try
                {
                    await _context.CreateAsync(smena);
                    await _log.CreateAsync(new LogModel()
                    {
                        Message = "Начата смена №" + settings.SmenaNum,
                        LogType = "Действие оператора",
                        User = User.FindFirstValue(JwtClaimTypes.Name),
                        LogTime = DateTime.Now,
                        ON_OFF_text = "Пришло"
                    });
                    return Ok();
                }
                catch
                {
                    return BadRequest();
                }
            }
            return Ok();
        }

        /// <summary>
        /// Закончить смену
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult<DPacket>> FinishSmena()
        {
            

            var settings = await _contextPLC.GetPlcSetDBAsync();
            if (settings.SmenaActive)
            {


                var smena = await _context.GetByIdNumber(settings.SmenaNum);
                smena.DateTimeStop = DateTime.Now;

                try
                {
                    await _context.UpdateAsync(smena);
                    settings.SmenaName = "";
                    settings.SmenaActive = false;
                    
                    settings.OperatorSurname = "";
                    _contextPLC.UpdateSettings(settings);
                    _contextPLC.StartNewPack(settings.SmenaActive);
                    await _log.CreateAsync(new LogModel()
                    {
                        Message = "Закончена смена №" + settings.SmenaNum,
                        LogType = "Действие оператора",
                        User = User.FindFirstValue(JwtClaimTypes.Name),
                        LogTime = DateTime.Now,
                        ON_OFF_text = "Пришло"
                    });
                    return Ok();
                }
                catch
                {
                    return BadRequest();
                }
            }
            return Ok();
        }
    }
}
