﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Domain.Management;
using DataAccess.CustomLogger;
using DataAccess.Repositories;
using IdentityModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebHost.Models;
using System.Security.Claims;

namespace WebHost.Controllers
{        
    /// <summary>
    /// Партии
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DMainPartsController : ControllerBase
    {

        private readonly MainPartRepository _context;
        private readonly LogRepository _log;

        /// <summary>
        /// Партии
        /// </summary>
        /// 
        public DMainPartsController(MainPartRepository context, LogRepository log)
        {
            _context = context;
            _log = log;
        }

        /// <summary>
        /// Получить данные всех партий
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DMainPartResponse>>> Getparts()
        {
            var mainparts = await _context.GetAllAsync();
            if (mainparts != null) { 
                var partsResp = mainparts.Select(x => new DMainPartResponse
                {
                    Id = x.Id,
                    Comment = x.Comment,
                    MainPartNumber = x.MainPartNumber,
                    ProviderId = x.ProviderId,
                    State = x.State,
                    ItemCounter = x.ItemCounter,
                    Provider = new DProviderShortResponse
                    {
                        Id = x.Provider.Id,
                        Name = x.Provider.Name,
                        ITEM = x.Provider.ITEM,
                        VATno = x.Provider.VATno
                    },
                    ItemParts = x.ItemParts.Select(pe => new DPartResponse
                    { 
                        Id = pe.Id, 
                        Comments = pe.Comments,
                        PartNumber =pe.PartNumber,
                        State = pe.State,
                        ItemId = pe.ItemId,
                        MainPartId = pe.MainPartId,
                        Item = new DItemResponse
                        {
                            Id = pe.Item.Id,
                            Breed = pe.Item.Breed,
                            ItemName = pe.Item.ItemName,
                            Length = pe.Item.Length,
                            MixedLength = pe.Item.MixedLength,
                            Moisture = pe.Item.Moisture,
                            Quality = pe.Item.Quality,
                            Side = pe.Item.Side,
                            Thick = pe.Item.Thick,
                            Width = pe.Item.Width
                        },
                        Packets = pe.Packets.Select(pa => new DPacketShortResponse
                        {
                            Id = pa.Id,
                            PacketNumber = pa.PacketNumber,
                            State = pa.State,
                            PartId = pa.PartId,
                            TotalPlanks = pa.TotalPlanks,
                            Note = pa.Note,
                            PackDate = pa.PackDate.ToString("u"),
                            Status = pa.Status,
                            TotalVolume = pa.TotalVolume
                        }),
                        ResortedPackets = pe.ResortedPackets.Select(po => new DResortedPacketShortResponse
                        {
                            Id = po.Id,
                            isDefected = po.isDefected,
                            PackDate = po.PackDate.ToString("u"),
                            PacketNumber = po.PacketNumber,
                            PartId = po.PartId,
                            SmenaName = po.SmenaName,
                            SmenaNum = po.SmenaNum,
                            TotalPlanks = po.TotalPlanks,
                            TotalVolume = po.TotalVolume
                        }),
                        
                        

                    })
                    

                });
                return Ok(partsResp.OrderByDescending(x => x.Id).Take(100));
            }
            return Ok();
        }

        /// <summary>
        /// Получить данные партии по id
        /// </summary>
        /// <returns></returns>
        /// <param name="id">Id партии</param>
        [HttpGet("{id}")]
        public async Task<ActionResult<DMainPartResponse>> GetDPart(int id)
        {
            var mainpart = await _context.GetByIdAsync(id);
            if (mainpart != null) 
            {
                var partResp = new DMainPartResponse
                {
                    Id = mainpart.Id,
                    Comment = mainpart.Comment,
                    MainPartNumber = mainpart.MainPartNumber,
                    ProviderId = mainpart.ProviderId,
                    State = mainpart.State,
                    Provider = new DProviderShortResponse
                    {
                        Id = mainpart.Provider.Id,
                        Name = mainpart.Provider.Name,
                        ITEM = mainpart.Provider.ITEM,
                        VATno = mainpart.Provider.VATno
                    },
                    ItemParts = mainpart.ItemParts.Select(pe => new DPartResponse
                    {
                        Id = pe.Id,
                        Comments = pe.Comments,
                        PartNumber = pe.PartNumber,
                        State = pe.State,
                        ItemId = pe.ItemId,
                        Item = new DItemResponse
                        {
                            Id = pe.Id,
                            Breed = pe.Item.Breed,
                            ItemName = pe.Item.ItemName,
                            Length = pe.Item.Length,
                            MixedLength = pe.Item.MixedLength,
                            Moisture = pe.Item.Moisture,
                            Quality = pe.Item.Quality,
                            Side = pe.Item.Side,
                            Thick = pe.Item.Thick,
                            Width = pe.Item.Width
                        },
                        Packets = pe.Packets.Select(pa => new DPacketShortResponse
                        {
                            Id = pa.Id,
                            PacketNumber = pa.PacketNumber,
                            State = pa.State,
                            PartId = pa.PartId,
                            TotalPlanks = pa.TotalPlanks,
                            Note = pa.Note,
                            PackDate = pa.PackDate.ToString("u"),
                            Status = pa.Status,
                            TotalVolume = pa.TotalVolume
                        }),
                        ResortedPackets = pe.ResortedPackets.Select(po => new DResortedPacketShortResponse
                        {
                            Id = po.Id,
                            isDefected = po.isDefected,
                            PackDate = po.PackDate.ToString("u"),
                            PacketNumber = po.PacketNumber,
                            PartId = po.PartId,
                            SmenaName = po.SmenaName,
                            SmenaNum = po.SmenaNum,
                            TotalPlanks = po.TotalPlanks,
                            TotalVolume = po.TotalVolume
                        }),
                        MainPartId = pe.MainPartId

                    })


                };
                return Ok(partResp);
            }else{
                return NotFound();    
            }
        }

        /// <summary>
        /// Обновить данные партии по id
        /// </summary>
        /// <returns></returns>
        /// <param name="id">Id партии</param>
        /// <param name="cep">Параметры партии</param>
        [HttpPut("{id}")]
        [Authorize(Roles = "Admin, Master")]
        public async Task<IActionResult> PutDPart(int id, CreateOrEditMainPart cep)
        {
            try
            {
                var mainpart = await _context.GetByIdAsync(id);

                mainpart.State = cep.State;
                mainpart.Comment = cep.Comment;
                mainpart.MainPartNumber = cep.MainPartNumber;
                mainpart.ProviderId = cep.ProviderId;

                await _context.UpdateAsync(mainpart);
                await _log.CreateAsync(new LogModel()
                {
                    Message = "Обновлёна партия номер " + mainpart.MainPartNumber,
                    LogType = "Действие оператора",
                    User = User.FindFirstValue(JwtClaimTypes.Name),
                    LogTime = DateTime.Now,
                    ON_OFF_text = "Пришло"
                });
                return Ok();
            }
            catch
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Создать данные новой партии
        /// </summary>
        /// <returns></returns>
        /// <param name="cep">Параметры партии</param>
        [HttpPost]
        [Authorize(Roles = "Admin, Master")]
        public async Task<ActionResult<DPart>> PostDPart(CreateOrEditMainPart cep)
        {
            var mainpart = new DMainPart()
            {
                State = 0,
                Comment = cep.Comment,
                MainPartNumber = cep.MainPartNumber,
                ItemCounter = 0,
                ProviderId = cep.ProviderId
            };
            try
            {
                await _context.CreateAsync(mainpart);
                await _log.CreateAsync(new LogModel()
                {
                    Message = "Создана партия номер " + mainpart.MainPartNumber,
                    LogType = "Действие оператора",
                    User = User.FindFirstValue(JwtClaimTypes.Name),
                    LogTime = DateTime.Now,
                    ON_OFF_text = "Пришло"
                });
                return Ok();
            }
            catch
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Удалить данные партии по id
        /// </summary>
        /// <returns></returns>
        /// <param name="id">Id партии</param>
        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin, Master")]
        public async Task<ActionResult<DPart>> DeleteDPart(int id)
        {
            try
            {
                var part = await _context.GetByIdAsync(id);
                await _context.DeleteAsync(id);
                await _log.CreateAsync(new LogModel()
                {
                    Message = "Удалена партия номер " + part.MainPartNumber,
                    LogType = "Действие оператора",
                    User = User.FindFirstValue(JwtClaimTypes.Name),
                    LogTime = DateTime.Now,
                    ON_OFF_text = "Пришло"
                });
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }

    }
}
