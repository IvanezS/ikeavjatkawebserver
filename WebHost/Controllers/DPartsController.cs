﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Domain.Management;
using DataAccess.CustomLogger;
using DataAccess.Repositories;
using IdentityModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebHost.Models;
using System.Security.Claims;

namespace WebHost.Controllers
{        
    /// <summary>
    /// Партии
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DPartsController : ControllerBase
    {

        private readonly PartRepository _context;
        private readonly MainPartRepository _contextMain;
        private readonly LogRepository _log;

        /// <summary>
        /// Партии
        /// </summary>
        /// 
        public DPartsController(PartRepository context, LogRepository log, MainPartRepository contextMain)
        {
            _context = context;
            _log = log;
            _contextMain = contextMain;
        }

        /// <summary>
        /// Получить данные всех партий
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DPartShortResponse>>> Getparts()
        {
            var parts = await _context.GetAllAsync();
            if (parts != null) { 
                var partsResp = parts.Select(x => new DPartResponse
                {
                    Id = x.Id,
                    Comments = x.Comments,
                    MainPartId = x.MainPartId,
                    Packets = x.Packets.Select(pa => new DPacketShortResponse
                    {
                        Id = pa.Id,
                        PacketNumber = pa.PacketNumber,
                        State = pa.State,
                        PartId = pa.PartId,
                        TotalPlanks = pa.TotalPlanks,
                        TotalVolume = pa.TotalVolume,
                        PackDate = pa.PackDate.ToString(),
                        Note = pa.Note,
                        Status = pa.Status,
                      }).ToList(),
                    PartNumber = x.PartNumber,
                    //Provider = new DProviderShortResponse
                    //{
                    //    Id = x.Provider.Id,
                    //    Name = x.Provider.Name,
                    //    ITEM = x.Provider.ITEM,
                    //    VATno = x.Provider.VATno
                    //},
                    //ProviderId = x.ProviderId,
                    State = x.State,
                    ResortedPackets = x.ResortedPackets.Select(po => new DResortedPacketShortResponse
                    {
                        Id = po.Id,
                        isDefected = po.isDefected,
                        PackDate = po.PackDate.ToString("u"),
                        PacketNumber = po.PacketNumber,
                        PartId = po.PartId,
                        SmenaName = po.SmenaName,
                        SmenaNum = po.SmenaNum,
                        TotalPlanks = po.TotalPlanks,
                        TotalVolume = po.TotalVolume
                    }).ToList(),
                    ItemId = x.ItemId,
                    Item = new DItemResponse
                    {
                        Breed = x.Item.Breed,
                        ItemName = x.Item.ItemName,
                        Length = x.Item.Length,
                        MixedLength = x.Item.MixedLength,
                        Moisture = x.Item.Moisture,
                        Quality = x.Item.Quality,
                        Side = x.Item.Side,
                        Thick = x.Item.Thick,
                        Width = x.Item.Width
                    }
                });
                return Ok(partsResp.OrderByDescending(x => x.Id).Take(100));
            }
            return Ok();
        }

        /// <summary>
        /// Получить данные партии по id
        /// </summary>
        /// <returns></returns>
        /// <param name="id">Id партии</param>
        [HttpGet("{id}")]
        public async Task<ActionResult<DPartResponse>> GetDPart(int id)
        {
            var part = await _context.GetByIdAsync(id);
            if (part != null) 
            { 
                var partResp = new DPartResponse
                {
                    Id = part.Id,
                    Comments = part.Comments,
                    MainPartId = part.MainPartId,
                    Packets = part.Packets.Select(pa => new DPacketShortResponse
                    {
                        Id = pa.Id,
                        PacketNumber = pa.PacketNumber,
                        PartId = pa.PartId,
                        State = pa.State,
                        TotalPlanks = pa.TotalPlanks,
                        TotalVolume = pa.TotalVolume,
                        Status = pa.Status,
                        Note = pa.Note,
                        PackDate = pa.PackDate.ToString("s")
                    }).ToList(),
                    PartNumber = part.PartNumber,
                    State = part.State,
                    ResortedPackets = part.ResortedPackets.Select(po => new DResortedPacketShortResponse
                    {
                        Id = po.Id,
                        isDefected = po.isDefected,
                        PackDate = po.PackDate.ToString("u"),
                        PacketNumber = po.PacketNumber,
                        PartId = po.PartId,
                        SmenaName = po.SmenaName,
                        SmenaNum = po.SmenaNum,
                        TotalPlanks = po.TotalPlanks,
                        TotalVolume = po.TotalVolume
                    }).ToList(),
                    ItemId = part.ItemId,
                    Item = new DItemResponse
                    {
                        Breed = part.Item.Breed,
                        ItemName = part.Item.ItemName,
                        Length = part.Item.Length,
                        MixedLength = part.Item.MixedLength,
                        Moisture = part.Item.Moisture,
                        Quality = part.Item.Quality,
                        Side = part.Item.Side,
                        Thick = part.Item.Thick,
                        Width = part.Item.Width
                    }
                };
                return Ok(partResp);
            }else{
                return NotFound();    
            }
        }

        /// <summary>
        /// Обновить данные партии по id
        /// </summary>
        /// <returns></returns>
        /// <param name="id">Id партии</param>
        /// <param name="ProvId">Id поставщика</param>
        /// <param name="cep">Параметры партии</param>
        [HttpPut("{id}")]
        [Authorize(Roles = "Admin, Master")]
        public async Task<IActionResult> PutDPart(int id, int ProvId, CreateOrEditPart cep)
        {
            try
            {
                var part = await _context.GetByIdAsync(id);
                part.State = cep.State;
                part.PartNumber = cep.PartNumber;
                part.Comments = cep.Comments;
                part.ItemId = cep.ItemId;
                await _context.UpdateAsync(part);
                await _log.CreateAsync(new LogModel()
                {
                    Message = "Обновлёна партия номер " + part.PartNumber,
                    LogType = "Действие оператора",
                    User = User.FindFirstValue(JwtClaimTypes.Name),
                    LogTime = DateTime.Now,
                    ON_OFF_text = "Пришло"
                });
                return Ok();
            }
            catch
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Создать данные новой партии
        /// </summary>
        /// <returns></returns>
        /// <param name="cep">Параметры партии</param>
        [HttpPost]
        [Authorize(Roles = "Admin, Master")]
        public async Task<ActionResult<DPart>> PostDPart(CreateOrEditPart cep)
        {
            var mainPart = await _contextMain.GetByIdAsync(cep.MainPartId);
            mainPart.ItemCounter++;
            await _contextMain.UpdateAsync(mainPart);

            var dPart = new DPart()
            {
                PartNumber = cep.PartNumber + "-"+ mainPart.ItemCounter.ToString(),
                ItemId = cep.ItemId,
                Comments = cep.Comments,
                MainPartId = cep.MainPartId,
                State = 0,
                PacketNumber = 0,
                ResortedPacketNumber = 0
            };
            try
            {
                await _context.CreateAsync(dPart);
                await _log.CreateAsync(new LogModel()
                {
                    Message = "Создана партия номер " + dPart.PartNumber,
                    LogType = "Действие оператора",
                    User = User.FindFirstValue(JwtClaimTypes.Name),
                    LogTime = DateTime.Now,
                    ON_OFF_text = "Пришло"
                });
                return Ok();
            }
            catch
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Удалить данные партии по id
        /// </summary>
        /// <returns></returns>
        /// <param name="id">Id партии</param>
        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin, Master")]
        public async Task<ActionResult<DPart>> DeleteDPart(int id)
        {
            try
            {
                var part = await _context.GetByIdAsync(id);
                await _context.DeleteAsync(id);
                await _log.CreateAsync(new LogModel()
                {
                    Message = "Удалена партия номер " + part.PartNumber,
                    LogType = "Действие оператора",
                    User = User.FindFirstValue(JwtClaimTypes.Name),
                    LogTime = DateTime.Now,
                    ON_OFF_text = "Пришло"
                });
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }

    }
}
