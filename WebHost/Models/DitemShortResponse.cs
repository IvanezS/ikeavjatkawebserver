﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebHost.Models
{
    public class DitemShortResponse
    {
        ///<summary>Наименование ITEM-кода</summary>
        public string ItemName { get; set; }
    }
}
