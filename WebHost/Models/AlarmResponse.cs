﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebHost.Models
{
    public class AlarmResponse
    {
        public string Message { get; set; }
        public string TimeON { get; set; }
        public bool On { get; set; }

    }
}
