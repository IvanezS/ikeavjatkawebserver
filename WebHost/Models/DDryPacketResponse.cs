﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebHost.Models
{
    public class DDryPacketResponse
    {
        //<summary>Ключ</summary>
        public int Id { get; set; }

        ///<summary>Номер пакета</summary>
        public string PacketNumber { get; set; }

        ///<summary>ключ ITEM-код</summary>
        public int ItemId { get; set; }

        ///<summary>ITEM-код</summary>
        public virtual DitemShortResponse Item { get; set; }

        ///<summary>Кол-во досок в пакете, шт</summary>
        public int TotalPlanks { get; set; }

        ///<summary>Номер смены</summary>
        public int SmenaNum { get; set; }

        ///<summary>Номер смены</summary>
        public string SmenaName { get; set; }

        ///<summary>Дата сборки пакета</summary>
        public System.DateTime PackDate { get; set; }

        ///<summary>Суммарный объём досок пакета, м3</summary>
        public float TotalVolume { get; set; }

    }
}
