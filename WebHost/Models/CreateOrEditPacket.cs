﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebHost.Controllers
{
    public class CreateOrEditPacket
    {
        ///<summary>Состояние (0 - несортированный, 1 - сортированный, 2 - потерян)</summary>
        public int State { get; set; }

        ///<summary>Ключ партии</summary>
        public int PartId { get; set; }

        ///<summary>Кол-во досок в пакете, шт</summary>
        public int TotalPlanks { get; set; }

        ///<summary>Суммарный объём досок пакета, м3</summary>
        public float TotalVolume { get; set; }

        ///<summary>Заметка к пакету</summary>
        public string Note { get; set; }

        ///<summary>Статус пакета : OK - true, NOK - false</summary>
        public bool Status { get; set; }

        ///<summary>Номер пакета</summary>
        public int PacketNumber { get; set; }




    }
}
