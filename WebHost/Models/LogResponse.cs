﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebHost.Models
{
    public class LogResponse
    {
        public string Message { get; set; }
        public string LogType { get; set; }
        public string User { get; set; }
        public string ON_OFF_text { get; set; }
        public string LogTime { get; set; }
    }
}
