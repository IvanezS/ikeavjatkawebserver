﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebHost.Models
{
    ///<summary>
    ///Схема укладки прокладок
    ///</summary>
    public class DGasketResponse
    {
        ///<summary>
        ///Ключ
        ///</summary>
        public int Id { get; set; }


        ///<summary>
        ///Наименование схемы укладки прокладок
        ///</summary>
        public string Name { get; set; }

        ///<summary>
        ///Схема укладки прокладок
        ///</summary>
        public int Sxema { get; set; }





    }
}
