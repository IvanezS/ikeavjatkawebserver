﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebHost.Models
{
    public class DResortedPacketShortResponse
    {
        //<summary>Ключ</summary>
        public int Id { get; set; }

        ///<summary>Номер пакета</summary>
        public int PacketNumber { get; set; }

        ///<summary>Пакет брака</summary>
        public bool isDefected { get; set; }

        ///<summary>Ключ партии</summary>
        public int PartId { get; set; }

        ///<summary>Кол-во досок в пакете, шт</summary>
        public int TotalPlanks { get; set; }

        ///<summary>Номер смены</summary>
        public int SmenaNum { get; set; }

        ///<summary>Номер смены</summary>
        public string SmenaName { get; set; }

        ///<summary>Дата сборки пакета</summary>
        public string PackDate { get; set; }

        ///<summary>Суммарный объём досок пакета, м3</summary>
        public float TotalVolume { get; set; }

    }
}
