﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebHost.Models
{
    public class DProviderResponse
    {
        //<summary>Ключ</summary>
        public int Id { get; set; }

        ///<summary>ITEM-код поставщика</summary>
        public string ITEM { get; set; }

        ///<summary>Наименование поставщика</summary>
        public string Name { get; set; }

        ///<summary>VAT номер</summary>
        public string VATno { get; set; }

        ///<summary>Список партий поставщика</summary>
        public virtual ICollection<DPartShortResponse> Parties { get; set; }
    }
}
