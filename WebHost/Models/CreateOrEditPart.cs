﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebHost.Controllers
{
    public class CreateOrEditPart
    {
        ///<summary>Состояние партии (0 - ни одного пакета не отсортировано, 1 - начат процесс пересортировки, 2 - закончен процесс пересортировки)</summary>
        public int State { get; set; }

        ///<summary>Наименование партии</summary>
        public string PartNumber { get; set; }

        ///<summary>Ключ к главной партии</summary>
        public int MainPartId { get; set; }

        ///<summary>Ключ к ITEM-коду партии</summary>
        public int ItemId { get; set; }

        ///<summary>Комментарий к партии</summary>
        public string Comments { get; set; }

    }
}
