﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebHost.Controllers
{
    public class CreateOrEditProvider
    {
        ///<summary>ITEM-код поставщика</summary>
        public string ITEM { get; set; }

        ///<summary>Наименование поставщика</summary>
        public string Name { get; set; }

        ///<summary>VAT номер</summary>
        public string VATno { get; set; }
    }
}
