﻿using System.Collections.Generic;

namespace WebHost.Models
{
    public class DMainPartResponse
    {
        //<summary>Ключ</summary>
        public int Id { get; set; }

        ///<summary>Состояние партии (0 - ни одного пакета не отсортировано, 1 - начат процесс пересортировки, 2 - закончен процесс пересортировки)</summary>
        public int State { get; set; }

        ///<summary>Номер главной партии</summary>
        public string MainPartNumber { get; set; }

        ///<summary>Счётчик ITEM-кодов партии</summary>
        public int ItemCounter { get; set; }

        ///<summary>Ключ поставщика</summary>
        public int ProviderId { get; set; }

        ///<summary>Поставщик</summary>
        public virtual DProviderShortResponse Provider { get; set; }

        ///<summary>Комментарий</summary>
        public string Comment { get; set; }

        ///<summary>Список ITEM-записей партии</summary>
        public virtual IEnumerable<DPartResponse> ItemParts { get; set; }
    }
}
