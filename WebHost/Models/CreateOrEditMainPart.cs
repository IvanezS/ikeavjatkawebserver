﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebHost.Controllers
{
    public class CreateOrEditMainPart
    {
        ///<summary>Состояние партии (0 - ни одного пакета не отсортировано, 1 - начат процесс пересортировки, 2 - закончен процесс пересортировки)</summary>
        public int State { get; set; }

        ///<summary>Номер главной партии</summary>
        public string MainPartNumber { get; set; }

        ///<summary>Ключ поставщика</summary>
        public int ProviderId { get; set; }

        ///<summary>Комментарий</summary>
        public string Comment { get; set; }

    }
}
