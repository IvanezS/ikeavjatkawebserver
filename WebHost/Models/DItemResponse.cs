﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebHost.Models
{
    public class DItemResponse
    {
        //<summary>Ключ</summary>
        public int Id { get; set; }

        ///<summary>Наименование ITEM-кода</summary>
        public string ItemName { get; set; }

        ///<summary>Толщина, мм</summary>
        public int Thick { get; set; }

        ///<summary>Ширина, мм</summary>
        public int Width { get; set; }

        ///<summary>Длина, мм</summary>
        public int Length { get; set; }

        ///<summary>Смешанная длина</summary>
        public string MixedLength { get; set; }

        ///<summary>Качество</summary>
        public string Quality { get; set; }

        ///<summary>Порода</summary>
        public string Breed { get; set; }

        ///<summary>Сторона (бок / центр)</summary>
        public string Side { get; set; }

        ///<summary>Влажность</summary>
        public string Moisture { get; set; }
    }
}
