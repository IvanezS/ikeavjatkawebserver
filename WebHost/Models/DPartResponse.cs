﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebHost.Models
{
    public class DPartResponse
    {
        //<summary>Ключ</summary>
        public int Id { get; set; }

        ///<summary>Состояние партии (0 - ни одного пакета не отсортировано, 1 - начат процесс пересортировки, 2 - закончен процесс пересортировки)</summary>
        public int State { get; set; }

        ///<summary>Номер партии</summary>
        public string PartNumber { get; set; }

        ///<summary>Ключ к ITEM-коду партии</summary>
        public int ItemId { get; set; }

        ///<summary>Поставщик</summary>
        public virtual DItemResponse Item { get; set; }

        ///<summary>Ключ к главной партии</summary>
        public int MainPartId { get; set; }

        ///<summary>Главная партия</summary>
        public virtual DMainPartResponse MainPart { get; set; }

        ///<summary>Комментарий к партии</summary>
        public string Comments { get; set; }

        ///<summary>Список пакетов партии</summary>
        public virtual IEnumerable<DPacketShortResponse> Packets { get; set; }

        ///<summary>Список пересортированных пакетов партии</summary>
        public virtual IEnumerable<DResortedPacketShortResponse> ResortedPackets { get; set; }


    }
}
