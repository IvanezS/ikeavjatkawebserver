using Core.Abstractions.Repositories;
using DataAccess.CreateAndPrint;
using DataAccess.CustomLogger;
using DataAccess.Data;
using DataAccess.Repositories;
using DataPLC;
using DataPLC.Core;
using DataPLC.Data;
using DataPLC.Settings;
using IdentityModel;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using NSwag;
using NSwag.AspNetCore;
using NSwag.Generation.Processors.Security;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using WebHost.Settings;

namespace WebHost
{
    public class Startup
    {
        public IWebHostEnvironment Environment { get; }
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            Environment = env;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            HostsSettings hostSet;
            if (Environment.IsDevelopment())
            {
                hostSet = Configuration.GetSection("HostsSettingsDevelopment").Get<HostsSettings>();
            }
            else
            {
                hostSet = Configuration.GetSection("HostsSettings").Get<HostsSettings>();
            }

            services.AddCors(options =>
            {
                options.AddDefaultPolicy(
                    builder =>
                    {
                        builder.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin();
                    });
            });

            services.AddControllers();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.Authority = hostSet.IdentityHost;
                options.Audience = "webhost-api";
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    NameClaimType = JwtClaimTypes.Name
                };
                options.RequireHttpsMetadata = false;

                // allow self-signed SSL certs
                options.BackchannelHttpHandler = new HttpClientHandler { ServerCertificateCustomValidationCallback = delegate { return true; } };
            });
            services.AddMvcCore(options => {
                options.EnableEndpointRouting = false;
            })
            .AddAuthorization();

            services.Configure<PlcSettings>(Configuration.GetSection("PlcSettings"));
            services.Configure<PlcDbReadSettings>(Configuration.GetSection("PlcDbReadSettings"));
            services.Configure<PlcDataExchangeBackgroundSettings>(Configuration.GetSection("PlcDataExchangeBackgroundSettings"));
            services.Configure<ImSettings>(Configuration.GetSection("ImSettings"));
            services.Configure<PlcAlarmSettings>(Configuration.GetSection("PlcAlarmSettings"));
            services.Configure<PrinterNames>(Configuration.GetSection("PrinterNames"));
            

            
            services.AddSingleton<IDataPlcExchange, PlcS7Exchange>();
            //services.AddSingleton<IDataPlcExchange, PlcSharp7Exchange>(); //� �������� ��� ����� ������� ��� �� �������� �������� ����
            services.AddSingleton<IPlcDb, PlcDB>();

            services.AddScoped<RpackCreateAndPrint>();
            services.AddScoped<PackCreateAndPrint>();


            services.AddScoped<GasketRepository>();
            services.AddScoped<SmenaRepository>();
            services.AddScoped<LogRepository>();
            services.AddScoped<ProviderRepository>();
            services.AddScoped<PartRepository>();
            services.AddScoped<PacketRepository>();
            services.AddScoped<ResortedPacketRepository>();
            services.AddScoped<MainPartRepository>();
            services.AddScoped<ItemRepository>();
            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
            services.AddScoped<DbContextFactory>();

            services.AddScoped<IUserDbContextInit, DbContextInit>();

            services.AddDbContext<UserDBcontext>(options => options.UseSqlServer(Configuration.GetConnectionString("DevConnection")));

            services.AddOpenApiDocument(options =>
            {
                options.Title = "IKEA Vjatka API Doc";
                options.Version = "1.0";
                options.AddSecurity("oauth2", new OpenApiSecurityScheme
                {
                    Type = OpenApiSecuritySchemeType.OAuth2,
                    Flows = new OpenApiOAuthFlows
                    {
                        Implicit = new OpenApiOAuthFlow
                        {
                            AuthorizationUrl = hostSet.IdentityHost + $"/connect/authorize",
                            TokenUrl = hostSet.IdentityHost + $"/connect/token",
                            Scopes = new Dictionary<string, string>
                            {
                                { "webhost-api", "The WebHost API" }
                            }
                        }
                    }
                });
                options.OperationProcessors.Add(new OperationSecurityScopeProcessor("oauth2"));
            });



        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IUserDbContextInit userDbContextInit)
        {
            IdentityModelEventSource.ShowPII = true;
            HostsSettings hostSet;
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                hostSet = Configuration.GetSection("HostsSettingsDevelopment").Get<HostsSettings>();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
                hostSet = Configuration.GetSection("HostsSettings").Get<HostsSettings>();
            }

            app.UseCors(builder =>
                 builder
                   .WithOrigins(hostSet.WebAppHost)
                   .AllowAnyHeader()
                   .AllowAnyMethod()
                   .AllowCredentials()
            );

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
                x.OAuth2Client = new OAuth2ClientSettings
                {
                    ClientId = "swaggerui",
                    AppName = "WebHost API Swagger UI",
                };

            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();
            app.UseMvc();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }
    }
}
