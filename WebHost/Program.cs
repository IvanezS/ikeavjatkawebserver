using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using DataPLC;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace WebHost
{
    public class Program
    {


        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>

            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();

                    webBuilder.UseKestrel(
                        options =>
                        {
                            options.AddServerHeader = false;

                            var certificate = new X509Certificate2("stabel.local.pfx", "903");
                            options.Listen(IPAddress.Parse("0.0.0.0"), 6001, listenOptions =>
                            {
                                listenOptions.UseHttps(certificate);
                            });
                        }
                    );

                });
                        //.ConfigureServices(services =>
                        //{
                        //    services.AddHostedService<PlcDataExchangeService>();
                        //    services.AddScoped<IScopedProcessingService, ScopedProcessingService>();
                        //});
    }
}
