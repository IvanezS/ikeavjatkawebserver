﻿using System;
using System.Linq;
using System.Security.Claims;
using IdentityModel;
using IdentityProvider.Data;
using IdentityProvider.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace IdentityProvider
{
    public class SeedData
    {
        public static void EnsureSeedData(string connectionString)
        {
            var services = new ServiceCollection();
            services.AddLogging();
            services.AddDbContext<ApplicationDbContext>(options =>
               options.UseSqlite(connectionString));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            using (var serviceProvider = services.BuildServiceProvider())
            {
                using (var scope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
                {
                    var context = scope.ServiceProvider.GetService<ApplicationDbContext>();
                    context.Database.Migrate();

                    var userMgr = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();

                    var admin = userMgr.FindByNameAsync("Admin").Result;
                    if (admin == null)
                    {
                        admin = new ApplicationUser
                        {
                            UserName = "Admin"
                        };
                        var result = userMgr.CreateAsync(admin, "Kjyljy$3").Result;
                        if (!result.Succeeded)
                        {
                            throw new Exception(result.Errors.First().Description);
                        }

                        result = userMgr.AddClaimsAsync(admin, new Claim[]{
                        new Claim(JwtClaimTypes.Name, "Administrator"),
                        new Claim(JwtClaimTypes.Role, "Admin"),
                        new Claim(JwtClaimTypes.Email, "Admin@email.com"),
                        new Claim(JwtClaimTypes.EmailVerified, "true", ClaimValueTypes.Boolean),
                    }).Result;
                        if (!result.Succeeded)
                        {
                            throw new Exception(result.Errors.First().Description);
                        }
                        Log.Debug("Administrator created");
                    }
                    else
                    {
                        Log.Debug("Administrator already exists");
                    }

                    var oper = userMgr.FindByNameAsync("Operator").Result;
                    if (oper == null)
                    {
                        oper = new ApplicationUser
                        {
                            UserName = "Operator"
                        };
                        var result = userMgr.CreateAsync(oper, "Operator1@").Result;
                        if (!result.Succeeded)
                        {
                            throw new Exception(result.Errors.First().Description);
                        }

                        result = userMgr.AddClaimsAsync(oper, new Claim[]{
                        new Claim(JwtClaimTypes.Name, "Operator"),
                        new Claim(JwtClaimTypes.Role, "Oper"),
                        new Claim(JwtClaimTypes.Email, "Oper@email.com"),
                        new Claim(JwtClaimTypes.EmailVerified, "true", ClaimValueTypes.Boolean),
                    }).Result;
                        if (!result.Succeeded)
                        {
                            throw new Exception(result.Errors.First().Description);
                        }
                        Log.Debug("Operator created");
                    }
                    else
                    {
                        Log.Debug("Operator already exists");
                    }

                    var master = userMgr.FindByNameAsync("Master").Result;
                    if (master == null)
                    {
                        master = new ApplicationUser
                        {
                            UserName = "Master"
                        };
                        var result = userMgr.CreateAsync(master, "Master1@").Result;
                        if (!result.Succeeded)
                        {
                            throw new Exception(result.Errors.First().Description);
                        }

                        result = userMgr.AddClaimsAsync(master, new Claim[]{
                        new Claim(JwtClaimTypes.Name, "Master"),
                        new Claim(JwtClaimTypes.Role, "Master"),
                        new Claim(JwtClaimTypes.Email, "Master@email.com"),
                        new Claim(JwtClaimTypes.EmailVerified, "true", ClaimValueTypes.Boolean),
                    }).Result;
                        if (!result.Succeeded)
                        {
                            throw new Exception(result.Errors.First().Description);
                        }
                        Log.Debug("Master created");
                    }
                    else
                    {
                        Log.Debug("Master already exists");
                    }

                }
            }
        }
    }
}
