﻿using IdentityModel;
using IdentityProvider.Settings;
using IdentityServer4;
using IdentityServer4.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System.Collections.Generic;

namespace IdentityProvider
{
    public static class Config
    {

        static HostsSettings hostSet = new HostsSettings();
        static Config()
        {
            if (Startup.EnvironmentStat.IsDevelopment())
            {
                hostSet = Startup.StaticConfig.GetSection("HostsSettingsDevelopment").Get<HostsSettings>();
            }
            else
            {
                hostSet = Startup.StaticConfig.GetSection("HostsSettings").Get<HostsSettings>();
            }
        }

        public static IEnumerable<IdentityResource> Ids =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),

                // let's include the role claim in the profile
                new ProfileWithRoleIdentityResource(),
                new IdentityResources.Email()
            };


        public static IEnumerable<ApiResource> Apis =>
            new ApiResource[]
            {
                // the api requires the role claim
                new ApiResource("webhost-api", "The WebHost API", new[] { JwtClaimTypes.Name, JwtClaimTypes.Role })
            };


        public static IEnumerable<Client> Clients =>
            new Client[]
            {
                new Client
                {
                    ClientId = "react",
                    ClientName = "WebHost API",
                    ClientUri = hostSet.WebAppHost,
                    AllowedGrantTypes = GrantTypes.Implicit,
                    RequireClientSecret = false,
                    RequireConsent = false,
                    AlwaysIncludeUserClaimsInIdToken = true,
                    AlwaysSendClientClaims = true,
                    AllowedScopes = {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "webhost-api" 
                    },
                    RedirectUris = { hostSet.WebAppHost + $"/signin-oidc", hostSet.WebAppHost + $"/silent_renew.html" },
                    PostLogoutRedirectUris = { hostSet.WebAppHost },
                    AllowedCorsOrigins = { hostSet.WebAppHost },
                    AllowAccessTokensViaBrowser = true

                },

                new Client
                {
                    ClientId = "swaggerui",
                    ClientName = "WebHost API Swagger UI",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowAccessTokensViaBrowser = true,

                    RedirectUris = { hostSet.WebApiHost + $"/swagger/oauth2-redirect.html" },
                    PostLogoutRedirectUris = { hostSet.WebApiHost + $"/swagger/" },

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "webhost-api"
                    }
                },

                new Client
                {
                    ClientId = "WebAPI scanner client",
                    ClientName = "WebAPI Scanner Client Credentials Client",

                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    ClientSecrets = { new Secret("vjatka".Sha256()) },

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "webhost-api"
                    }
                },


            };
    }
}