﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityProvider.Settings
{
    public class HostsSettings
    {
        public string IdentityHost { get; set; }
        public string WebApiHost { get; set; }
        public string WebAppHost { get; set; }
    }
}
