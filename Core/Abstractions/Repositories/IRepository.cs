﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Domain;
using Core.Domain.Management;

namespace Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(int id);

        Task CreateAsync(T item);

        Task UpdateAsync(T item);

        Task DeleteAsync(int id);

        Task<List<T>> GetListByListId(List<int> ListId);
        
        Task ImportCSV(List<T> ListItem);
    }
}