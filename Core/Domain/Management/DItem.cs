﻿using Core.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
namespace Core.Domain.Management
{
    ///<summary>Класс ITEM кодов</summary>
    public class DItem : BaseEntity
    {
        ///<summary>Наименование ITEM-кода</summary>
        public string ItemName { get; set; }

        ///<summary>Толщина, мм</summary>
        public int Thick { get; set; }

        ///<summary>Ширина, мм</summary>
        public int Width { get; set; }

        ///<summary>Длина, мм</summary>
        public int Length { get; set; }

        ///<summary>Смешанная длина</summary>
        public string MixedLength { get; set; }

        ///<summary>Качество</summary>
        public string Quality { get; set; }

        ///<summary>Порода</summary>
        public string Breed { get; set; }

        ///<summary>Сторона (бок / центр)</summary>
        public string Side { get; set; }

        ///<summary>Влажность</summary>
        public string Moisture { get; set; }




    }
}
