﻿using System.Collections.Generic;

namespace Core.Domain.Management
{
    ///<summary>Класс поставщика</summary>
    public class DProvider : BaseEntity
    {        
        ///<summary>ITEM-код поставщика</summary>
        public string ITEM { get; set; }

        ///<summary>Наименование поставщика</summary>
        public string Name { get; set; }

        ///<summary>VAT номер</summary>
        public string VATno { get; set; }

    }
}
