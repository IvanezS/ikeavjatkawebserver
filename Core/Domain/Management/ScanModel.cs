﻿namespace Core.Domain.Management
{
    public class ScanModel
    {
        ///<summary>ITEM поставщика</summary>
        public string ProviderItem { get; set; }
        ///<summary>Номер партии</summary>
        public string PartNumber { get; set; }
        ///<summary>Номер пакета</summary>
        public string PacktNumber { get; set; }
        ///<summary>ITEM пакета</summary>
        public string PackItem { get; set; }

    }
}
