﻿using System.Collections.Generic;

namespace Core.Domain.Management
{
    ///<summary>Класс партии пакетов</summary>
    public class DPart : BaseEntity
    {
        ///<summary>Состояние партии (0 - ни одного пакета не отсортировано, 1 - начат процесс пересортировки, 2 - закончен процесс пересортировки)</summary>
        public int State { get; set; }

        ///<summary>Наименование партии</summary>
        public string PartNumber { get; set; }

        ///<summary>Текущий номер исходного пакета</summary>
        public int PacketNumber { get; set; }

        ///<summary>Текущий номер пересортированного пакета</summary>
        public int ResortedPacketNumber { get; set; }

        ///<summary>Ключ к главной партии</summary>
        public int MainPartId { get; set; }

        ///<summary>Главная партия</summary>
        public virtual DMainPart MainPart { get; set; }

        ///<summary>Ключ к ITEM-коду партии</summary>
        public int ItemId { get; set; }

        ///<summary>Поставщик</summary>
        public virtual DItem Item { get; set; }

        ///<summary>Комментарий к партии</summary>
        public string Comments { get; set; }

        ///<summary>Список пакетов партии</summary>
        public virtual ICollection<DPacket> Packets { get; set; }

        ///<summary>Список пересортированных пакетов партии</summary>
        public virtual ICollection<DResortedPacket> ResortedPackets { get; set; }

    }
}
