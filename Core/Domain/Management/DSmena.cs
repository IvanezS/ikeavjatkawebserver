﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Domain.Management
{
    public class DSmena : BaseEntity
    {
        public string SmenaName { get; set; }
        public string Surname { get; set; }
        public int SmenaNum { get; set; }
        public DateTime DateTimeStart { get; set; }
        public DateTime DateTimeStop { get; set; }
    }
}
