﻿using System;

namespace Core.Domain.Management
{
    ///<summary>Класс пакета</summary>
    public class DResortedPacket : BaseEntity
    {
        ///<summary>Состояние (0 - пересортированный, 1 - сухой)</summary>
        public int State { get; set; }

        ///<summary>Номер пакета</summary>
        public int PacketNumber { get; set; }

        ///<summary>Пакет брака</summary>
        public bool isDefected { get; set; }

        ///<summary>Ключ партии</summary>
        public int PartId { get; set; }

        ///<summary>Партия</summary>
        public virtual DPart Part { get; set; }

        ///<summary>Кол-во досок в пакете, шт</summary>
        public int TotalPlanks { get; set; }

        ///<summary>Суммарный объём досок пакета, м3</summary>
        public float TotalVolume { get; set; }

        ///<summary>Номер смены</summary>
        public int SmenaNum { get; set; }

        ///<summary>Номер смены</summary>
        public string SmenaName { get; set; }

        ///<summary>Дата сборки пакета</summary>
        public DateTime PackDate { get; set; }

    }
}
