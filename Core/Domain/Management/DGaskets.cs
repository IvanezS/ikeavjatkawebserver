﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Domain.Management
{
    ///<summary>
    ///Схема укладки прокладок
    ///</summary>
    public class DGaskets : BaseEntity
    {
        ///<summary>
        ///Наименование схемы укладки прокладок
        ///</summary>
        public string name { get; set; }

        ///<summary>
        ///Схема укладки прокладок
        ///</summary>
        public int sxema { get; set; }
    }
}
