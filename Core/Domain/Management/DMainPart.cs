﻿using System.Collections.Generic;

namespace Core.Domain.Management
{
    public class DMainPart : BaseEntity
    {
        ///<summary>Состояние партии (0 - ни одного пакета не отсортировано, 1 - начат процесс пересортировки, 2 - закончен процесс пересортировки)</summary>
        public int State { get; set; }

        ///<summary>Номер главной партии</summary>
        public string MainPartNumber { get; set; }

        ///<summary>Счётчик ITEM-кодов партии</summary>
        public int ItemCounter { get; set; }
        
        ///<summary>Ключ поставщика</summary>
        public int ProviderId { get; set; }

        ///<summary>Поставщик</summary>
        public virtual DProvider Provider { get; set; }

        ///<summary>Комментарий</summary>
        public string Comment { get; set; }

        ///<summary>Список партий (габаритов досок в рамках партии)</summary>
        public virtual ICollection<DPart> ItemParts { get; set; }
    }
}
