﻿using System;
using System.Collections.Generic;

namespace Core.Domain.Administration
{
    public class Employee
        : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        public int RoleId { get; set; }
        public virtual Role Role { get; set; }


    }
}